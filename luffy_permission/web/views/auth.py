from django.shortcuts import HttpResponse, render, redirect, reverse
from rbac import models
from django.conf import settings
from rbac.service.init_permission import init_permission


def login(request):
	if request.method == 'POST':
		# 获取用户名和密码
		username = request.POST.get('username')
		password = request.POST.get('password')
		# 在数据库筛选用户名和密码
		user = models.User.objects.filter(name=username, pwd=password).first()
		if not user:
			# 认证失败   返回错误提示
			return render(request, 'login.html', {'error': '用户名或密码错误'})
		# 认证成功   保存登录状态 权限的信息  菜单的信息
		init_permission(request,user)

		# 重定向到首页
		return redirect('index')

	return render(request, 'login.html')


def index(request):
	return render(request, 'index.html')
