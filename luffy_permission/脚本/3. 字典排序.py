from collections import OrderedDict

dic = OrderedDict()
dic['k2'] = 'v2'
dic['k1'] = 'v1'
dic['k3'] = 'v3'
# print(dic)

ret = sorted([2, 1, 5, 3], reverse=True)
# print(ret)

ret = sorted({2: None, 1: None, 3: None}, reverse=True)
# print(ret)

dic = {
	2: {
		'title': '财务管理',
		'icon': 'fa-cny',
		'weight': 101,
		'children': [
			{'url': '/payment/list/', 'title': '缴费列表'},
			{'url': '/order/list/', 'title': '订单列表'},
		]
	},
	1: {
		'title': '客户管理',
		'icon': 'fa-user-o',
		'weight': 100,
		'children': [
			{'url': '/customer/list/', 'title': '客户列表','class':'active'},
		]
	}
}

ret = sorted(dic,key=lambda x:dic[x]['weight'],reverse=True)
print(ret)
