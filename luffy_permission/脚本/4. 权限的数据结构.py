permission_list = [
	{'title': '客户列表', 'id': 1, 'url': '/customer/list/', 'pid': None},
	{'title': '添加客户', 'id': 2, 'url': '/customer/add/', 'pid': 1},
	{'title': '编辑客户', 'id': 3, 'url': '/customer/edit/(\\d+)/', 'pid': 1},
	{'title': '删除客户', 'id': 4, 'url': '/customer/del/(\\d+)/', 'pid': 1},
	{'title': '缴费列表', 'id': 5, 'url': '/payment/list/', 'pid': None},
	{'title': '添加缴费', 'id': 6, 'url': '/payment/add/', 'pid': 5},
	{'title': '编辑缴费', 'id': 7, 'url': '/payment/edit/(\\d+)/', 'pid': 5},
	{'title': '删除缴费', 'id': 8, 'url': '/payment/del/(\\d+)/', 'pid': 5}
]

permission_dict = {
	1: {'title': '客户列表', 'id': 1, 'url': '/customer/list/', 'pid': None},
	2: {'title': '添加客户', 'id': 2, 'url': '/customer/add/', 'pid': 1},
	3: {'title': '编辑客户', 'id': 3, 'url': '/customer/edit/(\\d+)/', 'pid': 1},
	4: {'title': '删除客户', 'id': 4, 'url': '/customer/del/(\\d+)/', 'pid': 1},
	5: {'title': '缴费列表', 'id': 5, 'url': '/payment/list/', 'pid': None},
	6: {'title': '添加缴费', 'id': 6, 'url': '/payment/add/', 'pid': 5},
	7: {'title': '编辑缴费', 'id': 7, 'url': '/payment/edit/(\\d+)/', 'pid': 5},
	8: {'title': '删除缴费', 'id': 8, 'url': '/payment/del/(\\d+)/', 'pid': 5}
}

import json

ret = json.dumps(permission_dict)
print(ret)

ret = json.loads(ret)
print(ret)

permission_dict = {
	'customer_add': {'pid': 1, 'id': 2, 'pname': 'customer_list', 'url': '/customer/add/', 'title': '添加客户'},
	'payment_list': {'pid': None, 'id': 5, 'pname': None, 'url': '/payment/list/', 'title': '缴费列表'},
	'payment_add': {'pid': 5, 'id': 6, 'pname': 'payment_list', 'url': '/payment/add/', 'title': '添加缴费'},
	'customer_edit': {'pid': 1, 'id': 3, 'pname': 'customer_list', 'url': '/customer/edit/(\\d+)/', 'title': '编辑客户'},
	'payment_del': {'pid': 5, 'id': 8, 'pname': 'payment_list', 'url': '/payment/del/(\\d+)/', 'title': '删除缴费'},
	'customer_list': {'pid': None, 'id': 1, 'pname': None, 'url': '/customer/list/', 'title': '客户列表'},
	'customer_del': {'pid': 1, 'id': 4, 'pname': 'customer_list', 'url': '/customer/del/(\\d+)/', 'title': '删除客户'},
	'payment_edit': {'pid': 5, 'id': 7, 'pname': 'payment_list', 'url': '/payment/edit/(\\d+)/', 'title': '编辑缴费'}
}
