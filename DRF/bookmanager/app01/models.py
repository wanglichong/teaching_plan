from django.db import models


# Create your models here.

class Publisher(models.Model):
	name = models.CharField(max_length=32)


class Author(models.Model):
	name = models.CharField(max_length=32)

	def __str__(self):
		return self.name


class Book(models.Model):
	title = models.CharField(max_length=32)
	pub_date = models.DateField()
	category_choice = ((1, '言情'), (2, '都市'), (3, '科幻'))
	category = models.IntegerField(choices=category_choice)
	publisher = models.ForeignKey('Publisher', on_delete=models.CASCADE)
	authors = models.ManyToManyField('Author')
