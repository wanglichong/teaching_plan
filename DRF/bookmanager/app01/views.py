from rest_framework.views import APIView
from rest_framework.response import Response
from app01.serializers import BookModelSerializer, PublisherModelSerializer, AuthorModelSerializer
from app01 import models


class BookListView(APIView):
	"""使用djangorestful进行json序列化"""
	queryset =  models.Book.objects.all()
	serializer_class = BookModelSerializer
	def get(self, request):
		"""以json形式返回书籍的列表"""
		# 1. 获取所有的书籍对象
		queryset = self.queryset
		# 2. 将数据序列化成json格式
		ser_obj = self.serializer_class(queryset, many=True)
		# 3. 返回
		return Response(ser_obj.data)

	def post(self, request):
		# 1.获取提交的数据
		# print(request.data)
		# request 是重新封装的对象 request._request ——》 原来的request对象
		ser_obj = BookModelSerializer(data=request.data)
		# 2. 检验通过保存到数据库
		if ser_obj.is_valid():
			# ser_obj.validated_data
			ser_obj.save()

			return Response(ser_obj.data)
		# 3. 返回不同的内容
		return Response(ser_obj.errors)


class BookDetailView(APIView):
	def get(self, request, pk):
		"""获取一本书的详情"""
		# 1. 根据PK获取一本书的对象
		book_obj = models.Book.objects.filter(pk=pk).first()
		if book_obj:
			# 2. 对书的对象进行json序列化
			# ser_obj = BookSerializer(book_obj)
			ser_obj = BookModelSerializer(book_obj)
			# 3. 返回json数据
			return Response(ser_obj.data)
		else:
			return Response({'error': '查无此书'})

	def put(self, request, pk):
		"""修改一本书"""
		# 1. 获取书籍对象
		book_obj = models.Book.objects.filter(pk=pk).first()

		# 2. 使用序列化器对数据进行校验 保存
		ser_obj = BookModelSerializer(data=request.data, instance=book_obj, partial=True)
		if ser_obj.is_valid():
			ser_obj.save()
			# 3. 返回修改后的对象的json数据
			return Response(ser_obj.data)
		else:
			return Response(ser_obj.errors)

	def delete(self, request, pk):
		"""删除一本书"""
		# 1. 获取书籍对象
		book_obj = models.Book.objects.filter(pk=pk).first()
		if book_obj:
			# 2. 删除
			book_obj.delete()
			# 3. 返回json数据
			return Response({'msg': '删除成功'})
		else:
			return Response({'error': '查无此书'})


class GenericView(APIView):
	queryset = None
	serializer_class = None

	def get_queryset(self):
		# 确保每次查询都是最新的数据
		return self.queryset.all()

	def get_obj(self, request, pk, *args, **kwargs):
		return self.get_queryset().filter(pk=pk).first()


# 展示列表
class ListViewMixin(object):  # 混合类   不能单独使用  配合其他的类进行使用(利用Python的多继承)
	def list(self, request):
		queryset = self.get_queryset()
		ser_obj = self.serializer_class(queryset, many=True)
		return Response(ser_obj.data)


# 新增数据
class CreateViewMixin(object):
	def create(self, request):
		ser_obj = self.serializer_class(data=request.data)
		if ser_obj.is_valid():
			ser_obj.save()
			return Response(ser_obj.data)
		return Response(ser_obj.errors)


# 获取一个数据
class RetrieveViewMixin(object):
	def retrieve(self, request, pk):
		obj = self.get_object()
		if obj:
			ser_obj = self.serializer_class(obj)
			return Response(ser_obj.data)
		else:
			return Response({'error': '查无此ID'})


# 更新
class UpdateViewMixin(object):
	def update(self, request, pk):

		obj = self.get_object()
		ser_obj = self.serializer_class(data=request.data, instance=obj, partial=True)
		if ser_obj.is_valid():
			ser_obj.save()
			return Response(ser_obj.data)
		else:
			return Response(ser_obj.errors)


# 删除
class DestroyViewMixin(object):

	def destroy(self, request, pk):
		obj = self.get_object()
		if obj:
			obj.delete()
			return Response({'msg': '删除成功'})
		else:
			return Response({'error': '查无此ID'})


class PublisherListView(GenericView, ListViewMixin, CreateViewMixin):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


class PublisherDetialView(GenericView, RetrieveViewMixin, UpdateViewMixin, DestroyViewMixin):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


from rest_framework.generics import *

class AuthorListView(ListCreateAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


class AuthorDetailView(RetrieveUpdateDestroyAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


# class ListView(GenericView, ListViewMixin):
#
# 	def get(self, request, *args, **kwargs):
# 		return self.list(request)

from rest_framework.viewsets import GenericViewSet,ModelViewSet


class AuthorView(ModelViewSet):
	lookup_field = 'pk'  # 查询的关键字  默认 pk
	queryset = models.Author.objects.all()
	serializer_class = AuthorModelSerializer
