from stark.service.stark import site, StarkConfig
from app01 import models

site.register(models.Book)
site.register(models.Publisher)
site.register(models.Author)
