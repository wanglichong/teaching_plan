"""bookmanager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from stark.service.stark import site
from app01 import views

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^', site.urls),
	url(r'^books/$', views.BookListView.as_view(), ),
	url(r'^book/(?P<pk>\d+)/$', views.BookDetailView.as_view(), ),

	url(r'^publishers/$', views.PublisherListView.as_view(), ),
	url(r'^publishers/(?P<pk>\d+)/$', views.PublisherDetialView.as_view(), ),

	# url(r'^authors/$', views.AuthorListView.as_view(), ),
	url(r'^authors/$', views.AuthorView.as_view(actions={'get': 'list', 'post': 'create'}), ),
	url(r'^authors/(?P<pk>\d+)/$',
		views.AuthorView.as_view(actions={'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), ),

]

# from rest_framework.routers import DefaultRouter
#
# router = DefaultRouter()
# router.register(r'authors',views.AuthorView,)
# urlpatterns += router.urls
