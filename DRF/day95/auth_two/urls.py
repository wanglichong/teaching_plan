
from django.conf.urls import url,include

from auth_two import views

urlpatterns = [
	url(r'register/$',views.RegisterView.as_view()),
	url(r'login/$',views.LoginView.as_view()),
	url(r'test/$',views.TestView.as_view())

]
