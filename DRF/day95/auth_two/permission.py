from rest_framework.permissions import BasePermission


class VipPermission(BasePermission):
	message = '没有权限'

	def has_permission(self, request, view):
		# 是否登录 request.user 没有认证成功是一个匿名用户
		if not request.auth:
			return False
		# 对用户的权限进行判断
		if request.user.vip:
			return True
		else:
			return False
