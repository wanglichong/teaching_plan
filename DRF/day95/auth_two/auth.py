from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
from auth_two import models


class MyAuth(BaseAuthentication):

	def authenticate(self, request):
		# 获取用户的token
		token = request.query_params.get('token')
		if not token:
			raise AuthenticationFailed('无效参数')
		# 查询用户表中是否有相应的token
		user_obj = models.UserInfo.objects.filter(token=token).first()
		if user_obj:
			return user_obj, token
		else:
			# 没有token就认证失败
			raise AuthenticationFailed('认证失败')
