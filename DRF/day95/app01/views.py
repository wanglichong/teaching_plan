from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.mixins import ListModelMixin
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from app01 import models
from app01.serializers import PublisherModelSerializer, BookModelSerializer


class PublisherListView(ListCreateAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


class PublisherDetailView(RetrieveUpdateDestroyAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


from rest_framework.viewsets import ModelViewSet


class BookView(ModelViewSet):
	queryset = models.Book.objects.all()
	serializer_class = BookModelSerializer
