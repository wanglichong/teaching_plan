# 用户自定义的配置
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

ENGINE = 'agent'

ENGINE_DICT = {
	'agent': 'src.engine.agent.AgentHandler',
	'ssh': 'src.engine.ssh.SSHHandler',
	'salt': 'src.engine.salt.SaltHandler',
}

PLUGINS_DICT = {
	'basic': 'src.plugins.basic.Basic',
	'disk': 'src.plugins.disk.Disk',
	'memory': 'src.plugins.memory.Memory',
	'NIC': 'src.plugins.nic.NIC',
	'board': 'src.plugins.main_board.MainBoard',
	'cpu': 'src.plugins.cpu.Cpu',
}

USER = 'root'
PASSWORD = 'centos'

DEBUG = True

ASSET_API = 'http://127.0.0.1:8000/asset/'

CERT_PATH = os.path.join(BASE_DIR, 'config', 'cert')


AUTH_KEY = 'ASKJHASFBNWFP；asdf'