import logging

# logging.basicConfig(filename='log.log',
# # 					format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
# # 					datefmt='%Y-%m-%d %H:%M:%S %p',
# # 					level=10)
# #
# # logging.debug('debug')
# # logging.info('info')
#
file_handler = logging.FileHandler('l1_1.log', 'a', encoding='utf-8')
fmt = logging.Formatter(fmt="%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s")
file_handler.setFormatter(fmt)

logger1 = logging.Logger('s1', level=logging.DEBUG)
logger1.addHandler(file_handler)

logger1.info('xxxx')
logger1.error('111')


class Logger():

	def __init__(self, name, path, level=logging.DEBUG):
		file_handler = logging.FileHandler(path, 'a', encoding='utf-8')
		fmt = logging.Formatter(fmt="%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s")
		file_handler.setFormatter(fmt)

		self.logger = logging.Logger(name, level=level)
		self.logger.addHandler(file_handler)

	def info(self, msg):
		self.logger.info(msg)

	def error(self, msg):
		self.logger.error(msg)


logger = Logger('s1', 's1.log')
logger.info('info')
logger.error('error')
