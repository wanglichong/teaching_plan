from concurrent.futures import ThreadPoolExecutor
import time
# 创建一个线程池
pool = ThreadPoolExecutor(20)


def task(hostname):
	time.sleep(2)
	print(hostname)


for i in range(1, 101):
	hostname = "c{}.com".format(i)
	pool.submit(task, hostname)
