import os
from . import global_settings
import importlib


class Settings():
	def __init__(self):
		# 获取自定义的配置的路径
		conf_str = os.environ.get('USER_SETTING')
		# 获取默认的配置
		for name in dir(global_settings):  # 循环global_settings中的属性
			if name.isupper():
				# 获取默认配置中对应属性的值
				val = getattr(global_settings, name)
				# 给settings对象设置属性
				setattr(self, name, val)  # EMAIL  ，'SS@qq.com'

		# 获取自定义的配置
		module = importlib.import_module(conf_str)   # 通过路径的字符串获取到模块的对象
		for name in dir(module):
			if name.isupper():
				# 获取自定义配置中对应属性的值
				val = getattr(module, name)
				# 给settings对象设置属性
				setattr(self, name, val)


settings = Settings()
