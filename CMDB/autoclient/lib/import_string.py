import importlib

def get_class(path):
	# 'src.engine.agent.AgentHandler'
	module_str, cls_str = path.rsplit('.', maxsplit=1)
	# 获取模块
	module = importlib.import_module(module_str)
	# 反射获取到类
	return getattr(module, cls_str)


