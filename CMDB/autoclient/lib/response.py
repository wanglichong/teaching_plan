class BaseResponse:
	def __init__(self):
		self.status = True
		self.error = None
		self.data = None

	@property
	def dict(self):
		return self.__dict__


# res = BaseResponse()
# print(res.dict)
# res.status = False
# print(res.dict)

