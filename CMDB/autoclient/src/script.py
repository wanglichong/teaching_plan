from lib.conf.config import settings
from lib.import_string import get_class


def run():
	"""
	资产采集程序的入口
	:return:
	"""
	# if settings.ENGINE == 'agent':
	# 	obj = agent.AgentHandler()
	# 	obj.handler()
	# elif settings.ENGINE == 'ssh':
	# 	obj = ssh.SSHHandler()
	# 	obj.handler()
	# elif settings.ENGINE == 'salt':
	# 	obj = salt.SaltHandler()
	# 	obj.handler()
	# elif settings.ENGINE == 'ansible':
	# 	obj = ansible.AnsibleHandler()
	# 	obj.handler()
	# else:
	# 	print('不支持该模式')

	#  'src.engine.agent.AgentHandler'
	engine_path = settings.ENGINE_DICT.get(settings.ENGINE)
	cls = get_class(engine_path)  #  AgentHandler
	# 实例化对象 执行handler（）
	obj = cls()
	obj.handler()
