import sys
import os

os.environ['USER_SETTING'] = 'config.settings'  # 自定义配置的路径的字符串

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from src.script import run

if __name__ == '__main__':
	run()
