import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "autoserver.settings")

import django

django.setup()
from repository import models

#  给一个model ，拿到所有的字段
cls = models.Disk
print(cls._meta.fields)
for field in cls._meta.fields:
	print(field.name, field.verbose_name)

obj = cls._meta.get_field('slot')
print(obj.verbose_name)

info = {'slot': '0', 'pd_type': 'SAS', 'capacity': '279.396',
		'model': 'SEAGATE ST300MM0006     LS08S0K2B5NV'}


"插槽位 : 0 ; 磁盘类型: SAS ; 磁盘容量GB :279.396;磁盘型号 : SEAGATE ST300MM0006     LS08S0K2B5NV   "

tpl_list = []
for name,value in info.items():
	verbose_name = cls._meta.get_field(name).verbose_name
	tpl_list.append("{} : {} ".format(verbose_name,value))

print(tpl_list)
ret = ';'.join(tpl_list)
print(ret)
