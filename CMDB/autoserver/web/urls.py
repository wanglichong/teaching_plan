from django.conf.urls import url
from web import views

urlpatterns = [

	url(r'^index/$', views.index),
	# url(r'^business_unit/list/$', views.business_unit_list, name='business_unit_list'),
	# url(r'^business_unit/add/$', views.business_unit_change, name='business_unit_add'),
	# url(r'^business_unit/edit/(\d+)/$', views.business_unit_change, name='business_unit_edit'),

	# url(r'^server/list/$', views.server_list, name='server_list'),
	# url(r'^server/detail/(\d+)/$', views.server_detail, name='server_detail'),
	# url(r'^server/record/(\d+)/$', views.server_record, name='server_record'),
	# url(r'^server/add/$', views.server_change, name='server_add'),
	# url(r'^server/edit/(\d+)/$', views.server_change, name='server_edit'),

]
