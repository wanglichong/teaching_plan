from django.shortcuts import HttpResponse, render, redirect, reverse
from stark.service.stark import site, StarkConfig, get_choice_text, Option
from repository import models
from django.conf.urls import url
from django.utils.safestring import mark_safe


class BusinessUnitConfig(StarkConfig):
	list_display = ['id', 'name']


site.register(models.BusinessUnit, BusinessUnitConfig)


class BusinessUnitConfig1(StarkConfig):
	list_display = ['id', 'name']

	def get_queryset(self, request, *args, **kwargs):
		return self.model_class.objects.filter()

site.register(models.BusinessUnit, BusinessUnitConfig1, 'v1')


class IDCCofig(StarkConfig):
	# 要显示的字段
	list_display = [StarkConfig.display_checkbox, 'id', 'name', 'floor']
	# 排序
	order_by = ['-id']
	# 搜索
	search_list = ['name', 'floor']

	# 自定义方法
	def multi_delete(self, request):
		return HttpResponse('删除成功')

	multi_delete.text = '批量删除'
	# 批量操作
	action_list = [multi_delete]


site.register(models.IDC, IDCCofig)

from stark.forms.forms import StarkModelForm
from stark.forms.widgets import DatePickerInput


class ServerForm(StarkModelForm):
	class Meta:
		model = models.Server
		fields = "__all__"

		widgets = {
			'latest_date': DatePickerInput(attrs={'class': 'date-picker', 'autocomplete': 'off'})
		}


class ServerConfig(StarkConfig):

	# 自定义显示内容
	def show_device_status(self,row=None, header=None,):
		if header:
			return '状态'
		return row.get_device_status_id_display()

	def show_detail(self,row=None, header=None, ):
		if header:
			return '详情'
		return mark_safe('<a href="/stark/repository/server/detail/{}/">查看</a>'.format(row.pk))

	def show_record(self, header=None, row=None):
		if header:
			return '变更记录'
		return mark_safe('<a href="/stark/repository/server/record/{}/">查看</a>'.format(row.pk))

	list_display = [StarkConfig.display_checkbox, 'id', 'hostname', 'os_platform', 'cpu_physical_count', 'latest_date',
					get_choice_text('device_status_id', '设备状态'), show_detail, show_record, 'business_unit']

	search_list = ['hostname', 'business_unit__name']

	def multi_apply(self, request):
		# 公户变私户的业务
		pass

	multi_apply.text = '公户变私户'
	action_list = [multi_apply, StarkConfig.multi_delete]

	# 组合搜索
	list_filter = [
		Option('business_unit', is_multi=True, condition={'id__in': [1, 3]}),
		Option('device_status_id', is_choice=True, text_func=lambda x: x[1], value_func=lambda x: x[0], is_multi=True),
	]

	def server_detail(self, request, pk):
		server = models.Server.objects.filter(pk=pk).first()
		all_nics = server.nic_list.all().order_by('pk')
		all_memories = server.memory_list.all().order_by('slot')
		all_disks = server.disk_list.all().order_by('slot')
		return render(request,
					  'server_detail.html',
					  {'server': server,
					   'all_nics': all_nics,
					   'all_memories': all_memories,
					   'all_disks': all_disks,
					   })

	def server_record(self, request, pk):
		all_records = models.AssetRecord.objects.filter(server_id=pk).order_by('-create_at')
		return render(request, 'server_record.html', {'all_records': all_records})

	def extra_url(self):

		urlpatterns = [
			url(r'^detail/(\d+)/$', self.server_detail, name='server_detail'),
			url(r'^record/(\d+)/$', self.server_record, name='server_record'),
		]

		return urlpatterns

	model_form_class = ServerForm


site.register(models.Server, ServerConfig)
