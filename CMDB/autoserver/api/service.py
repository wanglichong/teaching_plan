from repository import models


def process_basic(info):
	server_dict = {}
	server_dict.update(info['basic']['data'])
	server_dict.update(info['board']['data'])
	server_dict.update(info['cpu']['data'])
	# 获取要修改的主机
	old_hostname = info.get('hostname') if info.get('hostname') else info['basic']['data']['hostname']
	hostname = info['basic']['data']['hostname']
	server_list = models.Server.objects.filter(hostname=old_hostname)
	server_list.update(**server_dict)
	server = models.Server.objects.get(hostname=hostname)
	return server


def process_disk(info, server):
	disk_info = info['disk']['data']  # 汇报硬盘的数据
	disk_query = models.Disk.objects.filter(server=server)  # 数据库中的硬盘数据

	disk_slot_set = set(disk_info)  # 汇报的槽位的集合
	disk_db_slot_set = {disk.slot for disk in disk_query}  # 数据库中的槽位的集合
	# 新增槽位的集合
	disk_add_set = disk_slot_set - disk_db_slot_set
	# 删除槽位的集合
	disk_del_set = disk_db_slot_set - disk_slot_set
	# 更新槽位的集合
	disk_update_set = disk_db_slot_set & disk_slot_set

	# 新增硬盘
	disk_add_list = []
	add_record_list = []
	for slot in disk_add_set:
		row_dict = disk_info[slot]

		tpl_list = []  # ['插槽位 : 0 ', '磁盘类型 : SAS ', '磁盘容量GB : 279.396 ', '磁盘型号 : SEAGATE ST300MM0006     LS08S0K2B5NV ']
		for name, value in row_dict.items():
			verbose_name = models.Disk._meta.get_field(name).verbose_name
			tpl_list.append("{} :{}".format(verbose_name, value))

		content = "新增硬盘，硬盘信息如下：{}".format(';'.join(tpl_list))
		add_record_list.append(models.AssetRecord(server=server, content=content))  # 变更记录的对象
		disk_add_list.append(models.Disk(**row_dict, server=server))  # 硬盘的对象

	if disk_add_list:
		models.Disk.objects.bulk_create(disk_add_list)
		models.AssetRecord.objects.bulk_create(add_record_list)

	# 更新硬盘
	update_record_list = []
	for slot in disk_update_set:
		row_dict = disk_info[slot]  # 新值
		disk_obj = models.Disk.objects.filter(server=server, slot=slot).first()  # 原硬盘的对象

		tpl_list = []   # '容量 由 100 变更成 200'
		update_dict = {}  # 字段名：要更新的值
		for name, value in row_dict.items():
			old_value = getattr(disk_obj, name)
			if value != str(old_value):
				update_dict[name] = value
				verbose_name = models.Disk._meta.get_field(name).verbose_name
				tpl_str = "{}由{}变成{}".format(verbose_name,old_value,value)
				tpl_list.append(tpl_str)
		if tpl_list: # 硬盘信息有变化
			content = "硬盘更新-槽位{}变更信息如下：{} ".format(slot, ';'.join(tpl_list))
			record_obj = models.AssetRecord(server=server,content=content)
			update_record_list.append(record_obj)
			models.Disk.objects.filter(server=server, slot=slot).update(**update_dict)  # 变化的字段都更新
	if update_record_list:
		models.AssetRecord.objects.bulk_create(update_record_list)

	# 删除硬盘
	if disk_del_set:
		models.Disk.objects.filter(server=server, slot__in=disk_del_set).delete()
		models.AssetRecord.objects.create(server=server, content="删除硬盘：槽位{}的硬盘被移除了 ".format(','.join(disk_del_set)))


# 更新硬盘
# for slot,row_dict in disk_info.items():
# 	if slot in disk_update_set:
# 		models.Disk.objects.filter(server=server, slot=slot).update(**row_dict)


def process_memory(info, server):
	memory_info = info['memory']['data']  # 汇报的数据
	memory_query = models.Memory.objects.filter(server=server)  # 数据库中的内存数据

	memory_slot_set = set(memory_info)  # 汇报的槽位的集合
	memory_db_slot_set = {memory.slot for memory in memory_query}  # 数据库中的槽位的集合
	# 新增槽位的集合
	memory_add_set = memory_slot_set - memory_db_slot_set
	# 删除槽位的集合
	memory_del_set = memory_db_slot_set - memory_slot_set
	# 更新槽位的集合
	memory_update_set = memory_db_slot_set & memory_slot_set

	# 新增内存
	memory_add_list = []
	for slot, row_dict in memory_info.items():
		if slot in memory_add_set:
			memory_add_list.append(models.Memory(**row_dict, server=server))
		# 更新
		elif slot in memory_update_set:
			models.Memory.objects.filter(server=server, slot=slot).update(**row_dict)

	if memory_add_list:
		models.Memory.objects.bulk_create(memory_add_list)

	# 删除内存
	if memory_del_set:
		models.Memory.objects.filter(server=server, slot__in=memory_del_set).delete()


# 更新内存
# for slot,row_dict in memory_info.items():
# 	if slot in memory_update_set:
# 		models.Memory.objects.filter(server=server, slot=slot).update(**row_dict)


def process_nic(info, server):
	nic_info = info['NIC']['data']  # 汇报的数据
	nic_query = models.NIC.objects.filter(server=server)  # 数据库中的网卡数据

	nic_name_set = set(nic_info)  # 汇报的网卡名称的集合
	nic_db_name_set = {nic.name for nic in nic_query}  # 数据库中的网卡名称的集合
	# 新增网卡名称的集合
	nic_add_set = nic_name_set - nic_db_name_set
	# 删除网卡名称的集合
	nic_del_set = nic_db_name_set - nic_name_set
	# 更新网卡名称的集合
	nic_update_set = nic_db_name_set & nic_name_set

	# 新增网卡
	nic_add_list = []
	for name, row_dict in nic_info.items():
		if name in nic_add_set:
			nic_add_list.append(models.NIC(**row_dict, name=name, server=server))
		# 更新
		elif name in nic_update_set:
			models.NIC.objects.filter(server=server, name=name).update(**row_dict)

	if nic_add_list:
		models.NIC.objects.bulk_create(nic_add_list)

	# 删除网卡
	if nic_del_set:
		models.NIC.objects.filter(server=server, name__in=nic_del_set).delete()

# 更新网卡
# for name,row_dict in nic_info.items():
# 	if name in nic_update_set:
# 		models.NIC.objects.filter(server=server, name=name).update(**row_dict)
