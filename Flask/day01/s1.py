from flask import Flask,render_template,redirect


app = Flask("DragonFire")

@app.route("/index")
def index():
	return "Hello World!"

@app.route("/home")
def home():
	return render_template("home.html")

@app.route("/r")
def goto_index():
	return redirect("/index") # 302 /index

app.run()