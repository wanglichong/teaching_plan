import json,time
from flask import Flask, jsonify, send_file

app = Flask("DragonFire")

@app.route("/get_data")
def get_data():
	# return jsonify({"name":"Alexander.DSB.Li"})
	# return "Hello World!"
	return json.dumps({"name":"Alexander.DSB.Li"})

@app.route("/get_data1")
def get_data1():
	return jsonify({"name":"Alexander.DSB.Li"})
	# return json.dumps({"name":"Alexander.DSB.Li"})

# {
# 	"code":0,
# 	"data":{
# 		"name":"Alexander.DSB.Li"
# 	}
# }

@app.route("/get_file")
def get_file():
	return send_file("2.mp3")

app.run()