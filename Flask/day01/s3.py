from flask import Flask, request, render_template

app = Flask(__name__)
app.config["DEBUG"] = True

# Request
@app.route("/url_get")
def url_get():
	print(request.args)
	print(request.args["name"])
	print(request.args.get("name"))
	user_dict = request.args.to_dict()
	user_dict["pwd"] = "123456"

	return "200 OK"


@app.route("/post_data",methods=["POST","GET"])
def post_data():
	# print(request.method)
	# print(request.path)
	# print(request.url)
	# print(request.host)
	# print(request.host_url)
	if request.method == "GET":
		return render_template("home.html")
	else:
		# print(request.data) # b""请求体中的原始数据
		# Content-Type != form-data
		# print(request.json) # None Content-Type:application/json -> dict()
		# print(request.form)
		# print(request.args)
		# print(request.values.to_dict())
		# my_file = request.files.get("my_file")
		# my_file.save()
		# print(my_file)
		# print(request.form["username"])
		# print(request.form.get("username"))
		# print(request.form.to_dict())
		return "200 OK"



if __name__ == '__main__':
	app.run()