from flask import Flask, session, render_template, request, redirect
import functools
# from functools import wraps

app = Flask(__name__)
app.config["DEBUG"] = True
app.secret_key = "~!@#$%^&*()!@#$%^&*(~!@#$%^&*(!@#$%^&*~!@#$%^&*("

STUDENT_DICT = {
	1: {'name': 'Old', 'age': 38, 'gender': '中'},
	2: {'name': 'Boy', 'age': 73, 'gender': '男'},
	3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

def war(func):
	# @functools.wraps(func) # -> inner.__name__ = func.__name__（index）
	def inner(*args,**kwargs):
		if session.get("username"):
			ret = func(*args,**kwargs)
			return ret
		else:
			return redirect("/login")
	return inner


@app.route("/index",endpoint="index")
def index():
	return render_template("s_student.html", stu=STUDENT_DICT)


@app.route("/login", methods=["GET", "POST"])
def login():
	if request.method == "POST":
		session["username"] = request.form.get("username")
		return redirect("/index")
	else:
		return render_template("s_login.html")


@app.route("/detail",endpoint="det")
def detail():
	id = int(request.args.get("id"))
	stu_info = STUDENT_DICT[id]
	return render_template("s_detial.html", stu_info=stu_info)

if __name__ == '__main__':
	app.run()
