STUDENT = {'name': 'Old', 'age': 38, 'gender': '中'}

STUDENT_LIST = [
    {'name': 'Old', 'age': 38, 'gender': '中'},
    {'name': 'Boy', 'age': 73, 'gender': '男'},
    {'name': 'EDU', 'age': 84, 'gender': '女'}
]

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

from flask import Flask,render_template,request,Markup

app = Flask(__name__)
app.config["DEBUG"] = True
# app.config["TESTING"] = True

# @app.route("/index")
# def index():
# 	return render_template("student.html",stu = STUDENT)

# @app.route("/index")
# def index():
# 	return render_template("student.html",stu_l = STUDENT_LIST)

# @app.route("/index")
# def index():
# 	return render_template("student.html",stu_d = STUDENT_DICT)

# @app.template_global()
# def ab(a,b):
# 	return a+b

@app.route("/my_func")
def my_func():
	input_str = Markup("<input type='text' name='username'>")
	return render_template("my_func.html",input_str=input_str)


if __name__ == '__main__':
	app.run()