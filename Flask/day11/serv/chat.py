import os

from bson import ObjectId
from flask import Blueprint, request, send_file, jsonify
from setting import COVER_PATH, MUSIC_PATH, MONGODB, RET

chat = Blueprint("chat", __name__)


@chat.route("/chat_list", methods=["POST"])
def chat_list():
	chat_id = ObjectId(request.form.get("chat_id"))
	chat_info = MONGODB.chats.find_one({"_id":chat_id})

	RET["CODE"] = 0
	RET["MSG"] = "查询聊天记录"
	RET["DATA"] = chat_info.get("chat_list")[-2:]

	return jsonify(RET)


@chat.route("/recv_msg", methods=["POST"])
def recv_msg():
	# 想法写出来
	# 收取消息按钮
	# 消息提醒 来自xxx的消息
	# 一下是来自xxx的消息 逐条播放 ？
	# 逐几条播放
	pass