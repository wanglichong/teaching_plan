import os

from bson import ObjectId
from flask import Blueprint, request, send_file, jsonify
from setting import COVER_PATH, MUSIC_PATH, MONGODB, RET

friends = Blueprint("friends", __name__)


@friends.route("/friend_list", methods=["POST"])
def friend_list():
	user = request.form.to_dict()
	user["_id"] = ObjectId(user.get("_id"))
	user_info = MONGODB.users.find_one(user)

	RET["CODE"] = 0
	RET["MSG"] = "好友列表查询"
	RET["DATA"] = user_info.get("friend_list")

	return jsonify(RET)