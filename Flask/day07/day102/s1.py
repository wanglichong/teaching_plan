from pymongo import MongoClient

mongo_client = MongoClient("127.0.0.1",27017)
db = mongo_client["locals"]

# 增加
# res = db.users.insert_one/insert_many
# res.inserted_id / res.inserted_ids
# 查询
# import pymongo
# res = db.users.find({}).sort("_id",-1) # 生成器对象 排序与原生语法有差异 {_id:-1},("_id",-1)
# print(list(res))
# res = db.users.find_one({}) #字典
# print(res)

# res = db.users.find({}).limit(5)
# res_list = list(res)
# print(res_list,len(res_list))

# res = db.users.find({}).skip(5)
# res_list = list(res)
# print(res_list,len(res_list))

# res = db.users.find({}).limit(2).skip(4).sort("_id",-1)
# res_list = list(res)
# print(res_list,len(res_list))

# 删除：
# db.users.delete_one({})
# db.users.delete_many({})

# 改
# res = db.users.update_one({"name":1,"hobby.detail":"喝酒"},{"$set":{"hobby.$.detail":"喝饮料"}})
# print(res)
# 在Python中操作 JSON (字典)
# res = db.users.find_one({"name":1,"hobby.detail":"喝饮料"})
# print(res)
# for index,item in enumerate(res.get("hobby")):
# 	print(item,index)
# 	if item.get("detail") == "喝饮料":
# 		res["hobby"][index]["detail"] = "喝酒"
# print(res)
#
# db.users.update_one({"name":1,"hobby.detail":"喝饮料"},{"$set":res})



