from flask import Flask, render_template
from serv import users
from serv import content
from serv import set_get_anthing
from serv import devices

app = Flask(__name__)
app.config["DEBUG"] = True
app.register_blueprint(users.user)
app.register_blueprint(content.content)
app.register_blueprint(set_get_anthing.gsa)
app.register_blueprint(devices.devices)


@app.route("/webtoy")
def webtoy():
    return render_template("WebToy.html")


if __name__ == '__main__':
    app.run("0.0.0.0",9527)