import os

from setting import LT_URL,MONGODB,QRCODE_PATH
import requests
# 建立随机码
import time
from uuid import uuid4
import hashlib

l = []

def create_qr_code(num):
	for i in range(num):
		qr_code = f"{time.time()}{uuid4()}{time.time()}".encode("utf8")
		qr_code_end = hashlib.md5(qr_code).hexdigest()
		res = requests.get(LT_URL%(qr_code_end))
		with open(f"{os.path.join(QRCODE_PATH,qr_code_end)}.jpg","wb")  as f :
			f.write(res.content)

		l.append({"device_key":qr_code_end})
	MONGODB.devices.insert_many(l)


"""
{
	"_id" : ObjectId("5c9d9e72ea512d1ae49f002e"), // 自动生成ID
	"device_key" : "afc59916257ae8a2b6ccdfb9fd273373" // 设备的唯一编号 二维码信息
}
"""

if __name__ == '__main__':
	create_qr_code(10)