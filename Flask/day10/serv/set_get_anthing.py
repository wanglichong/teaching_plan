import os

from flask import Blueprint,request,send_file
from setting import COVER_PATH,MUSIC_PATH,QRCODE_PATH
gsa = Blueprint("gsa",__name__)

@gsa.route("/get_cover/<filename>")
def get_cover(filename):
	file_path = os.path.join(COVER_PATH,filename)
	return send_file(file_path)


@gsa.route("/get_music/<filename>")
def get_music(filename):
	file_path = os.path.join(MUSIC_PATH,filename)
	return send_file(file_path)


@gsa.route("/get_qr/<filename>")
def get_qr(filename):
	file_path = os.path.join(QRCODE_PATH,filename)
	return send_file(file_path)