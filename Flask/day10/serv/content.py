from bson import ObjectId
from flask import Blueprint,request,jsonify
from setting import MONGODB,RET

content = Blueprint("content",__name__)

@content.route("/content_list",methods=["POST"])
def content_list():

	res_list = list(MONGODB.content.find({}))

	for index,item in enumerate(res_list):
		res_list[index]["_id"] = f"{item.get('_id')}"

	RET["CODE"] = 0
	RET["MSG"] = "获取内容资源列表"
	RET["DATA"] = res_list

	return jsonify(RET)
