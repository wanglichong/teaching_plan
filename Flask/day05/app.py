from flask import Flask, request, render_template, jsonify, send_file
import os
from uuid import uuid4
import my_ai
app = Flask(__name__)
app.config["DEBUG"] = True


@app.route("/toy")
def open_toy():
	return render_template("WebToy.html")

@app.route("/uploader",methods=["POST"])
def uploader():
	file = request.files.get("reco")
	filename = f"{uuid4()}.wav"
	filepath = os.path.join("chat",filename)
	file.save(filepath)

	Q = my_ai.audio2text(filepath)
	A = my_ai.my_nlp(Q)
	filename = my_ai.text2auido(A)

	return jsonify({"code":200,"chat":filename})

@app.route("/get_chat/<filename>")
def get_chat(filename):
	file_path = os.path.join("chat",filename)
	return send_file(file_path)


if __name__ == '__main__':
	app.run()
