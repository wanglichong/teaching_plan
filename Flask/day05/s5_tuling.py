import requests

data={
    "perception": {
        "inputText": {
            "text": "今天天气怎么样"
        }
    },
    "userInfo": {
        "apiKey": "9839d499aa73422382b5cbde26b044c6",
        "userId": "123" # 用唯一标识
    }
}



def to_tuling(Q):
	data["perception"]["inputText"]["text"] = Q
	res = requests.post("http://openapi.tuling123.com/openapi/api/v2", json=data)
	res_dict = res.json()

	return res_dict.get("results")[0].get("values").get("text")