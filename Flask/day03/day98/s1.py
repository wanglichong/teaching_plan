from flask import Flask,views,request

app = Flask(__name__)

@app.route("/",methods=["post"],endpoint="ooooo")
def index():
	return "200OK!"

# CBV
class Login(views.MethodView):
	methods = ["post","get","dragonfire"]
	# decorators = ["","",""]

	def dragonfire(self):
		return "300OK ! dragonfire"

	def get(self):
		return "200OK Get"

	def post(self):
		return "200OK Post"

app.add_url_rule("/login",view_func=Login.as_view(name="login"),endpoint="loging")
#,endpoint="loging"


if __name__ == '__main__':
	app.run("0.0.0.0",9527) # 1-65535
	# app.run("127.0.0.1",9527)
	# app.run("192.168.16.232",9527)
	# app.run("*.*.*.*",9527)
