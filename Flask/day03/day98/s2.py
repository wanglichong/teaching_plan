from flask import Flask, render_template, redirect, jsonify, send_file, request,session
from werkzeug.datastructures import ImmutableMultiDict
from redis import Redis

from flask_session import Session

from bp import bp

app = Flask(__name__)

# class DebugSetting(object):
# 	DEBUG = True
# 	# SECRET_KEY = "$%^&*()"
# 	PERMANENT_SESSION_LIFETIME=7
#
# app.config.from_object(DebugSetting)

app.config["DEBUG"] = True
app.config["SESSION_TYPE"] = "redis"
app.config["SESSION_REDIS"] = Redis("127.0.0.1",6379,db=10)

Session(app)


@app.route("/index", methods=["GET", "POST"])  # app.add_url_rule()
def index():
	# return "200 OK"
	# return render_template("templates.html")
	# return redirect("templates.html")
	session["username"] = "Alexander.DSB.Li"
	# de467214-2f72-43b3-b8b1-ecd9d89c4783
	return jsonify({'name': 666})  # Content-Type:application/json
	# return send_file("filename") # 打开并返回文件内容 and 自动识别文件类型 Content-Type:文件类型


# @app.route("/login",methods=["GET","POST"]) # app.add_url_rule()
def login():
	# request.method # 请求方式
	# request.args  # 获取URL中的参数数据
	# print(type(request.form))
	# request.form # type:ImmutableMultiDict # 获取 FormData 中的数据 排除 file
	# request.files # 获取 所有 file对象
	# request.files.save("path+filename") # 保存文件
	# print(request.json) # 获取当请求头中带有 Content-Type:application/json 请求体中的数据
	# print(request.data) # 获取原始请求体中的数据 b"" Content-Type中不包含formdata
	return jsonify({"code":200})




# app.default_config
app.add_url_rule("/login", view_func=login, methods=("get", "POST"))

# @app.before_request

app.register_blueprint(bp)

if __name__ == '__main__':
	app.run()
