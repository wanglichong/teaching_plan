from flask import Flask
from serv import users

app = Flask(__name__)
app.config["DEBUG"] = True
app.register_blueprint(users.user)

if __name__ == '__main__':
    app.run("0.0.0.0",9527)