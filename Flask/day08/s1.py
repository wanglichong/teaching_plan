import os

from flask import Flask, request, jsonify, send_file
from setting import db
app = Flask(__name__)
app.debug = True


@app.route("/login", methods=["POST"])
def login():
	user_info = request.form.to_dict()
	# 数据处理校验等等一系列操作
	res = db.users.find_one(user_info)
	if res:
		ret = {
			"code": 0,
			"msg": "成功"
		}
	else:
		ret = {
			"code": 1,
			"msg": "失败"
		}
	return jsonify(ret)

@app.route("/get_music/<filename>")
def get_music(filename):
	return send_file(filename)

from uuid import uuid4
@app.route("/uploader",methods=["POST"])
def uploader():
	filename = f"{uuid4()}.amr"
	file = request.files.get("my_record")
	file.save(filename)

	os.system(f"ffmpeg -i {filename} {filename}.mp3")

	return jsonify({"code":0,"msg":"上传成功","filename":f"{filename}.mp3"})


if __name__ == '__main__':
	app.run("0.0.0.0", 9527)
