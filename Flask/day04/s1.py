import json

from flask import Flask, request

from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer
from geventwebsocket.websocket import WebSocket  # 做语法提示

user_list = []

app = Flask(__name__)
@app.route("/ws")
def my_ws():
	# print(request.environ)
	user_socket = request.environ.get("wsgi.websocket") # type:WebSocket
	user_list.append(user_socket)
	print(user_list)
	while 1:
		msg = user_socket.receive() # 等待客户端发送的数据
		for u_socket in user_list: # type:WebSocket
			if u_socket == user_socket:
				continue
			u_socket.send(msg)

		# msg = json.loads(msg)
		# print(msg,type(msg))
		# user_socket.send(json.dumps(msg))





if __name__ == '__main__':
	# app.run("0.0.0.0",9527)
	server = WSGIServer(("0.0.0.0", 9527), app, handler_class=WebSocketHandler)
	server.serve_forever()
