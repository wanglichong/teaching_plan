class DebugSetting(object):
	DEBUG = True
	SECRET_KEY = "ABCDEFGHI"
	SESSION_COOKIE_NAME = "I am Not Session"
	PERMANENT_SESSION_LIFETIME = 1
	DRAGONFIRE = "666"


class TestSetting(object):
	TESTING = True
	SECRET_KEY = "@#$%^&*$%&^&**&^&$%*^T&Y*%^&&$%&&*(J"
	SESSION_COOKIE_NAME = "!@#%$^&()"
	PERMANENT_SESSION_LIFETIME = 7
	JWB = "999"



USER_INFO = [
	{"name":"1"},
	{"name":"2"},
	{"name":"3"},
]