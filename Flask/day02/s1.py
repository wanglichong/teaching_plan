import os

from flask import Flask, url_for, send_file

app = Flask(__name__)
app.debug = True

# FBV -> CBV
@app.route("/index/<path>/<file_name>",
		   methods=["GET","POST"],
		   endpoint="Alexander.DSB.Li",
		   strict_slashes=True
		   )
def index(file_name,path):
	return send_file(os.path.join(path,file_name))

@app.route("/index1",methods=["GET","POST"])
def index1():
	return "200 OK index1"





if __name__ == '__main__':
	app.run()
