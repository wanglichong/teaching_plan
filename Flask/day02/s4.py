from flask import Flask
from s4_bp import bp

app = Flask(__name__,template_folder="temp")
app.debug = True

app.register_blueprint(bp)
from s4user import s4u
app.register_blueprint(s4u)
from s4user_add import s4ua
app.register_blueprint(s4ua)

if __name__ == '__main__':
    app.run()