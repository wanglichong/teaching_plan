from flask import Blueprint,render_template
from setting import USER_INFO

s4u = Blueprint("s4u",__name__)

@s4u.route("/user_list")
def user_list():
	return render_template("s4user_list.html",u=USER_INFO)