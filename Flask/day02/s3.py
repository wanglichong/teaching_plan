from flask import Flask, url_for, send_file, session, redirect, request

app = Flask(__name__)
app.debug = True
app.secret_key = '$%^&*('

@app.before_request
def be1():
	print("be1")
	if request.path == "/login":
		return None
	if session.get("username"):
		return None
	else:
		return redirect("/login")

@app.before_request
def be2():
	print("be2")


@app.after_request
def af1(response):
	print("af1")
	return response

@app.after_request
def af2(response):
	print("af2")
	return response


@app.errorhandler(404)
def error404(error_msg):
	print(error_msg)
	return send_file("setting.py")


# FBV -> CBV
@app.route("/login")
def login():
	session["username"] = "567890"
	return "login"

@app.route("/index",methods=["GET","POST"])
def index():
	return "200 OK index"


if __name__ == '__main__':
	app.run()
