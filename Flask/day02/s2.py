from flask import Flask, render_template, session, jsonify

app = Flask(__name__,
			template_folder='temp',
			static_folder='jingtaiwenjianmulu',
			static_url_path="/static"
			)
from setting import DebugSetting
from setting import TestSetting
app.config.from_object(DebugSetting)
# app.config.from_object(TestSetting)

#
# app.config["DEBUG"] = True
# # app.default_config
#
# app.config["SECRET_KEY"] = "$%^&*()"
# app.config["SESSION_COOKIE_NAME"] = "I am Not Session"
# app.config["JSONIFY_MIMETYPE"] = "Dragon/Fire"
# app.config["PERMANENT_SESSION_LIFETIME"]
@app.route("/")
def index():
	session["user"] = "SESSION_COOKIE_NAME"
	return jsonify({"name":"66666"})


if __name__ == '__main__':
    app.run()