from flask import Flask,request,render_template
from s1 import db

app = Flask(__name__)
app.debug = True

@app.route("/reg",methods=["GET","POST"])
def reg():
	if request.method == "GET":
		return render_template("reg.html")
	else:
		# username = request.form.get("username")
		# password = request.form.get("password")
		user_info = request.form.to_dict()
		res = db.users.insert_one(user_info)
		print(res.inserted_id)
		return "200 OK"



if __name__ == '__main__':
    app.run()

