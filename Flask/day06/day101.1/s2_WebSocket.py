# 1.websocket 是长连接
# 2.客户端 + 服务器 同时保持连接
from flask import Flask, request

from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer
from geventwebsocket.websocket import WebSocket

app = Flask(__name__)


@app.route("/ws") # 协议已经不是HTTP协议了 而是 Websocket协议
def mywebsocket():
	user_socket = request.environ.get("wsgi.websocket")
	print(user_socket)
	return "200"
	# 大爷打电话
	# 小王儿，去传达室找大爷，告诉大爷自己的电话号码
	# 大爷说，你先回去，记着 天王盖地虎
	# 小王儿接到大爷的电话，天王盖地虎 ，大爷说身份验证成功
	# 请不要挂电话





if __name__ == '__main__':
	serv = WSGIServer(("0.0.0.0", 9527), app,handler_class=WebSocketHandler)
	serv.serve_forever()


