from flask import Flask, session
from flask_session import Session
from redis import Redis

app = Flask(__name__)
app.default_config

app.config["DEBUG"] = True
app.config["SESSION_TYPE"] = "redis"
app.config["SESSION_REDIS"] = Redis(db=10)

app.session_interface


app.secret_key = "#$%^&*()"

app.session_interface

Session(app)


@app.route("/login")
def login():
	session['user'] = "username"
	# session.get("user")
	return "200OK"


if __name__ == '__main__':
	app.run()
