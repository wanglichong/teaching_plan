# day61

## 内容回顾

### 1. url conf

```python
from django.conf.urls import url

urlpatterns = [
	url(r'^blog/$', views.blogs, name='xxxx'),
	url(r'^blog/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blog, name='blog'),
	url(r'home', views.home, name='home')
]
```

### 2. 正则表达式

url最初步加/   r''

^    $   [sdf]    [0-9]   [a-z]{}    \d   +   ?   * 

### 3. 分组和命名分组

捕获的参数是字符串的类型

```python
url(r'^(bl)og/$', views.blogs, name='xxxx'),    # ——》 将捕获的参数当做  位置参数  传递给视图 
```

```python
url(r'^blog/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blog, name='blog'),
# 将捕获的参数当做  关键字参数  传递给视图 
```

### 4. include  路由分发

```Python
urlpatterns = [
   url(r'^admin/', admin.site.urls),
   url(r'^app01/', include('app01.urls')),
   url(r'^app02/', include('app02.urls')),
]
```

### 5. url命名和反向解析

静态地址

```
url(r'^blog/$', views.blogs, name='xxxx'),
```

模板：

​	{% url  'xxxx'  %}     ——》  ‘/app01/blog/’

py文件：

​	from django.urls import reverse

​	reverse('xxxx')       ——》    ‘/app01/blog/’

分组

​	url(r'^blog/([0-9]{4})/([0-9]{2})/$', views.blog, name='blog'),

模板：

​	{% url  'blog'  '1998' '12' %}       ——》   '/blog/1998/12/'

py文件：

​	reverse('blog', args=('1199','02') )     ——》   '/blog/1199/02/'

命名分组

​	url(r'^blog/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blog, name='blog'),

模板：

​	{% url 'blog'  '2018'  '06'  %}     ——》  '/blog/2018/06/'    位置传参

​	{% url 'blog'  month='06'   year='2010'  %}     ——》  '/blog/2010/06/'    关键字传参

py文件：

​	reverse('blog', args=('1199','02') )     ——》   '/blog/1199/02/'

​	reverse('blog',kwargs={'year':'2018','month':'08'})      ——》    '/blog/2018/08/'

### 6. namespace

```python
urlpatterns = [
   url(r'^admin/', admin.site.urls),
   url(r'^app01/', include('app01.urls',namespace='app01')),
   url(r'^app02/', include('app02.urls',namespace='app02')),
]
```

反向解析时  都加上namespace   

{%  url  'app01:xxxx'  %}         namespace1:namespace2:name

reverse('app01:xxxx')

7. 传递参数

url(r'^blog/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blog, {'year':'2018'}，name='blog'),

def blog(request,year='2018')



## 今日内容

#### 1. ORM字段和字段的参数

常用的字段

​	**AutoField**

​	**IntegerField**

​	**CharField**

​	**DateField**

​	**DatetimeField**

​	**BooleanField**

​	**NullBooleanField**	

​	**TextField**

​	**FloatField**

​	**DecimalField**

参数

 1. null  = Ture    数据库中可以为空

 2. blank = Ture    表单中可以为空

 3. default   默认值

 4. unique 唯一

 5. verbose_name 中文提示

 6. choices   可供选择的值

#### 2. admin的使用

 1. 创建超级用户

    python manage.py createsuperuser

    用户名和密码    密码 数字+字母+至少8位

2. 注册model

   在app下的admin.py中写

   ```
   from app01 import models
   admin.site.register(models.Person)
   ```
3. 访问 http://127.0.0.1:8000/admin/

4. ![1554776739687](assets/1554776739687.png)
5. ![1554776767637](assets/1554776767637.png)
6. ![1554776902261](assets/1554776902261.png)

#### 3. 表的参数

```
class Person(models.Model):
   pid = models.AutoField(primary_key=True)
   name = models.CharField(max_length=32, db_column='username', unique=True, verbose_name='姓名')  # varchar(32)


   class Meta:
      # 数据库中生成的表名称 默认 app名称 + 下划线 + 类名
      db_table = "person"
		
	  # 排序
	  ordering = ('pk',)
      
      # 联合索引 
      index_together = [
         ("name", "age"),
      ]

      # 联合唯一索引
      unique_together = (("name", "age"),)
```

#### 4. ORM的查询（13条）  必知必会13条

```python
返回的是对象列表 (8)
all
filter  
exclude
order_by 
reverse
values   
values_list 
distinct 

返回的是对象 (3)
get  
first  
last

返回布尔值 （1）
exists 

返回数字  （1）
count  
```

#### 5. 单表的双下划线

```python
ret = models.Person.objects.filter(pk=1)
ret = models.Person.objects.filter(pk__gt=1)  # greater than   大于
ret = models.Person.objects.filter(pk__lt=3)  # less than    小于
ret = models.Person.objects.filter(pk__gte=1)  # greater than equal   大于等于
ret = models.Person.objects.filter(pk__lte=3)  # less than  equal     小于等于

ret = models.Person.objects.filter(pk__range=[1, 3])  # 范围   左右都包含
ret = models.Person.objects.filter(pk__in=[1, 5])  # in

ret = models.Person.objects.filter(name__contains='alex')  #  like  完全包含
ret = models.Person.objects.filter(name__icontains='ale')  #  like   忽略大小写  ignore

ret = models.Person.objects.filter(name__startswith='a')  #  以a开头
ret = models.Person.objects.filter(name__istartswith='a')  #  以a开头


ret = models.Person.objects.filter(name__endswith='a')  #  以a开头
ret = models.Person.objects.filter(name__iendswith='g')  #  以a开头

ret = models.Person.objects.filter(phone__isnull=True)  #  phone字段为空
ret = models.Person.objects.filter(phone__isnull=False)  #  phone字段不为空


ret = models.Person.objects.filter(birth__year=2019)  #  phone字段不为空
ret = models.Person.objects.filter(birth__contains='2019-04-09')  #  phone字段不为空
# ret = models.Person.objects.filter(birth__month=4)  #  phone字段不为空
```

#### 6. 外键的操作

```Python
from app01 import models

# 基于对象的查询

# 正向查询
book_obj = models.Book.objects.get(pk=1)

book_obj.publisher        # 书籍所关联的对象
book_obj.publisher.name   # 书籍所关联的对象的名字
book_obj.publisher_id     # 书籍所关联的对象的ID

# 反向查询
pub_obj = models.Publisher.objects.get(pk=1)

# 外键中 不指定related_name    pub_obj.表名小写_set  关系管理对象
pub_obj.book_set    #  表名小写_set   关系管理对象
pub_obj.book_set.all()   # 出版社所关联的所有的书籍

# 外键中 指定related_name='books'    pub_obj.books  关系管理对象
pub_obj.books     
```

#### 7. 多对多的操作

见代码

