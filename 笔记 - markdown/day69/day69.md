# day69

## 内容回顾

1.添加编辑客户

2.公户和私户的展示

3.公户的私户的转换

## 今日内容

1.模糊查询（搜索）

```
from django.db.models import Q

filter(Q(Q(qq__contains=query) | Q(name__contains=query)))

q = Q()   # 定义一个Q对象
q.connector = 'OR'    # 规定Q对象中的元素的关系  OR  AND
q.children  #  []

Q(qq__contains=query)    ==  Q(('qq__contains',query))

```

2.分页保留原搜索条件

```
from django.http.request import QueryDict

dic = QueryDict()     # 不能修改的对象
dic._mutable=Ture 

dic = QueryDict(mutable=Ture)     # 可以修改的对象


dic['page'] = 1
dic['query'] = 'alex'
dic.urlencode()   #  page=1&query=alex

QueryDict().copy()   # 一个可以修改的深拷贝的对象

```

3.新增和编辑后跳转到原页面

http://127.0.0.1:8000/crm/edit_customer/6/?next=/crm/customer_list/?query=123&page=2

'next=/crm/customer_list/?query=123&page=2'

next = /crm/customer_list/?query=123

page =2



4.跟进记录的管理

限制choices的选择

```
obj = models.ConsultRecord(consultant=request.user_obj)

form_obj = ConsultForm(instance=obj)


self.fields['customer'].choices = [('','-----------'),]+[(i.pk, str(i)) for i in self.instance.consultant.customers.all()]

self.fields['consultant'].choices = [(self.instance.consultant.pk,self.instance.consultant)]


```

5.报名表的管理

