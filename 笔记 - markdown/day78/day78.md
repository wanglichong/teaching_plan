# day77

## 内容回顾

Linux复习：

1.linux是个操作系统，服务器的操作系统，centos7系列的发行版
2.windows机器上，安装虚拟化软件vmware，在vmware软件里，安装linux操作系统
3.vmware如同服务器，我们选择远程登录，使用的工具是 xshell



linux命令会议：


su 切换用户
mkdir   创建文件夹
cd   切换目录
touch   创建普通文件
cat  查看文本内容
vim   文本编辑器
ls   查看文件夹内容
ll   等于 ls   -l 
ps -ef   查看linux的进程
top    linux的任务管理器
find  查找linux文件的
grep   过滤字符串信息的
pwd   打印当前工作目录的绝对路径
mv   移动文件 ，重命名
rm  删除文件  rm -rf    -r  递归删除文件夹  -f  强制不提醒就删除
yum   linux安装软件的命令，如同pip
head   从文本前*行开始看，默认前10行
tail  从文本后面10行看      tail  -f  filename   实时监控文件内容
more  翻页显示文件内容
less  翻页显示文件内容
echo  相当于print打印

/        在路径的最前面是根目录，在路径中间是，路径分隔符
-  		上一次的工作目录
     ~	 当前登录用户的家目录
     .		当前目录 
     ..		上一级工作目录  
     ./		 当前目录 
>       重定向输出符，覆盖写入。。。
>       >		重定向输出符，追加写入 
>       >		<		重定向输入符，覆盖写入
>       >		<<  	重定向输入符，追加写入    

linux的变量赋值不得有空格，有空格就识别为参数了



wget   在线下载url资源
alias   linux的别名命令    
			alias  rm="echo 你别使用rm了，你这个笨蛋"

scp    linux之间网络传输文件的

chattr +a  filename  加锁，让文件不得删除
chattr -a  filename  减锁

lsattr   filname  显示文件是否有锁


linux命令学习day3：

1.管理网络的命令

	如果发现自己没有ip地址
	1.保证vmware的网络小电脑亮着，如同插上了网线
	2.管理网络的文件夹，路径
	/etc/sysconfig/network-scripts/
	3.查看管理网络的文件内容
	ifcfg-ens33  这个就是网卡的配置文件了
	4.确保一个参数是yes
	ONBOOT=yes  #代表机器启动时，就加载ip地址
	5.通过命令重启网卡
	systemctl restart network  
	系统服务管理命令  重启   网络服务


2.启停网卡的快捷命令
ifup 
ifdown 

3.linux的用户权限篇

	qq群的角色分配：
	超级用户：群主    root  
	拥有超级用户权限的人：管理员   sudo  加上你的命令，临时提权的 
	渣渣用户：普通成员  linux的普通用户成员



	权限的目的：保护文件信息安全的


	创建用户
	useradd 
	更改用户密码
	passwd 
	
	root用户就是皇帝，他的皇宫
	/root
	普通用户，只有一个小破房子，还是统一管理地方/home 
	比如有一个kun普通用户，/home/kun
	
	普通用户的信息都存放到了/etc/passwd 

4.查看linux用户的身份id信息

id kun  #查看用户身份id  

[root@localhost home]# id kun
uid=1002(kun) gid=1002(kun) groups=1002(kun)
uid 用户id号码
gid   group id  用户组 id
groups  组id号码



系统超级用户 uid  默认是 0
系统常用服务的用户  系统默认会创建mysql用户，去执行mysql这个软件，他的id是从1-999之间
root创建的普通用户，默认id是从1000开始的


用户组 ，一组同样身份信息的用户
用户 ，普通用户

5.root可以随意更改别人的密码，普通用户不行

6.用户切换
root可以随意切换普通用户
普通用户切换，必须想要登录用户的输入密码

7.临时提权的命令sudo 
	1.修改sudoers配置文件，把你想提权的用户写进去
	编辑配置文件
	vim  /etc/sudoers

	2.写入如下信息，定位到那一行
	## Allow root to run any commands anywhere 
	root    ALL=(ALL)       ALL
	kun    ALL=(ALL)       ALL   #允许kun在任何地方，执行任何命令 
	
	3.使用sudo命令
	
	sudo 你想执行的命令

8.删除用户
userdel -r  用户名 #删除用户信息，和家目录


9.linux的文件权限

	文件拥有者分三类
	属主
	属组
	其他人
	
	-rw-r--r--. 1 root root 0 May  5 09:42 真好玩.txt


	文件的读写执行，命令是？
	cat  读
	vim   echo追加写
	./文件    直接运行可执行文件 ，以绝对路径和相对路径
	sh shell脚本  #用shell解释器去读脚本
	
	文件夹的读写执行？
	ls  读
	touch  允许进入文件夹，写文本
	cd   允许进入文件夹，可执行
	
	linux文件的分类
	-  普通文本
	d  文件夹 
	l  软连接


	文件/文件夹权限
	r    read可读，可以用cat等命令查看
	w    write写入，可以编辑或者删除这个文件
	x    executable    可以执行

10.更改文件权限

chmod  权限   文件/文件夹权限 

-rw-r--rw-. 1 root root 79 May  5 09:58 你好.txt

-rw-r--rw-  
- 普通文本 
  rw-  指的是root用户 可读可写不可执行    users     u  
  r--   指的是root组里的成员，只读         group     g
  rw-   指的是其他人  可读可写，不可执行   others   o  

#让你好.txt没有任何的权限
chmod u-r,u-w,u-x  你好.txt 
chmod g-r,g-w,g-x  你好.txt 
chmod o-r,o-w,o-x  你好.txt 

#让你好.txt 所有角色都有所有的权限
-rwxrwxrwx. 1 root root 79 May  5 09:58 你好.txt
chmod 777  你好.txt  #赋予文本最高的权限
权限分为
r 4 
w 2 
x 1  

权限计算最高是 4+2+1 =7  最低是 0

#练习chmod
---xr---wx. 1 root root 79 May  5 09:58 你好.txt
-  0+0+1  4+0+0  0+2+1 
  chmod 143  你好.txt 


-r--r---wx. 1 root root 79 May  5 09:58 你好.txt
chmod 443 你好.txt 



chmod 666 你好.txt 
-rw-rw-rw-   


chmod  456  你好.txt 
-r--r-xrw-  你好

chmod 033 你好.txt 
-----wx-wx  你好.txt 


chmod 756 你好
drwxr-xrw-  你好 




11.更改文件的属主，属组

chown  用户名  要操作的文件 #更改文件属主
chgrp  组名   要操作的文件 #更改文件属组


12.软连接语法

ln   -s   目标文件  快捷方式绝对路径

13.linux的命令提示符
PS1变量

echo $PS1  #显示命令提示符

修改命令提示符

PS1="[\u@\h \w \t]\$"


14.linux的打包，解压缩的命令
	tar命令

	-c  打包
	-x  解包
	-z  调用gzip命令去压缩文件，节省磁盘空间
	-v  显示打包过程


	语法：
	tar  -cvf  打包文件的名字   你要打包的内容
	
	#实例：
	#压缩当前的所有内容到alltmp.tar这个文件中，这里不节省磁盘 
	tar  -cvf  压缩文件的名字.tar   ./*
	#解压的方式
	tar  -xvf  压缩文件的名字.tar


	#打包文件，并且压缩文件大小的用法，节省磁盘
	tar  -zcvf  压缩文件的名字.tar.gz  ./*  
	#解压缩
	tar -zxvf  压缩文件的名字.tar.gz  

15.django程序跑起来，如何检测呢？
	1.去浏览器检测是否可以访问,192.168.16.37:8000
	2.确认django的端口是否启动
	netstat -tunlp  | grep 8000

	3.确认django的进程是否存在 
	ps -ef |grep python 

16.杀死进程的命令
kill 进程id

17.支持正则的kill命令
(慎用,,)
(慎用,,)
(慎用,,)
pkill   进程的名字 


18.显示磁盘空间 df -h 

19.什么是dns（域名解析系统）,其实就是一个超大的网络电话簿  
	dns就是域名解析到ip的一个过程，

	大型公司，用的dns服务名叫做 bind软件
	提供dns服务的公司有
	119.29.29.29  腾讯的
	223.5.5.5	阿里的
	223.6.6.6   阿里的 
	8.8.8.8		谷歌的
	114.114.114.114 	114公司的
	
	linux的dns配置文件如下
	vim /etc/resolv.conf 
	里面定义了dns服务器地址
	
	linux解析dns的命令
	nslookup  域名 


​	
20.dns解析流程，当你在浏览器输入一个url，有了那些解析

pythonav.cn:80/index.html
解析流程：
1.浏览器首先在本地机器，操作系统dns缓存中查找是否有域名-ip的对应记录
2.去/etc/hosts文件中寻找是否写死了域名解析记录
3.如果hosts没写，就取 /etc/resolv.conf配置文件中寻找dns服务器地址
4.如果找到了对应的解析记录，就记录到本地dns缓存中

/etc/hosts 本地强制dns的文件


21.linux的定时任务 
分 时  日  月 周 
*   *   *  *  *   命令的绝对路径


#每分钟执行一次命令

分 时  日  月 周 
*   *   *  *  *   命令的绝对路径





#每小时的3,15分钟执行命令

分 时  日  月 周 
*   *   *  *  *   命令的绝对路径
                3,15   *  *   *   *   命令的绝对路径



#在晚上8-11点的第3和第15分钟执行
分 	时  日  月 周 
*   	*   *  *  *   命令的绝对路径

3,15   20-23 *  *  *    命令的绝对路径






#每晚21:30执行命令

分 	时  日  月 周 
*   	*   *  *  *   命令的绝对路径
                 30   21  *   *  *  



#没周六、日的1：30执行命令

分 	时  日  月 周 
*   *   *  *  *   命令的绝对路径
                30   1     *  *   6,0   命令的绝对路径





#每周一到周五的凌晨1点，清空/tmp目录的所有文件

分 	时  日  月 周 
*   *   *  *  *   命令的绝对路径

00  1   *   *   1-5   	/usr/bin/rm -rf  /tmp/*  





#每晚的21:30重启nginx
分 	时  日  月 周 
*   *   *  *  *   命令的绝对路径
                30  21   *  *  *   /usr/bin/systemctl restart nginx 



#每月的1,10,22日的下午4:45重启nginx


分 	时  日  月 周 
*   *   *  *  *   命令的绝对路径
                45   16  1,10,22  *  *  /usr/bin/systemctl restart nginx




#每个星期一的上午8点到11点的第3和15分钟执行命令

分 		时  日  月 周 
*  	 	*   *  *  *   命令的绝对路径
                  3,15  8-11   *  *  1   



22.linux的软件包管理
	linux的软件包格式是rpm格式的

	安装软件的方式(了解即可)
	1.yum安装 如同pip(自动搜索你想要的软件包，以及它的依赖关系，自动解决下载)
	2.源代码编译安装 
	3.手动安装rpm包（需要手动解决依赖关系，很恶心，不用）


	rpm命令的使用方式：
	安装软件的命令格式                rpm -ivh filename.rpm     # i表示安装   v显示详细过程  h以进度条显示
	升级软件的命令格式                rpm -Uvh filename.rpm
	卸载软件的命令格式                rpm -e filename.rpm
	查询软件描述信息的命令格式         rpm -qpi filename.rpm
	列出软件文件信息的命令格式         rpm -qpl filename.rpm
	查询文件属于哪个 RPM 的命令格式 　 rpm -qf filename

linux配置阿里云的yum源，配置步骤如下

1.备份默认的yum仓库
linux的yum仓库地址：/etc/yum.repos.d/xx.repo 
在这个目录的第一层文件夹下，名字以*.repo结尾的都会别识别为yum仓库 
cd /etc/yum.repos.d  
mkdir backrepo
mv * ./backrepo  


2.找到阿里的镜像站
https://opsx.alibaba.com/mirror


3.通过命令，在线下载yum仓库源
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo


下载第二个源，epel源，你想要的很多工具都在这
#在线下载repo仓库文件，并且重命名，放到/etc/yum.repos.d/这个目录下
-O 就是 改名，且指定位置
wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo

4.测试安装mariadb数据库(其实就是mysql)

yum install mariadb-server mariadb -y  #安装数据库
5.启动数据库
systemctl start mariadb  
6.可以测试访问mysql数据库了
mysql -uroot -p 

7.练习运行crm代码






系统服务管理命令，只有通过yum安装的软件，才可以用系统服务管理命令
/opt/nginx112/


1.如果是centos6就是 
service nginx start 
2.如果是centos7 就是
systemctl  start/stop/restart  nginx 

















 











​	





 