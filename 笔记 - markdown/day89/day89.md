## 内容回顾

技术点 + 流程：

```python
客户端
1. 兼容三种模式 可扩展
	- agent
    - ssh
    - salt 
    
    - 配置
    	ENGINE = 'agent'
        
        ENGINE_DICT = {
            'agent': 'src.engine.agent.AgentHandler',
            'ssh': 'src.engine.ssh.SSHHandler',
            'salt': 'src.engine.salt.SaltHandler',
        }

	模块导入  importlib
	importlib.import_moudle('模块的路径')
    反射  获取模块里的类
2. 资产采集的插件   可插拔式
	- 配置
    	PLUGINS_DICT = {
            'disk': 'src.plugins.disk.Disk',
            'memory': 'src.plugins.memory.Memory',
            'NIC': 'src.plugins.nic.NIC',
        }
        
     - 执行process方法   基类
     - 支持 windows 和 Linux 操作系统的命令
        
 3. 类的继承 + 类的约束 
	继承 + 抛出异常 
 
 4. debug模式
	本地测试的时候 读取文件
    
 5. 线程池
	from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(20)
    pool.submit(task,agrs)
    
 6. 类相关
	dir()  获取对象里的属性
    __dict__   {}  对象的属性和值
    
 7. 发请求
	requests  
    requests.get(url)
    ret=requests.post(url,data,json={},headers={})
    ret.text   文本
    ret.json()   反序列化
    
 8. 错误的处理
	import traceback
    	try:
			
			with open(os.path.join(self.base_dir, 'files', 'disk')) as f:
		
		except Exception as e:
			ret = traceback.format_exc()
            
 
服务端
api   -  CBV
djangorestframework    rest_framework
from rest_framework.views import APIView
from rest_framework.response import Response

request.data   # 提交的数据    自动做解析  Content-Type':'application/json

INSTALLED_APPS = [
    'rest_framework'
]

不做CSRF校验
from django.views.decorators.csrf import csrf_exempt
CBV   ——》 加在dispatch上




```

唯一标识

资产： 物理机   —— 》 sn

物理机 + 虚拟机 ：

  1. 只采集 物理机   +  接口   —— sn号

  2. 主机名

     流程：

      1. 没有文件   ——  》  新增的机器

           1. 汇报到api   新增到数据库
           2. 回复结构中有 主机名
           3. 新建文件  把主机名保存到文件中

      2. 修改

          1. 汇报之前 先读取文件 拿到原来主机名

          2. 判断文件中的主机名和资产采集到主机名做对比：

              1.  相等  —— 主机名没有修改

                 汇报资产信息 —— >api

                 api 更新当前主机的资产

             2. 不相等  —— 主机名被修改

                汇报资产信息 、原来的主机名 —— >api

                api 找到原来的主机  改主机名 更新资产信息

             

          

