# day59

## 内容回顾

### 1.for

{% for  i in name_list %}

​	{{ i }}     {{ forloop }}

{% endfor %}

counter   从1开始

counter0   从0开始

revcounter   到1结束

revcounter0   到0结束

first   last   布尔值  

parentloop  

{% for  i in name_list %}

​	{{ i }}     {{ forloop }}

{% empty %}

{% endfor %}

### 2.if

{% if 条件 %}

{% elif 条件 %}

{% else %}

{% endif %}

注意： 不支持连续判断    不支持算数运算      10>5 >1     

### 3.with

{% with bian  as sss %}

​	{{ sss   }}

{% endwith %}

### 4.csrf_tonken

{% csrf_tonken  %}    放在form表单中    一个隐藏的input 标签   name='csrfmiddlewaretoken' 

### 5.母版和继承

母版：  

​	一个普通的HTML文件，提取到多个页面的公共部分，在里面定义block块

继承：

 	1. {%  extends  '母版的文件名字' %}
 	2. 复写block块中的内容

注意点：

 	1. {%  extends  '母版的文件名字' %} 在第一行 ，上面不要有内容
 	2. {%  extends  '母版的文件名字' %}     '母版的文件名字'   带引号  不带回当做变量
 	3. 内容写在block块中
 	4. 母版中多定义点block块   css  js  

### 6.组件

一小段HTML代码     nav.html

{% include 'nav.html' %}

### 7.静态文件相关

{%  load static %}

{%  static  '静态文件的相对路径' %}

{% get_static_prefix %}    ——》 获取STATIC_URL 值

8.自定义方法

filter  、 simple_tag 、 inclusion_tag

1. 在app下创建名为templatetags的Python包

2. 在包内创建py文件    ——》 my_tags.py

3. 在py文件中写代码：

   ```python
   from django import template
   register = template.Library()    #  register名字不能错
   ```

4.  写函数  + 装饰器

   ```python
   @register.filter(name='xxx')
   def add_str(value,arg):
       return True 
       
   @register.simple_tag
   def str_join(*args.**kwargs):
       return False
   
   @register.inclusion_tag('page.html')
   def page(num,now=None):
       return {'num':range(1,num+1),'now':now}
   ```

5. inclusion_tag还需要些模板

   ```html
   {% for i in num %}
   	{{ i }}
   {% endfor %}
   
   ```

6.  使用

   ```html
   {% load my_tags %}
   
   {{ 'alex'|xxx:'dsb'  }}    ——》 True
   
   {% str_join 'x' a k1='v1' k2='v2' %}    ——》 False
   
   {% page 3 %}   
   1
   2
   3
   ```

![1554339900128](assets/1554339900128.png)



##  今日内容

### 1. CBV FBV

FBV   funcation  based  view 

CBV  class based  view 

##### 定义

```python
from django.views import View

class AddPublisher(View):
   
   def get(self,request):
      # get请求
      return response
   
   def post(self,request):
      # post请求
      return response
```

##### 使用

```python
url(r'^add_publisher/', views.AddPublisher.as_view()),
```

##### as_view()的流程

 1. 程序启动的时候，就执行as_view（）   定义view函数并返回 

    ```
    url(r'^add_publisher/', view),
    ```

2. 请求到来的时候，执行view函数：
   1. 实例化当前的类  ——》 self
   2. self.request = request
   3. 执行self.dispatch的方法：
      1. 判断请求方式是否别允许：
         1. 允许    通过反射拿到对用请求方式的方法  —— 》 handler
         2. 不允许  self.http_method_not_allowed  ——》 handler
      2.  执行handler    得到 HttpResponse对象  返回



##### 使用装饰器

FBV     直接加在函数上

CBV 

​	from django.utils.decorators import method_decorator

 1. 加在方法上

    ```
    @method_decorator(timer)
    def get(self, request):
    ```

2. 加在dispatch方法上

```
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs):
```

3. 加在类上

```
@method_decorator(timer,name='post')
@method_decorator(timer,name='get')
class AddPublisher(View):
```

```
@method_decorator(timer,name='dispatch')
class AddPublisher(View):
```



不使用method_decorator：

args   (<app01.views.AddPublisher object at 0x03B465B0>, <WSGIRequest: GET '/add_publisher/'>)



不使用method_decorator：

args   (<WSGIRequest: GET '/add_publisher/'>,)

### 2. request

```Python
属性
request.method
request.GET  # URL上携带的参数
request.POST  # POST请求提交的数据
request.FILES  # 上传的文件

request.path_info   # 路径  不包含IP和端口 参数
request.body        # 请求体   请求数据  get没有请求体
request.META       # 请求头的信息
request.COOKIES    
request.session

方法
request.get_full_path()    # 路径  不包含IP和端口 包含参数
request.is_ajax()          # 是否是ajax请求
```

### 3. 上传文件

 	1. form的编码方式  enctype="multipart/form-data"
 	2. request.FILES  
 	3. 文件对象的  f1.chunks()

### 4. response

HttpResponse('字符串')    ——》 返回的字符串

render(request,'模板的文件名',{k1:v1})    ——》  返回一个完整的HTML页面

redirect(要跳转的地址)    —— 》重定向       Location ：地址

```
from django.http.response import JsonResponse

JsonResponse(data)    # Content-Type: application/json
JsonResponse(data,safe=False)  # 传输非字典类型
```







