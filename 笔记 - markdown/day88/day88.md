## 内容回顾

CMDB   Configuration Management Database

背景  ——  运维自动化平台

- 自动装机
- 配置管理
- 监控
- 堡垒机
- 代码发布

CMDB （资产信息自动采集）

思路：

兼容三种： agent  ssh  salt  

代码架构

 - client（客户端）
 - api(服务端)
 - 管理后台

技术点：

​	os.environ  环境变量

​	sys.path  模块导入的路径

​	执行三种不同的模式  可扩展

​      	-配置    dir 

  - 字符串导入模块    importlib.import_module

  -  反射

    类的约束

    ​	-抽象类 抽象方法 abc

    ​	-继承 + 抛出异常

​	资产采集插件的可插拔的设计

​		-配置

​	三种模式执行命令的方式不同

​	agent   subprocess

​	ssh    paramiko

​	salt   saltstack 

​	

## 今日内容

#### 兼容linux和Windows的命令

#### debug模式

#### handler的流程

agent：

1. 执行AgentHandler.handler 
2.  根据系统版本执行不同的命令  

 	3.  获取资产信息 ——
	4.  发送到API

SSH salt： 

1. 执行SshAndSaltHandler.handler      

2.  获取未采集的服务器列表

3. 多线程获取资产信息 发发送到API

API

​	csrf相关的装饰器

​	from django.views.decorators.csrf import csrf_exempt ，csrf_protect



csrf_exempt 只能加在dispatch方法上	

```
@method_decorator(csrf_exempt, name='dispatch')
class Asset(View):
```



pip install djangorestframework 



