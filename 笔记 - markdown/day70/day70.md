# day70

## 内容回顾

1.crm

 	客户关系管理系统

2.使用人员

​	销售   班主任  财务  老板  管理员 项目经理  讲师

3.业务和技术点

- 登录 注册 注销   -  认证

  - 注册   

    - modelform    校验两次密码一致 + 密码加密

      ```python
      from django import forms
      from crm import models
      class RegForm(forms.ModelForm):
          
          password = forms.CharField(min_length=6, widget=forms.PasswordInput(attrs={'placeholder': '您的密码'}))
      	re_password = forms.CharField(min_length=6, widget=forms.PasswordInput(attrs={'placeholder': '确认您的密码'}))
      
          class Meta:
              model = models.UserProfile
              fileds = "__all__"     #  [''  ]
              exculde = ['']   # 排除
              
              labels ={'name':'用户名'}
              widgets = {
                  'password':forms.PasswordInput(attrs={})            
              }
              
              error_messages = {
      			'username': {'invalid': '请输入正确的邮箱地址'}
      		}
              
         	def clean(self):
      		# 获取到两次密码
      		password = self.cleaned_data.get('password', )
      		re_password = self.cleaned_data.get('re_password', '')
      		if password == re_password:
      			# 加密后返回
      			md5 = hashlib.md5()
      			md5.update(password.encode('utf-8'))
      			password = md5.hexdigest()
      			self.cleaned_data['password'] = password
      			# 返回所有数据
      			return self.cleaned_data
      		# 抛出异常
      		self.add_error('re_password', '两次密码不一致!!')
      		raise ValidationError('两次密码不一致')
          
          
      视图：
      	展示添加
      	form_obj = RegForm（）
          render(request,'form.html',{'form_obj':form_obj})
          
          提交到数据库
          form_obj = RegForm（data=request.POST）
          form_obj.is_valid()
          form_obj.save()
          
          
      模板：
      	{{ form_obj.as_p }}
          
          {{ form_obj.name }}  #  ——》  input框
          {{ form_obj.name.label }}  #  ——》  中文的提示
          {{ form_obj.name.id_for_label }}  #  ——》  input框的ID
          {{ form_obj.name.errors }}  #  ——》  当前字段的所有的错误
          {{ form_obj.name.errors.0 }}  #  ——》  当前字段的第一个的错误
          {{ form_obj.errors }}  #  ——》  所有字段的错误
          
          {% for field in  form_obj  %} 
          	 {{ field.field  }}   #  ——》  原本的字段的对象
         		 {{ field }}
          
          {% endfor %}  
      ```

  - 登录

    ORM 普通的用户名  加密后的密码  is_active=True

    登录成功

    ​	保存当前用户的PK    —— 》session 

  - 中间件    

    - process_request
    - 白名单
    - 从session 中获取到pk   ——》 找对象
      - 没有对象 没有登录——》跳转到登录页面
      - 有对象   —— 》 request.user_obj = obj    return  None

  - 注销

    - 删除session

- 客户的管理

  - 展示客户

    - 公户和私户的展示

    - 展示不同字段
      - 普通字段     ——》 对象.字段名
      - choice字段   ——》 `对象.get_字段名_display()`
      - 外键字段     ——》  对象.外键 ——》 对象 （`__str__`）     对象.外键.字段名
      - 自定义方法
        - 多对多  
        - HTML代码    safe       mark_safe

  - 新增和编辑客户

    两个URL     一个视图函数   一个模板    modelform 

    CustomerForm(instance=obj)

    CustomerForm(request.POST,instance=obj)

    

  - 公户私户的转换

    反射  + ORM 

    

  - 分页

    切片起始值和终止值     ( page_num - 1 ) * per_num    page_num  * per_num

    

  - 模糊查询

    `Q( Q(name__contaions=query) | Q(qq__contaions=query) )`

    ```python
    q = Q()
    q.connector = 'OR'
    
    for i in field_list:
    	#  q.children.append(Q(('name__contaions',query))
        q.children.append(Q(('{}__contaions'.format(i),query))
                          
    return q
    ```

  - 分页保留搜索条件

    - QueryDict       request.GET  request.POST

      ```python
      q =  request.GET    # { 'query':'alex' }
      q.copy()    ——》  可修改的QueryDict    深拷贝
      { 'query':'alex' }   ——》 { 'query':'alex'，'page':'1'}
      q.urlencode()   ——》   query=alex&page=1
      ```

    - 修改或新增后跳转到原页面

      - simple_tag
      - 原来地址     path = request.get_full_path()  
      - qd = QueryDict (mutbale=True)
      - qd['next']  = path 

- 跟进记录的管理
  - 查询当前销售的所有的跟进记录
  - 查询某个客户的所有的跟进记录
  - 新增和编辑跟进记录

- 报名记录的管理
  - 查询当前销售的所有客户的报名表
  - 查询某个客户的报名表
  - 新增和编辑跟进记录报名表

## 今日内容

1. 公户变私户的问题

数据库的行级锁

事务 + 行级锁

```
begin；   # 开启事务
select * from 表  where id =10 for update ;  # 加行级锁
操作
commit；  # 结束事务  释放锁
```

```python
with transaction.atomic():  # 开启事务
   queryset = 	models.Customer.objects.filter(pk__in=ids,consultant__isnull=True).select_for_update() # 加行级锁 
```

2. 私户数量的上限

settings 中：

```Python
# 私户数量上限
CUSTOMER_MAX_NUM = 3
```

视图中：

```Python
from ob_crm.settings import CUSTOMER_MAX_NUM
from django.conf import settings

if len(ids) + self.request.user_obj.customers.all().count() > settings.CUSTOMER_MAX_NUM:
	return HttpResponse('差不多就行了，做人不要太贪心了！')
```

3. 班主任的功能

 1. 班级管理

    {{ form_obj.non_field_errors.0 }}     `__all__`的错误的显示

 2. 课程记录的管理

    

 3. 学习记录的管理

    批量插入

    ```python
    study_record_list = []
    for student in students:
             study_record_list.append(models.StudyRecord(student=student,course_record_id=course_record_id))
    
    models.StudyRecord.objects.bulk_create(study_record_list)
    ```

    

```Python
models.StudyRecord.objects.get_or_create(student=student,course_record_id=course_record_id)  # 获取或插入
models.StudyRecord.objects.update_or_create(student=student,course_record_id=course_record_id)   # 更新或插入
```

form  modelform

4. modelformset

```
from django.forms import modelformset_factory

FormSet = modelformset_factory(models.StudyRecord,form=StudyRecordForm,extra=0)  # 生产modelformset的类
formset_obj = FormSet(queryset=models.StudyRecord.objects.filter(course_record_id=course_record_id))

```

```
{{ formset_obj.management_form }}  # 管理  ************

{% for form in formset_obj %}
    <tr>
        {{ form.id }}      #  form的id   *************** 
        <td>{{ forloop.counter }}</td>
        <td>{{ form.instance.student }}</td>     # form.instance 记录对象  
        <td>{{ form.attendance }}</td>			 #  select框 input框
        <td>{{ form.score }}</td>
        <td>{{ form.homework_note }}</td>
        <td class="hidden">{{ form.student }}</td>     
        <td class="hidden">{{ form.course_record }}</td>
    </tr>
{% endfor %}
```



