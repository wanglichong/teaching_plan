# day77

## 内容回顾

day77Linux笔记


http://linux.51yip.com/  linux中文手册 
linux xshell的快捷键

ctrl + l 是清屏

 ctrl + d   是退出登录

 ctrl + shift + r 是快速重新连接

 

内容回顾：
1.什么是服务器，（在网络中对外提供服务器的一台性能超强的计算机）,硬件
实体服务器
云服务器

2.centos7版本的linux操作系统

3.选择的vmware虚拟化软件，安装的linux，充当一个服务器的角色


4.服务器（可能是云服务器，也可能在全球各地某一个机器），开发人员一般是不会直接触碰的，通过远程连接的方式，去使用

5.远程登录linux的命令

ssh  用户名@服务器的ip地址(公网的ip)

ssh 用户名@教室内的局域网ip地址（私有的）  确保在同一个网段

6.windows查看ip的命令
windows的命令行终端是　　ｃｍｄ


ipconfig  

7.linux查看ip的方式

ifconfig 查看网卡信息
ip  addr show  显示出ip网卡的信息  ip  a   效果同样


8.xshell就是封装了ssh(远程连接的命令)，只在windows下有的工具
如果你用的就是linux或者macos，只需要打开终端，输入ssh命令即可


9.linux登录之后的命令提示符

[root@localhost ~]# 
当前系统登录的用户名  root
@  占位符
localhost   当前机器主机名

~  当前你所在的路径
#  超级用户的身份提示符  
$  普通用户的身份提示符 


10.vim编辑器 ，文本编辑器 
vi  如同windows的记事本编辑器

vim 如同notepad++支持编程的编辑器 

vim的使用流程：
1.vim  你要操作的文件，此时进入了一个命令模式，等待你输入相关的指令
2.如果你要编辑，就输入 字母 i  ，代表插入，编辑
3.写完代码后，退出编辑模式，按下esc键 
4.此时回到了命令模式，输入 ：  冒号，进入底线命令模式 ，输入  ：wq!    :  w写入  q 退出  !强制的
:wq!  强制写入文本且退出vim
:q!  不写入直接强制退出

:w!  只保存写入，不退出 

[root@localhost tmp]# cat hello.py 
#!coding:utf-8
print("打瞌睡一时爽，一直打瞌睡一直爽")




11.创建linux普通用户

useradd  用户名  创建的用户信息会放在 /etc/passwd 这个文件下 

useradd xiaofeng  

passwd  xiaofeng  #给肖峰更改密码

12.linux命令的语法格式


linux命令   空格    参数（可有可无）    空格   你要操作的对象


linux参数的作用，就是更好的服务于开发人员，显示的更友好


ls  -a  显示所有隐藏文件

ls -la  以列表形式，详细输出文件信息


13.递归创建文件夹信息

mkdir --help  查看帮助信息
man  mkdir  


需求：创建s19文件夹，地下有男同学，女同学目录，且女同学里面有个魏文刚
mkdir  -p    /tmp/s19/{男同学,女同学/魏文刚}   


14.linux安装tree命令

pip3 instal django   #python装东西 

yum install  tree  #linux安装软件  




15.  linux的特殊符号的含义

~   用户家目录  
-   上一次的工作目录 
    .   当前目录
    ..   上一级目录
    ./    当前工作目录 
>     重定向覆盖输出符号     w 模式    
>     >   重定向追加输出符号     a  追加
>     >   <<  重定向追加写入符号    用在mysql数据导入时候



16.more  less  用于查看很大的文本  ，cat读取文件，是一次性读取，非常占内存，用于读取小文本


17.linux复制文件，文件夹

cp   你要复制的内容  复制之后的文件名

cp   filename   filename.bak


cp  -r   文件夹    新文件夹


18.mv命令
可以重命名还有移动路径的作用


19.linux下的搜索命令


find命令 查找命令 

语法

find   从哪找    -name   你要找的文件名
-name 指定文件名字

-type   指定文件类型   f  文本类型  d文件夹类型 

heeh.txt 


find   /       -name   heeh.txt #  全局搜索 

find   /etc   -name  heeh.txt  #局部搜索


找出/etc/下所有的txt文本

find   /etc  -name   "*.txt"


找出/etc下所有的python相关的文件信息

find /etc  -name  python* 

找到/etc/下所有的python文件夹

find  /etc  -type  d    -name  python* 
find  /etc  -type  f    -name  python* 

20.linux管道符的用法

第一条命令  |  第二条命令  

#用在查看linux进程信息


ps -ef  |  grep  python  

#查看linux端口信息
ip地址：标识计算机在网络中地址的号码   0~255 ip范围  192.168.16.37      192.168.16.xx 这是大家的windows地址
port : 标识应用服务的端口


80  http web服务器端口
443  https  加密的http协议
3306 mysql 
8000  django 
22    ssh协议用的端口 
8080  自定义的端口 


netstat -tunlp |grep  	3306  #确认mysql是否启动了3306端口
netstat -tunlp |grep   8000   #验证django是否正常启动 


21.过滤字符串命令，过滤文本信息
#语法

grep  参数   你要过滤字符串    你要操作的文件 


grep -i  "all"  settings.py 
-i 是忽略大小写

-v 是翻转搜索结果


#过滤掉settings.py中无用的信息（空白行，注释行）
grep   -v  "^#"   settings.py   |  grep -v "^$"  



22.从文件的 头，尾巴开始看


head  -5  filename  #看文件的前5行

tail  -5  filename   #看文件的后5行

tail -f   fielname  #实时监测文件信息

23.运维开发的职责

后端开发，crm开发，面向的是谁，你的产品，面向人类，路费卖python的片，crm提供企业人员数据管理 
运维开发做什么事？运维+开发，面向的是机器，
监控平台开发，有主流的监控软件 zabbix，nagios，监控软件，监控服务器状态的
cmdb资产平台开发
运维堡垒机开发，保护服务器安装  
容器管理平台开发，docker容器技术


24.别名alias

#当你输入rm，就提示用户，你这个大傻x，求你别用rm了

alias rm="echo 你这个大傻x，求你别用rm了"
unalias  rm   #取消别名 ，重新赋值别名变量也可以
#

25.远程传输命令

可以在两台linux之间互相传递文件
语法：
scp   你想传输的内容   你想传输到的地方

#把自己的文件 发送给别人
scp  小姐姐电话.txt  root@192.168.16.41:/tmp/

#把别人的文件夹拿过来
scp -r root@192.168.16.41:/tmp/s19/  ./

#把自己的文件夹，发送给别人 

scp  -r  你的文件夹    用户名@其他人的服务器ip:路径


26.如何统计/var/log/文件夹大小
如何查看文件夹大小

方式1： 
ls -lh   .   #详细的显示当前文件信息，和大小单位

方式2：
du  命令

-h 显示 kb  mb gb 单位
-s  显示合计大小

du -sh  /var/log/



27.linux的任务管理器  
top命令

28.给文件加锁 解锁

chattr +a  文件  给文件加锁

lsattr  文件 

29.linux的时间同步
date 查看当前系统时间

和阿里云的时间服务器同步

ntpdate -u  ntp.aliyun.com     #和阿里的时间服务器同步 
	-u  更新 


30.wget命令.
在线下载一个资源

wget https://img3.chouti.com/img_1556503826102=C200x200.jpg

递归深层爬取网站



31.linux下如何安装python3
	linux安装软件的方式
	1.yum安装，最省心的
	2.rpm包安装，需要手动解决依赖关系，很恶心
	3.源代码编译安装（公司都用这种方式），可以自定义软件版本，以及功能扩展
	
	
编译安装python3的步骤
1.下载python3的源代码
wget https://www.python.org/ftp/python/3.6.2/Python-3.6.2.tgz

2.解压缩源代码
tar  -xf  Python-3.6.2.tgz 
tar 是压缩解压的命令
-x  是解压参数
-f 指定一个压缩文件的名字

3.必须解决编译安装所需的软件依赖,比如操作系统的gcc工具....
yum install gcc patch libffi-devel python-devel  zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel -y

4.解决了软件依赖关系，就可以编译安装啦

5.进入python3的源代码目录，开始编译三部曲
	1.第一曲，释放makefile,释放编译文件 
	cd Python-3.6.2/ 
	./configure  --prefix=/opt/python362/   #告诉编译器，python3安装到哪里
		--prefix 就是制定安装路的
	
	2.编译第二曲 ，执行make指令，开始编译 ，编译完成后进行安装 make install 
	
	make && make install 
	
	3.编译第三区，配置python36的环境变量
		echo $PATH
	#永久生效环境变量，需要写入到linux  全局变量配置文件中 /etc/profile 
	
	vim  /etc/profile   
	#在最底行，写入如下配置即可
		PATH="/opt/python362/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin"
	#然后读取配置文件生效
		source /etc/profile  


​	
linux下进行django开发
1.创建django项目

django-admin  startproject    happy51



2.修改django配置

	127.0.0.1  本机回环地址，只能自己访问到自己，每台机器都有一个127.0.0.1
	
	0.0.0.0     代表绑定这台机器所有的网卡，所有人都可以去访问  
	
	192.168.16.37    当前机器对外提供访问的ip地址 


	关闭linux的防火墙
	iptables -F # 清空防火墙规则
	
	修改settings.py
	ALLOWED_HOSTS = ["*"]


​	


3.运行django项目
 #如果你项目启动在这个地址上，绑定了这个机器所有的ip，自己可以通过  127.0.0.1:8000
 #其他人如果想访问这个项目   ，应该访问 192.168.16.37:8000
python3 manage.py runserver  0.0.0.0:8000    


4.编写下课视图，返回一个 大家51快乐，玩的嗨皮

from django.shortcuts import render,HttpResponse
def xiake(request):
    return HttpResponse("大家51快乐，可以下课吃饭了，下午3点上课")





31.linux和windows互传文件的软件  lrzsz 

yum  install lrzsz -y    

rz   接受文件（从windows接受文件）
sz   发送文件（发送linux文件给windows）




32.linux切换用户的命令，普通用户切换超级用户

普通用户执行   su -  root    #输入root的密码即可







 