# day65

## 内容回顾

### 1.json

数据交换的格式  

Python

​	数据类型： 数字 字符串 列表 字典  布尔值  None

​	转化：

​		序列化   Python的类型  ——》 json的字符串   

​		json.dumps(Python的类型)

​		json.dump(Python的类型, f )

​		反序列化     json的字符串   ——》   Python的类型 

​		json.loads( json的字符串)

​		json.load(f )

js

​	数据类型： 数字 字符串 列表 对象 布尔值  null

​	转化：

​		序列化  js的类型  ——》 json的字符串   

​		JSON.stringify( js的类型  )

​		反序列化     json的字符串   ——》   JS的类型

​		JSON.parse( json的字符串 ) 

JsonResponse({})      #  contentType  :application/json

JsonResponse([],safe=False)     

### 2.发请求的方式

1. a标签	GET

2. 地址栏输入地址   GET
3. form表单  GET/POST
   1. method = ‘post’    方法     action=''     
   2. input 要有name属性     需要value的值
   3. button按钮  或者  input type=‘submit’

4. ajax 

   是js技术，发送请求。

   特性：异步   局部刷新  数据量小

   1. 用法：

   ​	jq用法

   ​	导入jQuery

   ​	$.ajax({

   ​		url :    提交的地址

   ​		type： 提交的方式

   ​		data:   提交的数据   {}，  // application/x-www-form-urlencoded

   ​		success: funcation(res) {}   res ——》 响应体

   ​		//   location.href  =  地址    重定向

   })

   2. 上传文件

      var form_obj = new FormData()   // multipart/form-data

      form_obj .append('f1',  $('#f1')[0].files[0]   )

      $.ajax({

      ​		url :    提交的地址

      ​		type： 提交的方式

      ​		data:  form_obj  ，

      ​		processData:false    //     不用处理数据

      ​		contentType:false   //    不用处理头信息

      ​		success: funcation(res) {}   res ——》 响应体

      })

   3. ajax通过django的csrf的校验

      1. 给data添加数据

         data:{ "csrfmiddlewaretoken":   隐藏标签  或者  cookie  }

      2. 加请求头

         headers : { "x-csrftoken"  :  隐藏标签  或者  cookie }

      3. 文件引入

## 今日内容

### form组件

### 1. 定义

```
from django import forms

class RegForm(forms.Form):
   user = forms.CharField()
   pwd = forms.CharField()
```

### 2.使用

view中：

```python
form_obj = RegForm()

form_obj = RegForm(request.POST)
form_obj.is_valid()
form_obj.cleaned_data   #   只有校验成功的值
```



return render(request, 'reg2.html', {'form_obj': form_obj})

模板 ：

```html
{{ form_obj.as_p }}    ——》 生成p标签   label 和 input标签
 
{{ form_obj.user }}    ——》 字段对应的input标签
{{ form_obj.user.id_for_label }}     ——》 input标签的ID
{{ form_obj.user.label }}    ——》 字段对应的input标签的中文
{{ form_obj.user.errors }}   ——》 当前字段的所有错误信息
{{ form_obj.user.errors.0 }} ——》 当前字段的第一个错误信息

{{ form_obj.errors }}    ——》  所有字段的所有的错误信息
```

### 3. 字段

```Python
CharField        文本
ChoiceField      选择框
MultipleChoiceField   多选框
```

### 4.字段参数

```Python
	required=True,               是否允许为空
    widget=None,                 HTML插件
    label=None,                  用于生成Label标签或显示内容
    initial=None,                初始值
    error_messages=None,         错误信息 {'required': '不能为空', 'invalid': '格式错误'}
    validators=[],               自定义验证规则
    disabled=False,              是否可以编辑
 
```

### 5.校验

1. 内置校验

```
min_length=8,
max_length
required=True
```

2. 自定义校验

字段的参数   validators=[check,RegexValidator()],

 1. 写函数

    ```
    from django.core.exceptions import ValidationError
    
    def check(value):
       # 自定义校验规则
       # 通过校验  不用返回
       # 没有通过校验  抛出异常   raise ValidationError('有非法字符')
    ```

	2. 内置的校验器

    ```
    from django.core.validators import RegexValidator
    
    phone = forms.CharField(
    		validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')]
    	)
    ```

### 6. is_valid()的流程：

1. 执行self.errors的方法 ——》 self.full_clean()

2. full_clean方法（）：

   1. 定义一个存放错误信息的字典  self._errors = ErrorDict() 
   2. 定义一个存放正确（经过校验）值 的字典   self.cleaned_data = {}

   3.  执行self._clean_fields()：

      1. 依次循环所有的字段
      2. 执行内部的校验  和 自定义校验器的校验  value = field.clean(value)
      3. 将正确的值放在self.cleaned_data       self.cleaned_data[name] = value

      4. 有局部钩子就执行
         1. 通过校验 self.cleaned_data[name]   重新赋值
         2. 不通过校验   self.cleaned_data[name] 当前字段的键值对被清除 self._errors中存放当前字段的错误信息

   4. 执行self._clean_form()  ——》  self.clean()
      1. 如果有重新定义就执行
         1. 通过校验   返回所有的值     self.cleaned_data  重新赋值
         2. 不通过校验  把错误信息添加到  all    中

### 7. 局部钩子和全局钩子

```
def clean_user(self):
   # 局部钩子
   value = self.cleaned_data.get('user')  # alex
   # 通过校验规则 返回正确的值  （你可以修改）
   return "{}dsb".format(value)
   # 不通过校验规则 抛出异常
   raise ValidationError('错误提示')

def clean(self):
	# 全局钩子
	# 通过校验规则 返回正确的值（所有的值 self.cleaned_data  ）  （你可以修改）
	# 不通过校验规则 抛出异常
	pwd = self.cleaned_data.get('pwd')
	re_pwd = self.cleaned_data.get('re_pwd')
	if pwd == re_pwd:
		return self.cleaned_data
	raise ValidationError('两次密码不一致')
```

