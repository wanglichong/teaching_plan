# day74

## 内容回顾

面试

rbac     基于角色的权限控制

1.如何实现权限控制？

url  代表权限

表结构

简单控制

菜单表

- title    一级菜单的标题
- icon   图标
- weight  权重 

权限表

  - url   权限    正则表达式  没有^$     
  - title   标题    是什么样的功能
  - name   URL的别名  唯一
  - menu    外键   可为空  关联菜单表    menu_id   有 menu_id  二级菜单  没有   menu_id   普通权限
  - parent   外键   可为空  关联权限表  自关联  parent_id  有parent_id 子权限   没有parent_id 父权限  二级菜单

一级菜单

  - is_menu  是否是菜单   
  - icon   图标

角色表

- name   角色名称
- permissions   多对多   关联权限

用户表

- name   用户名
- pwd    密码  
- roles   多对多    关联角色

角色和权限的关系表

用户和角色的关系表

2.流程和技术点

1. 中间件    —— process_request

   1. 获取当前访问的URL

   2. request.current_menu_id = None  二级菜单的id

   3. request.breadcrumb_list = [ { title：首页  ，url :' /index/'}  ]

   4. 白名单

      - re
      - settings

   5. 获取登录状态

      没有登录 跳转到登录页面

   4. 免认证地址的校验

      - re
      - settings

   5. 权限的校验

      从session中获取权限的信息

      re 正则匹配当前的URL

      ​	匹配成功

      ​	id   pid 

      ​	没有pid   当前访问的是二级菜单

      ​		request.current_menu_id = id 

      ​		

      ​	有pid     当前访问的是子权限

      ​		request.current_menu_id = pid

      ​		request.breadcrumb_list.append( {  父权限title    父权限url   }  )

      ​	request.breadcrumb_list.append( {  title    url   }  )

   6. 没有权限 返回 HTTPresponse（没权限）

2. 登录

   1. get请求时返回登录页面

   2. 认证

      1. ORM 查询用户名和密码是否正确

      2. 认证成功

         1. 查询当前用户的所有权限

            - ORM     user.roles.
            - values（）    【{}】
            - 跨表 
            - 去除权限为空的权限   `filter（permissions__url__isnull=False）`
            - 去重  distinct()  

         2. 构建权限和菜单的数据结构

            权限的结构

            简单控制

            permisison_list  =   [  { 'url' }  ]  

            非菜单权限归属

            permisison_list  =   [  { 'url',   ‘id’   ‘pid’ }  ]  

            路径导航

            permisison_dict  = {  id :  { 'url',   ‘id’   ‘pid’ , 'title'}  }

            权限控制到按钮级别

            permisison_dict  = {  name :  { 'url',   ‘id’   ‘pid’ , 'title' ,‘pname’}  }

            

            菜单是结构

            一级菜单

            menu_list =   [  { 'url'  ,  'title'  ,'icon' }  ]

            二级菜单

            menu_dict = {     

            ​	 一级菜单ID： {

            ​		title

            ​		icon

            ​		children：[

            ​			{  url    title  }

            ​			{  url    title  }

            ​		]

            ​	}

            }

            非菜单权限归属

            menu_dict = {     

            ​	 一级菜单ID： {

            ​		title

            ​		icon

            ​		children：[

            ​			{  url    title   id }

            ​			{  url    title   id  }

            ​		]

            ​	}

            }

         3. 把权限信息和菜单信息、登录状态保存在session中

            1. session 可配置在内存中 读取速度快
            2. json的序列化     字典的key是数字的话，序列化后变成数字字符串 

3. 模板

   - 母版和继承
   - 动态生成菜单
     - inclusion_tag   
     - 从session中后去菜单信息
     - 给一级菜单和二级菜单加样式
     - 给menu.html 渲染
     -  CSS  js  
   - 路径导航
     - inclusion_tag   
     - 循环request.breadcrumb_list 

   - 权限控制到按钮级别
     - filter
     - name    in    权限的字典中
       - return True 