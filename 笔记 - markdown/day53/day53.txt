day53
1. 内容回顾：
	1. HTTP协议
		1. 请求的方式  8种 GET POST PUT HEAD DELETE CONNECT OPTION TRACE
		2. 状态码
			1xx 
			2xx  200 OK
			3XX  重定向 
			4XX  请求的错误 
			5XX  服务器的错误 
			
		3. url
			https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1
			URL路径  /s   /
			
		4. 请求和响应的格式
			请求（request  浏览器发给服务器的消息 ）
				格式：
					'请求方法 url路径 HTTP/1.1\r\n
					k1: v1\r\n
					k2: v2\r\n
					\r\n
					请求数据（请求体）'       get请求没有请求体
			
			
			
			响应（response  服务器发给浏览器的消息 ）
				格式：
					'HTTP/1.1 状态码 状态描述\r\n
					k1: v1\r\n       
					k2: v2\r\n
					\r\n
					响应体（HTML文本）'
		
	2. 浏览器发送请求接受响应的流程：
		1. 在地址栏上输入地址，回车，发送一个GET请求。
		2. 服务端接受到请求，拿到url路径，根据不同的路径执行不同的函数
		3. 服务端把结果封装成符合HTTP响应格式的内容，发送给浏览器
		4. 浏览器接受到响应。断开连接。浏览器渲染结果。
			
	3. web框架
		本质：  socket服务端
		功能：  
			1. socket收发消息	wsgi    wsgiref  uwsgi 
			2. 根据不同的路径返回不同的结果
			3. 动态页面（字符串的替换   jinja2  模板渲染）
			
			
		django   2  3
		flask    2
		tornado  1 2 3
		
	4. Django
		1. 下载安装
			命令行： pip install django==1.11.20 -i http://pypi.douban.com/simple/ 
													https://pypi.tuna.tsinghua.edu.cn/simple
			
			pycharm
		2. 创建项目
			命令行  django-admin startproject 项目名称
				
			pycharm
		3. 启动
			命令行
				cd 项目的根目录
				python manage.py runserver      # 127.0.0.1:8000
				python manage.py runserver  80  # 127.0.0.1:80
				python manage.py runserver  0.0.0.0:80  # 0.0.0.0:80
				
			pycharm
				
		
			
2. 今日内容
	1. 写一个完整登录示例
		静态文件的配置
			STATIC_URL = '/static/'  # 别名

			STATICFILES_DIRS = [
				os.path.join(BASE_DIR, 'static')
			]
		使用：
			<link rel="stylesheet" href="/static/css/login.css">
	
		form的使用:
			1. action="" method="post"    提交的地址  请求的方式 post
			2. input标签有name属性
			3. 有一个butto按钮或者input typ=submit
		
		目前要提交POST请求：
			在settings中注释掉MIDDLEWARE中的 django.middleware.csrf.CsrfViewMiddleware 
			
			
		获取提交的数据
			request.POST   POST提交的数据  类似字典
			request.POST['username']
			request.POST.get('username','xxx')
			
			
		重定向
			from django.shortcuts import render, redirect, HttpResponse
			
			return redirect('http://www.xiaohuar.com/hua/')
			return redirect('/index/')    Location: /index/ 
	
	2. app
		创建：
			Python manage.py startapp app名称
			
			pycharm中
				tools  run manage.py task   startapp app名称
			
		注册：
			INSTALLED_APPS = [
				...
				'app01',  
				'app01.apps.App01Config',  # 推荐写法
			]
					
	3. orm
	
	
		1. 在Django中使用mysql的流程：
			1. 创建一个mysql数据库
			2. 在settings中进行数据库的配置：
				DATABASES = {
						'default': {
							'ENGINE': 'django.db.backends.mysql',   # 引擎
							'NAME': 'django_day53',					# 名称
							'HOST': '127.0.0.1',					# IP地址
							'PORT': 3306,							# 端口
							'USER': 'root',							# 用户名
							'PASSWORD': '123'						# 密码
						}
					}
			3. 告诉Django使用pymysql模块连接mysql数据库
				在与settings同级目录下的__init__中写：
					import pymysql
					pymysql.install_as_MySQLdb()
					
			4. 在app下的models.py中写类（表）：
				class User(models.Model):
					username = models.CharField(max_length=32)  # varchar(32) 
					password = models.CharField(max_length=32)
			5. 执行数据库迁移的命令：
				python manage.py makemigrations   # 保存models.py的变更记录
				python manage.py migrate		  # 将变更记录同步到数据库中  

		2. ORM操作
		
		
			models.User.objects.all()  # 所有数据   对象列表
			models.User.objects.get(username='alex')  # 获取一个满足条件的对象  没有数据 或者 多条数据就报错
			models.User.objects.filter(username='alex1')  # 获取满足条件的所有对象  对象列表
			
			
			