# day75

## 内容回顾

rbac 基于角色的权限控制

1.表结构

菜单表

​	title  标题

​	icon 图标

​	weight   权重    数值大的排在前面

权限表

​	url      权限      url正则  没有^$   

​	title    标题

​	name   url别名   唯一

​	menu  外键  关联菜单表    menu_id   

​	parent  外键  自关联    parent_id   



​	#   is_menu   是否是菜单       一级菜单使用

​	#   icon    图标		         一级菜单使用

角色表

​	name  名称  

​	permissions   多对多    关联权限表

用户表

​	name   用户名

​	pwd    密码

​	roles    多对多  关联角色表

角色和权限的关系表

用户和角色的关系表

2.流程

简单的权限控制

​	1. 登录成功后查询当前用户的权限信息 保存在session中

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

动态生成一级菜单

 1.  登录成功后查询当前用户的权限信息 

    把权限信息（列表）和菜单信息（列表）保存在session中

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

3. 模板

   inclusion_tag  动态生成一级菜单   循环menu_list

动态生成二级菜单

1.  登录成功后查询当前用户的权限信息 

   把权限信息（列表）和菜单信息（字典）保存在session中

   菜单的数据结构

   ​	menu_dict  =  {

   ​		一级菜单ID： {   

   ​				title：

   ​				icon：

   ​				children： [

   ​					{  url      title     }

   ​			]

   ​		}

   }

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

3. 模板

   inclusion_tag  动态生成二级菜单   循环menu_dict.values()   两次循环

   css  js  

一级菜单排序

1.  登录成功后查询当前用户的权限信息 

   把权限信息（列表）和菜单信息（字典）保存在session中

   菜单的数据结构

   ​	menu_dict  =  {

   ​		一级菜单ID： {   

   ​				title：

   ​				icon：

   ​				weight: 

   ​				children： [

   ​					{  url      title     }

   ​			]

   ​		}

   }

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

3. 模板

   inclusion_tag  动态生成二级菜单   循环menu_dict.values()   两次循环

   sotred  排序 

   OrderedDict   确保字典有序

   css  js  

二级菜单默认选中并且展开

1.  登录成功后查询当前用户的权限信息 

   把权限信息（列表）和菜单信息（字典）保存在session中

   菜单的数据结构

   ​	menu_dict  =  {

   ​		一级菜单ID： {   

   ​				title：

   ​				icon：

   ​				weight: 

   ​				children： [

   ​					{  url      title     }

   ​			]

   ​		}

   }

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

3. 模板

   inclusion_tag  动态生成二级菜单   循环menu_dict.values()   两次循环

   sotred  排序 

   OrderedDict   确保字典有序

   循环所有二级菜单
   	当前URL地址和 二级菜单的地址 正则匹配

   ​	匹配成功 

   ​		 给二级菜单加class = active 

   ​		 给一级菜单 去除  class = hide 		

   css  js  



非菜单权限归属（二级菜单和子权限 都有选中的效果）

1.  登录成功后查询当前用户的权限信息 

   把权限信息（列表）和菜单信息（字典）保存在session中

   权限的数据结构

   permission_list = [

   ​		{  url  id  pid  }

   ]

   菜单的数据结构

   ​	menu_dict  =  {

   ​		一级菜单ID： {   

   ​				title：

   ​				icon：

   ​				weight: 

   ​				children： [

   ​					{  url      title   id  }

   ​			]

   ​		}

   }

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

​			获取权限的id  parent_id  

​			没有parent_id    当前访问的是二级菜单    记录当前权限的id 

​			有parent_id      当前访问的是子权限         记录当前权限的parent_id 

3. 模板

   inclusion_tag  动态生成二级菜单   循环menu_dict.values()   两次循环

   sotred  排序 

   OrderedDict   确保字典有序

   循环所有二级菜单
   	记录下的二级菜单的id 和 当前二级菜单的id 匹配

   ​	匹配成功 

   ​		 给二级菜单加class = active 

   ​		 给一级菜单 去除  class = hide 		

   css  js  

路径导航

1.  登录成功后查询当前用户的权限信息 

   把权限信息（字典）和菜单信息（字典）保存在session中

   权限的数据结构

   permission_dict = [

   ​		id   :  {  url  id  pid title  }

   ]

   菜单的数据结构

   ​	menu_dict  =  {

   ​		一级菜单ID： {   

   ​				title：

   ​				icon：

   ​				weight: 

   ​				children： [

   ​					{  url      title   id  }

   ​			]

   ​		}

   }

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		breadcrumb_list = [   {  url :index  title 首页 }]

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

​			获取权限的id  parent_id  

​			没有parent_id    

​				当前访问的是二级菜单    记录当前权限的id 

​				breadcrumb_list.append({ url    title  })   当前权限

​			有parent_id     

​				 当前访问的是子权限         记录当前权限的parent_id 

​				breadcrumb_list.append({ url    title  })   父权限   permission_dict [str(parent_id)]

​				breadcrumb_list.append({ url    title  })   当前权限

3. 模板

   inclusion_tag  动态生成二级菜单   循环menu_dict.values()   两次循环

   sotred  排序 

   OrderedDict   确保字典有序

   循环所有二级菜单
   	记录下的二级菜单的id 和 当前二级菜单的id 匹配

   ​	匹配成功 

   ​		 给二级菜单加class = active 

   ​		 给一级菜单 去除  class = hide 		

   css  js  

   路径导航  inclusion_tag  

   ​	循环 breadcrumb_list 





权限控制到按钮级别

1.  登录成功后查询当前用户的权限信息 

   把权限信息（字典）和菜单信息（字典）保存在session中

   权限的数据结构

   permission_dict = [

   ​		name   :  {  url  id  pid title  pname }

   ]

   菜单的数据结构

   ​	menu_dict  =  {

   ​		一级菜单ID： {   

   ​				title：

   ​				icon：

   ​				weight: 

   ​				children： [

   ​					{  url      title   id  }

   ​			]

   ​		}

   }

​	2. 中间件中进行权限的校验

​		获取当前访问的url地址

​		breadcrumb_list = [   {  url :index  title 首页 }]

​		白名单

​		校验登录状态

​		免认证

​		获取当前用户的权限信息

​		权限的校验   循环  正则匹配

​			获取权限的id  parent_id  pname 

​			没有parent_id    

​				当前访问的是二级菜单    记录当前权限的id 

​				breadcrumb_list.append({ url    title  })   当前权限

​			有parent_id     

​				 当前访问的是子权限         记录当前权限的parent_id 

​				breadcrumb_list.append({ url    title  })   父权限   permission_dict [pname]

​				breadcrumb_list.append({ url    title  })   当前权限

3. 模板

   inclusion_tag  动态生成二级菜单   循环menu_dict.values()   两次循环

   sotred  排序 

   OrderedDict   确保字典有序

   循环所有二级菜单
   	记录下的二级菜单的id 和 当前二级菜单的id 匹配

   ​	匹配成功 

   ​		 给二级菜单加class = active 

   ​		 给一级菜单 去除  class = hide 		

   css  js  

   路径导航  inclusion_tag  

   ​	循环 breadcrumb_list 

   has_permisson  filter

   ​	name  in permission_dict 

   ​		return  True 



今日内容：

1.单个权限的管理

2.权限的批量操作

3.分配权限

4.权限组件的应用 **

1. 拷贝rbac组件到新项目中，并且注册

2. 迁移数据库

   1. 修改rbac用户表

      ManyToManyField 中 关联写上Role ，不要写字符串

      ```
      class User(models.Model):
         """用户表"""
         # name = models.CharField('用户名', max_length=32)
         # pwd = models.CharField('密码', max_length=32)
         roles = models.ManyToManyField(Role, verbose_name='用户所拥有的角色', blank=True)
      
         class Meta:
            abstract = True   # 迁移的时候不生成表，继承使用  当基类
      ```

   2. 已有的用户表继承rbac的User

      ```
      class UserProfile(User)
      ```

   3. 执行数据库迁移的命令

      python manage.py  makemigrations

      python  manage.py  migrate

3. 配置上rbac的路由

   ```python
   url(r'^rbac/',include('rbac.urls',namespace='rbac')),
   ```

4. 角色管理

   http://127.0.0.1:8001/rbac/role/list/

   添加角色   http://127.0.0.1:8001/rbac/role/add/

5. 菜单管理

   http://127.0.0.1:8001/rbac/menu/list/

   添加菜单 http://127.0.0.1:8001/rbac/menu/add/

6. 批量操作权限

   http://127.0.0.1:8001/rbac/multi/permissions/

   录入权限的标题、分配一级菜单下的二级菜单和子权限

7. 分配权限

   http://127.0.0.1:8001/rbac/distribute/permissions/

   权限分配的视图中  rbac的用户类替换成 使用用户表的类

   添加用户  给用户分配角色  给角色分配权限

8. 应用上权限

   1. 加上权限的中间件

      ```
      MIDDLEWARE = [
      	...
         'rbac.middlewares.rbac.RbacMiddleWare'
      ]
      ```

   2. 添加上权限的相关配置

      ```
      # 权限相关的配置
      
      # 白名单
      WHITE_LIST = [
         r'^/crm/login/$',
         r'^/crm/reg/$',
         r'^/admin/.*',
      ]
      
      # 免认证的URL地址
      NO_PERMISSION_LIST = [
         r'^/crm/index/$',
      ]
      
      # 存权限的SESSION KEY
      PERMISSION_SESSION_KEY = 'permission'
      
      # 存菜单的SESSION KEY
      MENU_SESSION_KEY = 'menu'
      ```

   3. 登录成功权限信息的初始化

      ```Python
      from rbac.service.init_permission import init_permission
      认证成功：
      	# 进行权限信息的初始化（保存权限信息 菜单信息 登录状态）
          init_permission(request,obj)
      
      ```

9. 动态生成二级菜单

   ```
   <div class="menu-body">
       {% load rbac %}
       {% menu request %}
   </div>
   ```

   加上CSS JS 

   ```
   <link rel="stylesheet" href="{% static 'rbac/css/menu.css' %} "/>
   <script src="{% static 'rbac/js/menu.js' %} "></script>
   
   ```

10. 路径导航

    ```
    {% breadcrumb request %}
    ```

11. 权限控制到按钮级别

    ```html
    {% if request|has_permission:'add_class' %}
        <a class="btn btn-primary" href="{% reverse_url request 'add_class' %}">添加</a>
    {% endif %}
    ```