## 内容回顾

资产变更

以硬盘为例

槽位   1  2  3  4 

新的槽位   3 4  5 6

```python
disk_slot_set =  { 3,4,5,6 }   # 新汇报的
disk_db_slot_set =  { 1,2,3,4 }  # 数据库中的
# 新增
add_set = disk_slot_set - disk_db_slot_set
# 删除
del_set = disk_db_slot_set - disk_slot_set
# 更新
update = disk_db_slot_set & disk_slot_set

```

API的验证

方案一：

​	客户端和服务端 规定一个 key  

​	缺点： 不动态    不安全

方案二：

​	key|时间戳   ——》 md5加密

​	时间上的校验

​	key只能用一次

​	key是否相等

​	

​	

