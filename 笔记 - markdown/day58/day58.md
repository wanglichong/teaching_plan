# day58

## 内容回顾

#### 1. MVC  MTV

1. MVC 
   - M model    和数据库进行交互
   - V   view      视图  展示数据  HTML
   - C   controller   控制器 调度  
2. MTV 
   - M model  ORM  
   - T  template   模板   字符串的替换
   - V  view    视图   —— 》  函数  业务流程

#### 2. 变量

render(request,'模板的文件名', {  k1:v2 ,k2:v2 })

{{   变量名  }}

{{   name_list.0   }} 

{{   d_dict.name   }} 

{{   d_dict.keys }} 

{{   d_dict.values }} 

{{   d_dict.items }} 

{{  p1.name  }}

{{  p1.talk  }}

优先级：    字典的key   》   属性或者方法   》 数字索引

#### 3. 过滤器

{{  变量|过滤器的名字:参数  }}

内置的过滤器：

​	default:'nothing'    变量不存在或者值为空 显示默认值

​	filesizeformat    文件大小 人性化显示  byte  PB

​	lower  upper title  ljust   rjust   center  join 

​	first  取第一个值

​	last  取最后一个值

​	add   +    数字加法减法  字符串的拼接 列表的拼接

​	length   返回变量的长度

​	slice    切片

​	date:' Y-m-d  H:i:s '   日期时间的格式化 

​			DATETIME_FORMAT = 'Y-m-d H:i:s'    USE_L10N = False

​	truncatechars:9   按照字符进行截断  9  ...

​	truncatewords:9   按照单词进行截断 

​	safe    取消对字符的转义

#### 4. 自定义过滤器

1. 在app下创建一个名为templatetags的Python包

2. 在包内创建一个py文件    my_tags.py

3. 在py文件中写：

   from  django  import template

   register  = template.Library()

   

 4. 写函数 + 装饰器

```Python
@register.filter
def xxxx(value,arg=None):
    
    return 

@register.filter(name='add_str')
def xxxx(value,arg=None):
    
    return 
```

5. 使用

   ```html
   
   {% load  my_tags %}
   {{ '1111'|xxxx:'222' }}
   
   {{  '1111'|add_str:'222' }}
   ```

## 今日内容

#### for 

{%  for  i in  name_list %}

​	{{ forloop }}    {   }  

{% endfor  %}



{%  for  i in  name_list %}



{% empty %}

 

{% endfor  %}

#### if 

支持的 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断

不支持 算数运算   add  

不支持连续判断     10 > 5 > 1   false

#### csrf 

跨站请求伪造

{% csrf_token %}   生成一个隐藏的input标签  name   csrfmiddlewaretoken  

#### 母版和继承

​	母版：

​		就是一个普通HTML，提取了多个页面的公共部分

​		定义一些 block 块  让子页面重新覆盖  {%  block  名字 %}   {% endblock %}

​	继承：

​		{% extends  'data_list.html' %}

​		重写block 块 

​	注意：

  		1. {% extends 'data_list.html' %} 写在第一行  前面不要有内容
  		2. {% extends 'data_list.html' %}   模板的文件名引号要带上  不然当做变量查找
  		3. 要显示的内容放在block块中，其他部分不显示
  		4. 模板中定义多个block块  一般写上  css  js 

#### 组件

​	把一小段HTML 放在一个HTML中   nav.html 

​	使用：

​		{%  include ‘nav.html ’ %}

#### 静态文件相关

​	{% load static %}

​	{% static 'plugins/bootstrap-3.3.7/css/bootstrap.css' %}     ——》 完整的静态文件的路径

​	{% get_static_prefix %}    			—— 》 静态文件的别名

#### simple_tag inclusion_tag

![1554263720468](assets/1554263720468.png)

filter   simple_tag  inclusion_tag

{{  value|过滤器:参数 }}





