day54
1. 内容回顾
	1. 下载安装
		1. 命令行
			pip install django==1.11.20  
			pip install django==1.11.20 -i 源 
		2. pycharm
			settings——》 解释器 ——》 点+号 ——》 输入Django ——》 选择版本 ——》 安装
			
	2. 创建项目
		1. 命令行
			切换到存放项目的目录下
			django-admin startproject 项目名 
		2. pycharm
			file ——》 new project ——》 django ——》 输入项目目录 ——》 选择解释器 ——》输入一个app名称 ——》创建
			
	3. 启动项目
		1. 命令行
			切换到项目根目录下   manage.py 
			python manage.py runserver      		# 127.0.0.1:8000
			python manage.py runserver  80  		# 127.0.0.1:80
			python manage.py runserver  0.0.0.0:80  # 0.0.0.0:80
			
		2. pycharm
			点击绿三角
			配置中改 ip 和 端口
			
	4. 	settings配置
		静态文件
			STATIC_URL = '/static/'   # 别名

			STATICFILES_DIRS = [
				os.path.join(BASE_DIR, 'static1'),
				os.path.join(BASE_DIR, 'static'),
				os.path.join(BASE_DIR, 'static2'),
			]
			
		INSTALLED_APPS  已注册的APP
		
		数据库
		
		中间件
			# 'django.middleware.csrf.CsrfViewMiddleware',    注释掉可以提交POST请求
		
		模板TEMPLATES
			'DIRS': [os.path.join(BASE_DIR, 'templates')]
		
		
	5. App
	
		1. 创建app
			命令行
				python manage.py startapp APP名称
			pycharm
				tools ——》 run manage.py task ——》 输入 startapp APP名称
				
			
		2. 注册app 	
		
		
			INSTALLED_APPS = [
				...
				# 'app01',
				'app01.apps.App01Config',
			]
			
	6. urls.py 
		写url路径和函数的对应关系
		from django.conf.urls import url
		from app01 import views

		urlpatterns = [
			url(r'^login/', views.login),
			url(r'^index/', views.index),
			url(r'^orm_test/', views.orm_test),

		]
	
	7.	views.py	
		写函数
		def login（request）
		
		request.method   请求方式  POST GET 
		request.POST     form表单提交POST请求的数据  querydict   {}  request.POST[] request.POST.get()
		
		返回值
			from django.shortcuts import render, redirect, HttpResponse
			
			HttpResponse('内容')   ——》   字符串
			render(request,'HTML文件名')   ——》  返回一个完整的HTML页面
			redirect('重定向的地址')       ——》  重定向     响应头 Location：重定向的地址
			
	8. form表单
		1. form标签的属性  action=''  method='post'    action提交的地址 method 请求的方式
		2. 所有的input标签要有name属性   有的还有value属性
		3. 有一个type=submit input标签 或者 一个button按钮
		
	9. get和post的区别
		get  获取一个页面  
		参数显示在url上
		提交参数  http://127.0.0.1:8000/login/?username=alex&password=alexdsb
		获取值：
			request.GET   request.GET[]  request.GET.get()
			
		post 提交数据
		参数在请求体  不显示
		获取值：
			request.POST   request.POST[]  request.POST.get()
		
	10. django使用mysql数据库的流程：
		1. 创建一个mysql数据库；
		2. 在settings中配置数据库：
			ENGINE:  mysql
			NAME:   数据库的名称
			HOST：  ip  127.0.0.1 
			PORT:   3306
			USER:  'root' 
			PASSWORD:  ''  

		3. 告诉django使用pymysql连接数据库
			在与settings同级目录下的__init__.py中写：
				import pymysql
				pymysql.install_as_MySQLdb()
		4. 写表  在app下的models.py中写类：
			from django.db import models

			class User(models.Model):      # app名称_类名小写
				username = models.CharField(max_length=32)  # varchar(32)
				password = models.CharField(max_length=32)
				
		5. 执行数据库迁移的命令
			python manage.py makemigrations  # 记录下models.py的变更记录
			python manage.py migrate         # 把变更记录同步到数据库中

	11. ORM
		对应关系
			类     —— 》   表
			对象   —— 》   数据（记录）
			属性   —— 》   字段
			
		ORM完成的操作：
			1. 表的操作
			2. 数据的操作
			
		ORM具体操作：
			from app01 import models


			models.User.objects.all()   #  获取表中所有的数据  queryset   对象列表 
			obj = models.User.objects.get(username='alex') # 获取一个对象 
			
			obj.username   # 字段值
			obj = models.User.objects.filter(username='alex')  # 获取满足条件的所有对象  queryset  对象列表
			
			
2. 今日内容
	1. 图书管理系统
	   - 出版社的管理
		展示
		新增
		删除
		编辑
	
	2. 展示
		1. 设计url
			    url(r'^publisher_list/', views.publisher_list),
		2.  写函数
			# 展示出版社
			def publisher_list(request):
				# 获取所有出版社数据
				publishers = models.Publisher.objects.all()
				
				# 返回一个包含出版社数据的页面
				return render(request, 'publisher_list.html', {'publishers': publishers})
				
		3. 写模板    {{ 变量 }}  {% %}  
			<thead>
			<tr>
				<th>序号</th>
				<th>ID</th>
				<th>出版社名称</th>
			</tr>
			</thead>
			<tbody>

			{% for i in publishers %}
				<tr>
					<td>{{ forloop.counter }}</td>
					<td>{{ i.pk }}</td>
					<td>{{ i.name }}</td>
				</tr>

			{% endfor %}

			</tbody>

	
	3. 新增
		# 方式一  create()
		ret = models.Publisher.objects.create(name=new_name)
		# 方式二
		ret = models.Publisher(name=new_name)
		ret.save()  # 插入到数据中

	4. 删除
		pk = request.GET.get('pk')
		# 删除数据
		obj = models.Publisher.objects.get(pk=pk)
		obj.delete()

	5. 修改
		obj.name = new_name  # 只在内存中修改属性
		obj.save()  # 保存到数据库中
