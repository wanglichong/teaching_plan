# day79

## 内容回顾

s19day79linux笔记

1.python的虚拟环境，用于解决python环境依赖冲突的问题，仅仅是多个解释器的分身，多个解释器的复制，和操作系统无关 
2.python虚拟环境的工具有很多 ，有virtualenv，pipenv  ,pyenv 
3.virtualenv 可以在系统中建立多个不同并且相互不干扰的虚拟环境。

virtualenv的学习安装使用


注意：确保python3成功的安装，并且PATH配置在第一条路径


1.下载安装
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple virtualenv

2.安装完毕，就可以使用virtualenv命令，创建虚拟环境去使用了

#这个命令，在哪敲，就会在哪生成venv文件夹
virtualenv --no-site-packages --python=python3  venv   #得到独立第三方包的环境，并且指定解释器是python3
	#参数解释
	 --no-site-packages   #这个参数用于构建，干净的环境，没有任何的第三方包
	 --python=python3   #指定虚拟环境的本体，是python的哪一个版本 
	 venv 就是一个虚拟环境的文件夹，是虚拟python解释器

3.创建完毕venv虚拟环境，就可以激活使用了
进入venv/bin目录下
source  activate    #source是读取指令，读取这个activate脚本中的内容，激活虚拟环境

4.验证虚拟环境是否正确
which pip3 
which python3  都来自于venv路径，就是对了 

5.使用虚拟环境，分别构建django1 和django2的平台

注意，开启2个linux窗口
注意，开启2个linux窗口
注意，开启2个linux窗口
步骤1：分别下载两个venv1 venv2，用于运行不同的django

virtualenv --no-site-packages --python=python3  venv1
virtualenv --no-site-packages --python=python3  venv2 

步骤2：这2个虚拟环境，都得单独的激活去使用

source venv1/bin/activate #激活虚拟环境
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple django==2.0.1  #下载django模块
deactivate  #退出虚拟环境 


source  venv2/bin/activate 
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple django==1.11.20 #下载django 
deactivate  #退出虚拟环境 




回忆：python3的编译安装
1.解决python3安装所需的依赖关系
yum install gcc patch libffi-devel python-devel  zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel -y

2.下载python3的源代码
wget https://www.python.org/ftp/python/3.6.7/Python-3.6.7.tar.xz

3.最终搞得到一个压缩包Python-3.6.7.tar.xz
xz -d  Python-3.6.7.tar.xz
tar -xvf  Python-3.6.7.tar

4.进入python3的源码目录
cd Python-3.6.7

5.开始编译安装三部曲
	1.释放makefile，编译文件，并且指定安装路径
	./configure --prefix=/opt/python36/
	2.开始编译，调用gcc编译器
	make 
	3.开始安装，到/opt/python36目录下
	make install  
	4.安装完毕之后，python3的解释器就出现在了/opt/python36/bin目录下
	5.配置PATH环境变量，写入如下变量到/etc/profile
	PATH="/opt/python36/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin"
	6.还得读取/etc/profile
	source /etc/profile  #读取配置文件，让PATH生效

​	


保证本地开发环境，和线上一致性的步骤
1.导出本地python环境的所有模块
pip3 freeze > requirements.txt 

2.将这个依赖文件，发送给服务器linux
requirements.txt 

3.服务器linux上，构建虚拟环境，安装这个文件，即可
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple  -r requirements.txt 



virtualenvwrapper的学习使用
1.安装 
pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple  virtualenvwrapper

2.配置环境变量，每次开机都加载virtualenvwrapper这个工具，注意配置的是个人环境变量配置文件
vim ~/.bash_profile   #打开文件

#写入如下环境变量 export也是一个读取指令，让变量生效的
export WORKON_HOME=~/Envs   #设置virtualenv的统一管理目录
export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'   #添加virtualenvwrapper的参数，生成干净隔绝的环境
export VIRTUALENVWRAPPER_PYTHON=/opt/python362/bin/python3    #指定python解释器，虚拟环境以谁去分身
source /opt/python362/bin/virtualenvwrapper.sh                 #执行virtualenvwrapper安装脚本

3.此时退出linux终端回话，重新登录，让virtualenvwrapper工具生效

4.学习virtualenvwrapper他的命令，管理虚拟环境

mkvirtualenv  虚拟环境的名字  #创建虚拟环境，存放目录是统一管理的
workon  虚拟环境的名字  #可以在任意目录直接激活虚拟环境
rmvirtualenv  虚拟环境的名字  #删除虚拟环境
lsvirtualenv  列出所有的虚拟环境
cdvirtualenv   进入虚拟环境的目录
cdsitepackages  进入虚拟环境的第三方包













 











​	





 