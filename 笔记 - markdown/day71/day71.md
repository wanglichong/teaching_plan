# day71

1.什么是权限？

2.为什么要有权限？

​	不同的用户使用不同的功能

3.在web开发中，什么东西是权限？

​	URL  约等于 权限

4.开发一个权限的组件，为什么要开发一个组件？

5.表结构的设计

第一版

权限表   permission

id     url   

1       /customer_list/

2       /add_customer/

用户表   user

id      username    pwd 

1       alex		123    

2       congcong        123

用户和权限关系表

id    user_id     permission_id

1       1 			1

2       1			 2

3       2			1

第二版

3个model  5张表

权限表  permission

id  url   title

角色表 

id  name

用户表

id  username  

角色和权限的关系表

用户和角色的关系表

流程：

1. 登录

   1. get 

      通过中间件的白名单   —— 》  获取到login页面

   2. 填写用户名和密码  —— 》POST

      通过中间件的白名单

      校验用户名和密码

      ​	校验失败  —— 》 重新登录

      ​	校验成功  ——》   通过当前的用户 查找他所有的权限   把权限信息和登录状态保存在session中 ——》 重定向  index

2. 访问index

   index 是一个免认证的地址  中间件直接通过  ——》 得到index的页面

3. 方法其他需要校验的地址

   中间件

   ​	获取当前用户的权限信息

   ​	循环权限信息  一个个做re判断

   ​		判断成功  —— 》 正常通过

   ​	都循环结束 没有权限  ——》 返回 HttpResponse('没有访问权限 请联系管理员')







