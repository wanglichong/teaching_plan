## 内容回顾

DRF视图

混合类 mixin 实现一部分的功能，不能单独使用，必须和其他的类搭配使用

封装和继承

把不一样地方提取出来，做为属性。



序列化 

```python
from rest_framework import serializers
from app01 import models


class PublisherModelSerializer(serializers.ModelSerializer):
   class Meta:
      model = models.Publisher
      fields = '__all__'


class AuthorModelSerializer(serializers.ModelSerializer):
   class Meta:
      model = models.Author
      fields = '__all__'


class BookModelSerializer(serializers.ModelSerializer):
   category_info = serializers.SerializerMethodField()  # 执行 get_字段名的方法
   publisher_info = serializers.SerializerMethodField()  
   authors_info = serializers.SerializerMethodField()  

   def get_category_info(self, obj):
      return obj.get_category_display()

   def get_publisher_info(self, obj):
      return PublisherModelSerializer(obj.publisher).data

   def get_authors_info(self, obj):
      return AuthorModelSerializer(obj.authors.all(), many=True).data

   class Meta:
      model = models.Book
      fields = '__all__'
      extra_kwargs = {
         'category': {'write_only': True},
         'publisher': {'write_only': True, },
         'authors': {'write_only': True, 'required': False}
      }
```



两种写法：

第一种：

urls:

```python 
url(r'^publishers/$', views.PublisherListView.as_view()),
url(r'^publishers/(?P<pk>\d+)/$', views.PublisherDetailView.as_view()),
```

视图：

```Python
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

class PublisherListView(ListCreateAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


class PublisherDetailView(RetrieveUpdateDestroyAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer

```

第二种：

urls:

```python 
url(r'^books/$', views.BookView.as_view(actions={'get': 'list', 'post': 'create'})),
url(r'^books/(?P<pk>\d+)$',
   views.BookView.as_view(actions={'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}, )),
```

视图：

```Python
from rest_framework.viewsets import ModelViewSet


class BookView(ModelViewSet):
   queryset = models.Book.objects.all()
   serializer_class = BookModelSerializer
```

## 今日内容

认证 

告诉API 你是谁

```python
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
from auth_two import models


class MyAuth(BaseAuthentication):

   def authenticate(self, request):
      # 获取用户的token
      token = request.query_params.get('token')
      if not token:
         raise AuthenticationFailed('无效参数')
      # 查询用户表中是否有相应的token
      user_obj = models.UserInfo.objects.filter(token=token).first()
      if user_obj:
         return user_obj, token  #  用户  token
      else:
         # 没有token就认证失败
         raise AuthenticationFailed('认证失败')
```

应用：

全局：

```python
REST_FRAMEWORK = {
  'DEFAULT_AUTHENTICATION_CLASSES': ['auth_two.auth.MyAuth']
}
```

视图

```Python
from auth_two.auth import MyAuth


class TestView(APIView):
   authentication_classes = [MyAuth] # 认证类

   def get(self, request):
      print(request.user)
      return Response({'msg': "认证后才能看到此内容"})
```





权限

某些用户是否能拿到数据

权限基于认证

```Python
from rest_framework.permissions import BasePermission


class VipPermission(BasePermission):
   message = '没有权限'

   def has_permission(self, request, view):
      # 是否登录 request.user 没有认证成功是一个匿名用户
      if not request.auth:
         return False
      # 对用户的权限进行判断
      if request.user.vip:
         return True
      else:
         return False
```

全局配置

```Python
REST_FRAMEWORK = {
   'DEFAULT_AUTHENTICATION_CLASSES': ['auth_two.auth.MyAuth'],
   'DEFAULT_PERMISSION_CLASSES': ['auth_two.permission.VipPermission']
}
```

视图配置

```Python
from auth_two.auth import MyAuth
from auth_two.permission import VipPermission

class TestView(APIView):
   authentication_classes = [MyAuth]
   permission_classes = [VipPermission]

   def get(self, request):
      print(request.user)
      return Response({'msg': "认证后才能看到此内容"})
```

限制访问频率

```Python
from rest_framework.throttling import SimpleRateThrottle


class MySimpleRateThrottle(SimpleRateThrottle):
   scope = 'xxxx'  # 要写和settings是一样的 

   def get_cache_key(self, request, view):
      return self.get_ident(request)
```



全局配置

```Python
REST_FRAMEWORK = {
  
   'DEFAULT_THROTTLE_CLASSES': ['auth_two.throttles.MySimpleRateThrottle'],
   'DEFAULT_THROTTLE_RATES': {
      'xxxx': '1/m'   # s 秒  m 分钟 h   d 
   }

}
```

