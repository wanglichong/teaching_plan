## 内容回顾

REST 

RESTful  API

1. 协议   Https

2. 域名     

   api.luffycity.com

   luffycity.com/api/

3. 版本

   /api/v1/

   把版放请求头中

4. 请求方式

   GET  获取资源  列表  一个

   POST    新增资源

   PUT    更新资源

   DELETE 删除资源

5. 地址上不要用动词

   /books/    

   ​	 GET   获取所有书籍的信息

   ​	POST   新增一本书籍

   /books/(\d+) 

   ​	   GET   获取一本书籍的详细信息

   ​	   PUT  更新书籍

   ​	 DELETE 删除书籍

6. 查询条件

   ？limit = 5    获取5条数据

   ？ page=1&per_num=10   

7. 状态码

   200

   201 

   202 

   4xx    401 403 404 

   5xx    500  502  504

8. 返回值

   错误 

9. 相关联的内容 ——》 连接



Django REST framework （DRF）

下载   pip install djangorestframework   -i  

使用：

注册APP

```Python
INSTALLED_APPS = [
 	....
   'rest_framework'
]
```

视图

`from rest_framework.views import APIView`

响应

`from rest_framework.response import Response`

序列化

ORM 对象   ——》   json 格式字符串   序列化 

json 格式字符串   ——》  ORM 对象   反序列化

序列化的工具

```
from rest_framework.serializers import Serializer    #   form 
from rest_framework.serializers import ModelSerializer  # modelform 
```

定义：

```
required=False   #  非必填
read_only=True    # 序列化时使用
write_only=True   # 反序列化时使用

```

校验：

1. 校验字段

   validate_字段名(self, attrs)

   类似于form中的局部钩子   clean_字段名（self）

   通过校验 返回值

   不通过校验  抛出异常 raise serializers.ValidationError('不合法')

2. 全局校验

   类似于form中的全局钩子   clean（self）

   通过校验 返回所有的值

   不通过校验  抛出异常 raise serializers.ValidationError('不合法')

3. 字段中validators —— 自定义函数

   ```Python
   def check_title(value):
      if 'xxx' in value:
         raise serializers.ValidationError('不能包含xxx')
      else:
         return value
       
   title = serializers.CharField(validators=[check_title])
   ```

方法字段

![1558919938668](assets/1558919938668.png)



## 今日内容

![1558923044094](assets/1558923044094.png)

### 第一次优化

提取出来相似的内容

```Python
class GenericView(APIView):
    """配置和公共方法 """
   queryset = None
   serializer_class = None

   def get_queryset(self):
      # 确保每次查询都是最新的数据
      return self.queryset.all()
```

混合类

```python 
class ListViewMixin(object):  # 混合类   不能单独使用  配合其他的类进行使用(利用Python的多继承)
   def get(self, request):
      queryset = self.get_queryset()
      ser_obj = self.serializer_class(queryset, many=True)
      return Response(ser_obj.data)
```

### 第二次优化

![1558928764316](assets/1558928764316.png)

### 第三次优化

```
ViewSetMixin重写as_view的方法  实行请求方法重新定向到咱们实现的方法上 actions
```

```Python
class AuthorView(GenericViewSet,
             ListViewMixin,
             RetrieveViewMixin,
             CreateViewMixin,
             UpdateViewMixin,
             DestroyViewMixin,
             ):
   lookup_field = 'pk'  # 查询的关键字  默认 pk
   queryset = models.Author.objects.all()
   serializer_class = AuthorModelSerializer
```

### 总结

#### 一个表两个视图类

url：

```
url(r'^authors/$', views.AuthorListView.as_view(), ),
url(r'^authors/(?P<pk>\d+)/$',  views.AuthorDetailView.as_view(), ),
```
视图：

```Python
from rest_framework.generics import GenericAPIView, ListAPIView, CreateAPIView,UpdateAPIView,DestroyAPIView,RetrieveAPIView


class AuthorListView(ListAPIView,CreateAPIView ):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


class AuthorDetailView(UpdateAPIView,DestroyAPIView,RetrieveAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer
    
###    最后的写法
class AuthorListView(ListCreateAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer


class AuthorDetailView(RetrieveUpdateDestroyAPIView):
	queryset = models.Publisher.objects.all()
	serializer_class = PublisherModelSerializer
```

#### 一个表一个视图类

url:

```python 
url(r'^authors/$', views.AuthorView.as_view(actions={'get': 'list', 'post': 'create'}), ),
url(r'^authors/(?P<pk>\d+)/$',
   views.AuthorView.as_view(actions={'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), ),
```

视图：

```Python
from rest_framework.viewsets import GenericViewSet,ModelViewSet


class AuthorView(ModelViewSet):
   lookup_field = 'pk'  # 查询的关键字  默认 pk
   queryset = models.Author.objects.all()
   serializer_class = AuthorModelSerializer
```

![modelivewset](assets/modelivewset.png)

