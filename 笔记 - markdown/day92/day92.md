## 内容回顾

CMDB 

配置管理数据库

自动化运维的系统：

- 自动装机
- 配管系统
- 自动发布
- 监控
- 堡垒机

实现的思路



项目的结构：

1. 资产采集    —— python 项目 
2. api          —— django项目
3. 管理后台



技术点：

资产采集：

1. 兼容三种模式 并且 可扩展

   配置 —— 类   自定义、默认

   importlib   +  反射

   类的约束  

   ​	抽象类 + 抽象方法

   ​	继承  + 抛出异常 NotImplementedError

2. 可插拔式的插件

3. 支持不同系统的采集

4. 响应的封装

   `对象.__dict__`

5. debug模式

6. 执行命令

   根据不同的模式 采取 不同执行命令的方式

   agent    subprocess

   ssh     paramiko

   salt   salt

7. 获取到执行命令的结果 ——》解析  ——》 { }

8. requests 发送请求

9. 操作类型

   唯一标识 ： 主机名

api

​	FBV   CBV 

​	API验证：

​		动态  key|时间戳  —— 》md5

​		限制时间

​		只能使用一次

后台管理：

​	django  —— 增删改查

​	

使用stark组件的流程：

1. 拷贝stark组件到项目中，注册APP ——  'stark.apps.StarkConfig'

2. 在ROOT_URLCONF配置路由

   ```
   from stark.service.stark import site
   
   urlpatterns = [
   	...
      url(r'^stark/', site.urls),
   	...
   ]
   ```

3. 在app下创建stark.py  注册model，写配置

   ```Python
   from stark.service.stark import site, StarkConfig
   from repository import models
   
   
   class BusinessUnitCofig(StarkConfig):
      list_display = ['id','name']
   
   
   site.register(models.BusinessUnit, BusinessUnitCofig)
   ```

   

展示字段

1. 普通字段  —— 字段名

   ```python
   class ServerConfig(StarkConfig):
      list_display = ['id', 'hostname','os_platform','cpu_physical_count','latest_date',]
   ```

2. choice字段

   1. 自定义方法

      ```Python
      # 自定义显示内容
      def show_device_status(self,header=None,row=None):
         if header:
            return '状态'
         return row.get_device_status_id_display()
      
      list_display = [show_device_status,...]
      ```

   2. 组件内置的函数

      ```
      from stark.service.stark import get_choice_text
      # 配置
      list_display = [get_choice_text('device_status_id','设备状态')]
      ```

3. 外键

   配置中填写外键字段名

   外键的model定义`__str__`的方法

组合搜索

```Python
# 组合搜索
list_filter = [
   Option('business_unit', is_multi=True,condition={'id__in':[1,3]}),
   Option('device_status_id', is_choice=True, text_func=lambda x: x[1], value_func=lambda x: x[0],is_multi=True),
]
```

Option的参数：

​	field   筛选的字段  —— 外键   choice

​	is_multi   多选 

​	is_choice  是否是choice

​	condition    条件  展示字段的筛选

​	text_func   文本显示的函数

​	value_func  确认值的函数	

​	

实现的机制：

1. stark  apps 类中写了  ready 方法  

   去每个app下执行stark 文件

2. stark文件中

   定义一个配置类

   注册  model 和 配置类 

   site._registry   =   [ ModelConfigMapping(model_class, stark_config(model_class, self, prev), prev)    ]

3. ```
   url(r'^stark/', site.urls)
   
   ```

   /stark/app名称/表名/list	

   /stark/app名称/表名/add

   /stark/app名称/表名/(\d+)/change

   /stark/app名称/表名/(\d+)/del



ansible

安装

1. ```
   wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
   ```

2. yum install ansible -y



ssh 

​	密码连接

​	秘钥连接

ssh-keygen  生成一对   公钥 私钥

ssh-copy-id 192.168.16.140    把公钥拷贝到192.168.16.140



ansible的使用

单台

ansible 192.168.16.140 -m shell -a 'ls'

ansible  s19minion  -m  shell  -a 'ls'

所有主机

ansible all -m shell -a 'ls'

多台 

ansible 192.168.16.241,s19minion -m shell -a 'ls'

组

并集

ansible db,web -m shell -a 'ls'

ansible 'web: db' -m shell -a 'ls'

差集

ansible 'web:!db' -m shell -a 'ls'

交集

ansible 'web:&db' -m shell -a 'ls'



```python
import subprocess

ret = subprocess.getoutput('ansible  s19minion  -m  shell  -a "ls"')
```




