# day57

## 内容回顾

#### 1.所有的命令

##### 下载安装

​	pip  install  django==1.11.20    - i 源

##### 创建项目

​	django-admin startproject 项目名

##### 启动项目

​	python manage.py runserver    # 127.0.0.1:8000 

​	python manage.py runserver   80 # 127.0.0.1:80

​	python manage.py runserver   0.0.0.0:80 #  0.0.0.0:80

##### 创建APP

​	Python manage.py startapp  APP名称

##### 数据库迁移

python manage.py  makemigrations   #  根据models变化生成迁移文件

python manage.py  migrate           #  将变更记录同步到数据库

#### 2.配置

1. 静态文件

   STAITC_URL  = '/static/'

   STATICFILES_DIRS = [

   ​	os.path.join(BASE_DIR,'static'),

   ]

2. INSTALLED_APPS = [

   ​	'app01'   或者   ‘app01.apps.App01Config’

   ]

3. 中间件

   注释掉CSRF有关  

4. DATABASES

   ENGINE: mysql

   NAME:  名字

   HOST：IP

   PORT:   端口 

   USER: 用户名

   PASSWORD: 密码 

5. TEMPLATES

   DIRS :[  os.path.join(BASE_DIR ,'templates') ]

   

#### 3.django使用mysql数据库的流程：

1. 创建一个mysql数据库

2. 数据库的配置：

   ENGINE: mysql

   NAME:  名字

   HOST：IP

   PORT:   端口 

   USER: 用户名

   PASSWORD: 密码 

3. 告诉django使用pymysql连接mysql数据库

   在与settings同级目录下的init。py写：

   ​	import pymysql

   ​	pymysql.install_as_MySQLdb()

4. 在app下的models中写类（表）：

   ```python
   from django.db import models
   
   
   class Publisher(models.Model):  # app01_publisher
      pid = models.AutoField(primary_key=True)
      name = models.CharField(max_length=32)  # varchar(32)
   
   
   class Book(models.Model):
      name = models.CharField(max_length=32)
      pub = models.ForeignKey('Publisher', on_delete=models.CASCADE)
   
   
   class Author(models.Model):
      name = models.CharField(max_length=32)
      books = models.ManyToManyField('Book')
   ```

5. 执行数据库迁移的命令

   python manage.py  makemigrations   #  根据models变化生成迁移文件

   python manage.py  migrate             #  将变更记录同步到数据库

#### 4. request

request.method    ——》 GET  POST 

request.GET          ——》 url地址上的参数

request.POST       ——》POST请求提交的数据

#### 5. response

HttpResponse("字符串")     ——》 页面显示字符串

render(reqeust,'模板的文件名', { k1:v1  })     ——》   一个完整的页面

redirect('要跳转的地址')     ——》 重定向     Location: 要跳转的地址

#### 6.ORM 

```python
from app01 import models

# 查
models.Publisher.objects.all()    # 获取所有的数据 queryset 对象列表
models.Publisher.objects.filter（条件）   # 获取所有满足条件的对象  queryset 对象列表
models.Publisher.objects.get(条件)       # 获取一个满足条件的对象   对象   无或者多个就报错
models.Publisher.objects.all().order_by('pk')    # 排序  升序

pub_obj.pid   pub_obj.pk  # 主键 
pub_obj.name    # 名字

book_obj.pk
book_obj.name
book_obj.pub   ——》 书籍所关联的出版社的对象
book_obj.pub_id   ——》 书籍所关联的出版社的对象pk

author_obj.books        ——》 关系管理对象 
author_obj.books.all()  ——》 作者所关联的书籍对象
author_obj.books.set([ 所关联对象的id， 所关联对象的id，所关联对象的id ])
author_obj.books.set([ 所关联对象， 所关联对象，所关联对象 ])

# 增加
    models.Publisher.objects.create(name='xxxx')
    # 外键
    models.Book.objects.create(name='xxxx'，pub=pub_obj)
    models.Book.objects.create(name='xxxx'，pub_id=pub_obj.pk)
    # 多对多
    author_obj = models.Author.objects.create(name='xxxx')
    author_obj.books.set([  id,id  ])

    obj = models.Publisher(name='xxxx')
    obj.save()
    
# 删除
models.Publisher.objects.filter（pk=pk）.delete() 
models.Publisher.objects.get(pk=pk).delete() 


# 修改
pub_obj.name ='new name'
pub_obj.save()    


book_obj.name = 'new name'
book_obj.pub_id = pub_obj.id
book_obj.pub = pub_obj
book_obj.save()  

author_obj.name = 'new name'
author_obj.save()
author_obj.books.set([id ,id])

```



#### 7.模板

{{  变量  }}

```html

{% for i in list %}
	{{ forloop.counter  }}
	{{ i }}

{% endfor %}

{% if 条件  %}
	xxx
{% elif 条件  %}
	xxxx
{%  else %}
	xx
{% endif %}
```

## 今日内容

#### 1. MVC  MTV

MVC   

 - M models
 - V  view HTML 
 - C controller  控制器 

MTV 

 - M models
 - T  template  HTML
 - V view  函数 业务逻辑



#### 2. 过滤器

​	语法

​	 {{ value|filter_name }}    {{ value|filter_name:参数 }}  

**default**　　

```
{{ value|default:"nothing"}}
```

**add** 

数字加减

字符串的拼接

列表的拼接

```
{{ value|add:"2" }}
```

**length**

取变量的长度

**slice**　　

切片

**truncatechars**

截断字符

**date**

```
{{ value|date:"Y-m-d H:i:s"}}
```

```
# settings 中配置 
USE_L10N = False
DATETIME_FORMAT = 'Y-m-d H:i:s'
```

**safe**

Django这段代码是安全的不必转义

https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#built-in-filter-reference

自定义filter

1. 在app下创建一个名为templatetags的Python包    #  templatetags不能变

2. 在包内创建py文件   ——  my_tags.py

3. 在py文件中下：

   ```python
   from django import template
   
   register = template.Library()     #  register 不能变
   ```

4. 写函数+装饰器

   ```python
   @register.filter
   def add_str(value, arg=None):   # 函数名 就是 过滤器的名字  value变量  arg 给变量添加额外的参数
   	ret = '{}__{} '.format(value, arg)
   	print(ret)
   	return ret
   ```

   使用：

   ```
   {% load my_tags %}  #  导入文件
   
   {{ 'alex'|add_str:'sb' }}
   ```

   作业：

   1. 整理笔记

   2. 官网查看内置过滤器 

   3. 使用内置过滤器完成加、减、乘、除的操作

   4. 自定义一个过滤器：

      给定一个网址的字符串和它的名字，页面显示a标签

      ```HTML
      使用
      {{ 'www.baidu.com'|show_a:'百度'  }}
      
      结果
      <a href="http://www.baidu.com">百度</a>   点击可跳转
      
      ```

      

   



