##s19day83笔记

mysql关系型数据库 ，文件型数据库 

redis数据库学习(nosql 不仅仅是sql)，内存型数据库 


1.redis的安装方式,yum安装，源码编译安装

查看是否用yum安装了redis
rpm -qi redis 

2.我们选择源码编译安装redis，需要先卸载yum安装的redis
yum remove redis -y


3.下载redis的源代码，编译安装，指定安装路径
wget http://download.redis.io/releases/redis-4.0.10.tar.gz

4.解压缩源代码，编译安装
tar -zxvf redis-4.0.10.tar.gz
进入源代码目录，开始编译安装
这里不需要configure，直接make && make install 
就直接装在了当前的redis源码目录下，配置文件，和启动命令，都在当前目录了
/opt/redis-4.0.10/redis.conf 

redis的可执行命令如下，解释：
./redis-benchmark //用于进行redis性能测试的工具
./redis-check-dump //用于修复出问题的dump.rdb文件
./redis-cli //redis的客户端
./redis-server //redis的服务端
./redis-check-aof //用于修复出问题的AOF文件
./redis-sentinel //用于集群管理


5.如何安全的启动redis   	只需要修改redis.conf不同的参数即可 
	1.更改默认的redis启动端口
	2.给redis启动添加密码
	3.开启redis的安全机制(必须用密码验证，才能登陆数据库)
	

修改s19redis.conf配置文件内容如下：
bind 192.168.16.37  #redis绑定的地址 
protected-mode yes 	#redis安全模式
port 6380			#redis的端口
daemonize yes		#后台运行redis
requirepass haohaio		#redis的密码 


6.指定redis配置文件启动数据库
redis-server s19redis.conf 


7.登陆redis服务端数据库
redis指定端口和主机登录
redis-cli -p  6380  -h 192.168.16.37 
登录之后，通过auth指令验证密码
auth  haohaio 

#在登录命令中，添加密码
redis-cli -p  6380  -h 192.168.16.37  -a haohaio 

8.远程python连接redis服务端


>>> import redis 
>>> conn=redis.Redis(host="192.168.16.37",password="haohaio",port=6380)
>>> conn.set("name","dsb")
>>> True
>>> conn.get("name")
>>> b'dsb'


9.redis的数据类型使用
redis是一种高级的key：value存储系统，其中value支持五种数据类型
字符串（strings）
set key  value  
set name  dsb #设置name为key，值是dsb
get  name   #获取name的值

散列（hashes） 字典key-value
列表（lists）
集合（sets）无序，去重的数据类型 
有序集合（sorted sets）


keys *  查看机器所有的key  
type key  #显示key的类型
expire key  #给key加上过期时间
ttl key  #查看key的剩余 过期时间 -1 是永不过期  -2 是没有这个key
persist    # 取消key的过期时间   -1表示key存在，没有过期时间
exists key     #判断key存在    存在返回1    否则0
dbsize         #计算key的数量

字符串的常用命令接口
set 　　设置key
get   获取key
append  追加string
mset   设置多个键值对
mget   获取多个键值对
del  删除key
incr  递增+1
decr  递减-1

redis的列表用法
lpush         从列表左边插
rpush         从列表右边插
lrange          获取一定长度的元素  lrange key  start stop
ltrim               截取一定长度列表
lpop                 删除最左边一个元素
rpop                     删除最右边一个元素
lpushx/rpushx                key存在则添加值，不存在不处理

无序集合用法 set 
sadd/srem   添加/删除 元素
sismember   判断是否为set的一个元素
smembers    返回集合所有的成员
sdiff             返回一个集合和其他集合的差异
sinter           返回几个集合的交集
sunion          返回几个集合的并集

有序集合zset



散列类型类型用法
hset 设置散列值
hget  获取散列值
hmset  设置多对散列值
hmget  获取多对散列值
hsetnx   如果散列已经存在，则不设置（防止覆盖key）
hkeys     返回所有keys
hvals     返回所有values
hlen      返回散列包含域（field）的数量
hdel     删除散列指定的域（field）
hexists    判断是否存在



redis发布订阅




redis的数据持久化

背景：进程被杀死，服务器断电，内存中的数据都会被释放，数据丢失，如果redis没有持久化，数据丢失

redis支持两种数据持久化
RDB
1.基于内存快照的数据持久化
持久化数据文件是一个压缩的二进制文件
通过save指令可以手动触发 持久化
也可以配置时间触发持久化

2.配置方式如下
vim s19-rdb.conf 

daemonize yes
port 6379
logfile /data/6379/redis.log
dir /data/6379         #redis的数据存放目录 
dbfilename  dbmp.rdb     #这里是配置redis 持久化rdb方式的核心文件，开启了rdb功能 
save 900 1       #以下这三个是持久化的时间间隔机制  自动执行save持久化

save 300 10      
save 60  10000  

创建数据文件夹
mkdir -p /data/6379

3.指定支持持久化的redis配置文件启动
redis-server s19-rdb.conf  
      

4.测试写入redis数据
redis-cli 
>>  set name  haha 
>>  save   触发持久化
>>  此时就会生成一个 dump.rdb 数据文件在 /data/6379目录

5.此时就算重启 数据也不会丢失
注意必须指定配置文件启动

rdb有缺点，可能会造成数据丢失，但是持久化速度最快


AOF持久化模式
1.在配置文件中，定义功能参数，即可使用aof
vim  s19-aof.conf 
daemonize yes
port 6379
logfile /data/6379/redis.log
dir /data/6379
appendonly yes
appendfsync everysec

2.指定aof配置文件启动

redis-server s19-aof.conf 

3.启动redis服务端，查看aof日志变动，并且aof数据持久化




redis不重启从rdb切换到aof数据持久化
1.实现准备，准备一个rdb的redis数据库，写入数据

2.在登录了 rdb的数据库当中，进行切换aof，命令如下

3.配置aof的方式如下，通过config set命令，设置aof的功能  
127.0.0.1:6379> CONFIG set appendonly yes
OK
127.0.0.1:6379> 
127.0.0.1:6379> 
127.0.0.1:6379> CONFIG SET save "" 
OK

4.还得修改配置文件，以后永远都是aof

daemonize yes
port 6379
logfile /data/6379/redis.log
dir /data/6379    
dbfilename  dbmp.rdb  
save 900 1    
save 300 10    
save 60  10000    
appendonly yes
appendfsync everysec




redis的主从同步功能
主从同步，读写分离的功能，redis.conf配置文件中定义的

1.redis支持多实例的功能，通过配置文件生效，多实例的概念
写多个配置文件，指定文件启动，端口区分数据库，就是多实例了。。


redis-6379.conf     主库
	port 6379
	daemonize yes
	pidfile /data/6379/redis.pid
	loglevel notice
	logfile "/data/6379/redis.log"
	dbfilename dump.rdb
	dir /data/6379
	protected-mode no

redis-6380.conf 	从库1 
	port 6380
	daemonize yes
	pidfile /data/6380/redis.pid
	loglevel notice
	logfile "/data/6380/redis.log"
	dbfilename dump.rdb
	dir /data/6380
	protected-mode no
	slaveof  127.0.0.1  6379  



redis-6381.conf 	从库2
	port 6381
	daemonize yes
	pidfile /data/6381/redis.pid
	loglevel notice
	logfile "/data/6381/redis.log"
	dbfilename dump.rdb
	dir /data/6381
	protected-mode no
	slaveof  127.0.0.1  6379  

创建数据库文件夹
mkdir -p /data/{6379,6380,6381}


分别启动三个redis数据库实例，默认已经是主从关系了

redis-server  redis-6379.conf 
redis-server  redis-6380.conf 
redis-server  redis-6381.conf 

检查redis的主从关系
info指令用于查看redis的数据库详细信息

redis-cli -p 6379  info replication  
redis-cli -p 6380  info replication  
redis-cli -p 6381  info replication  

#redis的主从复制故障修复
1.手动杀死主库
kill 主库pid

2.选择一个从库为新的主库，例如6380是主，6381是从
登录 6380数据库，输入
127.0.0.1:6380> slaveof no one 
OK


3.去登录6381从库
127.0.0.1:6381> slaveof no one 
OK
127.0.0.1:6381> slaveof 127.0.0.1 6380 

4.此时就是6380是主库，6381是从库





redis高可用之哨兵功能
1.配置redis-sentinel
环境准备
三个redis实例，准备一主两从的架构
redis-6379.conf 
	port 6379
	daemonize yes
	logfile "6379.log"
	dbfilename "dump-6379.rdb"
	dir "/opt/data/6379"
redis-6380.conf 
redis-6381.conf  
配置文件定义如下
 1015  sed "s/6379/6380/g"  redis-6379.conf > redis-6380.conf 
 1016  sed "s/6379/6381/g"  redis-6379.conf > redis-6381.conf 
 1017  echo "slaveof 127.0.0.1 6379" >> redis-6380.conf 
 1018  echo "slaveof 127.0.0.1 6379" >> redis-6381.conf 

创建三个数据文件夹 
mkdir -p /opt/data/{6379,6380,6381}


分别启动三个redis数据库
 1030  redis-server redis-6379.conf 
 1031  redis-server redis-6380.conf 
 1032  redis-server redis-6381.conf 

检查三个数据库redis的身份信息
[root@nginx1 sbredis]# redis-cli -p 6379 info replication 
[root@nginx1 sbredis]# redis-cli -p 6380 info replication 
[root@nginx1 sbredis]# redis-cli -p 6381 info replication 

 


三个redis-sentinel配置文件，运行三个哨兵进程
redis-sentinel-26379.conf 
配置如下，其他节点，仅仅是端口的不同
port 26379
dir /opt/data/26379
logfile "26379.log"

sentinel monitor s19msredis  127.0.0.1  6379 1

sentinel down-after-milliseconds s19msredis 30000

sentinel parallel-syncs s19msredis 1

sentinel failover-timeout s19msredis  180000
daemonize yes

其他两个配置文件，创建如下
sed "s/26379/26380/g"  redis-sentinel-26379.conf > redis-sentinel-26380.conf
sed "s/26379/26381/g"  redis-sentinel-26379.conf > redis-sentinel-26381.conf

创建三个文件夹
mkdir -p /opt/data/{26379,26380,26381}

分别启动三个哨兵
redis-sentinel redis-sentinel-26379.conf 
redis-sentinel redis-sentinel-26380.conf 
redis-sentinel redis-sentinel-26381.conf 


此时验证redis-sentinel是否成功
redis-cli -p 26379 info sentinel  #查看哨兵是否成功
如果信息如下，则正确，找到了2个slave从，3个哨兵同伴
[root@nginx1 sbredis]# redis-cli -p 26379 info sentinel
# Sentinel
sentinel_masters:1
sentinel_tilt:0
sentinel_running_scripts:0
sentinel_scripts_queue_length:0
sentinel_simulate_failure_flags:0
master0:name=s19msredis,status=ok,address=127.0.0.1:6379,slaves=2,sentinels=3

#杀死redis主库，查看是否会自动的主从故障切换



redis-cluster集群搭建
1.环境准备6个redis节点配置文件
redis-7000.conf 
	port 7000
	daemonize yes
	dir "/opt/redis/data"
	logfile "7000.log"
	dbfilename "dump-7000.rdb"
	cluster-enabled yes   
	cluster-config-file nodes-7000.conf
	cluster-require-full-coverage no    
	

redis-7001.conf 
redis-7002.conf 
redis-7003.conf 
redis-7004.conf 
redis-7005.conf 

其他redis数据库节点，仅仅是端口的不同即可
替换生成配置文件如下
sed "s/7000/7001/g" redis-7000.conf > redis-7001.conf 
sed "s/7000/7002/g" redis-7000.conf > redis-7002.conf 
sed "s/7000/7003/g" redis-7000.conf > redis-7003.conf 
sed "s/7000/7004/g" redis-7000.conf > redis-7004.conf 
sed "s/7000/7005/g" redis-7000.conf > redis-7005.conf 

生成数据文件夹
mkdir -p /opt/redis/data

分别启动6个节点

 1104  redis-server redis-7000.conf 
 1105  redis-server redis-7001.conf 
 1106  redis-server redis-7002.conf 
 1107  redis-server redis-7003.conf 
 1108  redis-server redis-7004.conf 
 1109  redis-server redis-7005.conf 


此时redis的集群还未分配  槽位   slots ，我们得下载ruby的脚本，创建这个16384个槽位分配

1.下载ruby的源码包
wget https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.1.tar.gz


2.解压缩
tar -zxvf ruby-2.3.1.tar.gz

3.进入ruby源码包目录
./configure --prefix=/opt/s19ruby/
make 
make install 
4.配置ruby的环境变量
PATH="/opt/python362/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin:/opt/tngx230/sbin:/opt/s19luffy/node-v8.6.0-linux-x64/bin:/opt/s19ruby/bin"


5.读取PATH环境变量
source /etc/profile


6.下载安装ruby操作redis的包
wget http://rubygems.org/downloads/redis-3.3.0.gem
#ruby安装操作redis的包模块
gem install -l redis-3.3.0.gem

7.一句命令分配redis集群的槽位
--replicas 1这个意思是每一个主库，只有一个从库
这个redis-trib.rb 命令，默认不会添加环境变量，直接用绝对路径去创建.
find /opt/  -name redis-trib.rb  #查看这个脚本的绝对路径

/opt/redis-4.0.10/src/redis-trib.rb  create --replicas 1 127.0.0.1:7000 127.0.0.1:7001 127.0.0.1:7002 127.0.0.1:7003 127.0.0.1:7004 127.0.0.1:7005


8.此时槽位已经分配好了，可以写入集群数据了
登录某一个节点，写入数据查看结果
redis-cli -p 7000  -c    #-c 以集群方式运行
集群数据会经过重定向，计算出槽位，放入某一个节点













python  pip  
nodejs  npm 
ruby  gem  























​	