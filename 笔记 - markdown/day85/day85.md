1.昨日内容回顾
docker一个容器技术
docker的生命周期，三大概念，
容器，镜像，仓库
容器是镜像的实例化
仓库有dockerhub 共有仓库，docker registory私有仓库，存放镜像的地方
常用docker管理容器镜像的命令
docker rmi 镜像id 
docker rm  容器id 
docker ps  -a  #显示出所有运行过的容器记录，挂掉的，以及活着的
docker ps   #查看正在运行的容器进程
docker images   #查看所有镜像文件
docker container ls  #列出所有的容器进程
docker pull  镜像名   #在线拉取镜像文件
docker search 镜像 
docker run  -d后台运行  -p指定端口映射  -P 随机端口映射 -it (交互式的命令操作)  -v 数据库挂载(宿主机和容器空间的映射 )   镜像名  
docker exec  -it   正在运行的容器id   #交互式命令操作，进入容器空间
docker logs  -f  容器id   #实时查看容器内日志（django日志，nginx日志）
docker save  #导出镜像
docker load  #导入镜像
docker commit  #提交本地容器记录，保存为一个新的镜像
docker start   #启停容器
docker stop   
docker tag  现有镜像名   打上新的标签的镜像名 
docker build .    #找到当前目录的Dockerfile 进行打包成新的镜像
systemctl start docker 
docker push  #推送本地镜像，到共有/私有仓库，如何区分往哪推，通过你镜像名来区分，需要用docker tag改
docker login   #登录dockerhub


今日内容
消息队列
实现了消息队列功能的软件rabbitmq，在linux如何安装
rabbitmq很吃资源，需要至少2G内存才能运行  

1.配置好yum源
2.下载rabbitmq服务端，erlang编程语言 
yum install erlang   rabbitmq-server  -y  

3.配置一下rabbitmq的web插件
rabbitmq-plugins enable rabbitmq_management

4.创建rabbitmq的登录账号密码
rabbitmqctl add_user heiheihei 123


5.设置用户为管理员权限
rabbitmqctl set_user_tags heiheihei administrator

6.对消息队列进行授权，可以读写
rabbitmqctl set_permissions -p "/" heiheihei  ".*" ".*" ".*"

7.重启rabbitmq，让配置生效
service rabbitmq-server restart  #这是centos6的系统服务管理命令

systemctl restart rabbitmq-server  

8.下载python操作rabbitmq的模块
pip3 install pika==0.10.0  #注意最新的模块，参数传入方式变了，需要看源码



9.实现生产消费者模型的代码

no_ack，不确认回复的机制代码如下

生产者.py 
	#!/usr/bin/env python
	import pika
	# 创建凭证，使用rabbitmq用户密码登录
	# 去邮局取邮件，必须得验证身份
	credentials = pika.PlainCredentials("heiheihei","123")
	# 新建连接，这里localhost可以更换为服务器ip
	# 找到这个邮局，等于连接上服务器
	connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.16.37',credentials=credentials))
	# 创建频道
	# 建造一个大邮箱，隶属于这家邮局的邮箱，就是个连接
	channel = connection.channel()
	# 声明一个队列，用于接收消息，队列名字叫“水许传”
	channel.queue_declare(queue='水许传')
	# 注意在rabbitmq中，消息想要发送给队列，必须经过交换(exchange)，初学可以使用空字符串交换(exchange='')，它允许我们精确的指定发送给哪个队列(routing_key=''),参数body值发送的数据
	channel.basic_publish(exchange='',
						  routing_key='水许传',
						  body='武松一拳打死了两只小脑斧')
	print("已经发送了消息")
	# 程序退出前，确保刷新网络缓冲以及消息发送给rabbitmq，需要关闭本次连接
	connection.close()




消费者.py (可以运行多个消费者)
	import pika
	# 建立与rabbitmq的连接
	credentials = pika.PlainCredentials("heiheihei","123")
	connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.16.37',credentials=credentials))
	channel = connection.channel()
	channel.queue_declare(queue="水许传")

	def callbak(ch,method,properties,body):
		#不确认的机制下，如果消费走了数据，且代码报错，数据会丢失，正确的应该是用确认机制
		
		#int("hehe")
		print("消费者,消费出了数据：%r"%body.decode("utf8"))
	# 有消息来临，立即执行callbak，没有消息则夯住，等待消息
	# 老百姓开始去邮箱取邮件啦，队列名字是水许传
	#no_ack机制，不确认机制，消费走了数据，不需要给服务端确认，服务端直接标记消息清除
	channel.basic_consume(callbak,queue="水许传",no_ack=True)
	# 开始消费，接收消息
	channel.start_consuming()



no_ack  机制  
no_ack=True
不确认机制，不需要给服务端一个确认回复，服务端直接标记消息清除，有可能造成消息丢失
no_ack=Flase
确认机制，你消费走了数据，还得给服务端一个确认回复，让服务端可以正确的标记消息清除，保证消息不丢失


一个生产者，多个消费者，默认是轮训机制

ack机制

生产者.py 

	#!/usr/bin/env python
	import pika
	# 创建凭证，使用rabbitmq用户密码登录
	# 去邮局取邮件，必须得验证身份
	credentials = pika.PlainCredentials("heiheihei","123")
	# 新建连接，这里localhost可以更换为服务器ip
	# 找到这个邮局，等于连接上服务器
	connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.16.37',credentials=credentials))
	# 创建频道
	# 建造一个大邮箱，隶属于这家邮局的邮箱，就是个连接
	channel = connection.channel()
	# 新建一个hello队列，用于接收消息
	# 这个邮箱可以收发各个班级的邮件，通过
	channel.queue_declare(queue='王者荣耀')
	# 注意在rabbitmq中，消息想要发送给队列，必须经过交换(exchange)，初学可以使用空字符串交换(exchange='')，它允许我们精确的指定发送给哪个队列(routing_key=''),参数body值发送的数据
	channel.basic_publish(exchange='',
						  routing_key='王者荣耀',
						  body='敌人还有5秒种，到达战场')
	print("已经发送了消息")
	# 程序退出前，确保刷新网络缓冲以及消息发送给rabbitmq，需要关闭本次连接
	connection.close()

消费者.py (支持ack的消费者)
	import pika

	credentials = pika.PlainCredentials("heiheihei","123")
	connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.16.37',credentials=credentials))
	channel = connection.channel()
	
	# 声明一个队列(创建一个队列)
	channel.queue_declare(queue='王者荣耀')
	
	def callback(ch, method, properties, body):
		print("消费者取到了消息: %r" % body.decode("utf-8"))
		# int('asdfasdf')
		# 我告诉rabbitmq服务端，我已经取走了消息
		# 回复方式在这
		ch.basic_ack(delivery_tag=method.delivery_tag)
	# 关闭no_ack，代表给与服务端ack回复，确认给与回复
	#no_ack=False  代表确认要给服务端，回复消息
	channel.basic_consume(callback,queue='王者荣耀',no_ack=False)
	
	channel.start_consuming()


队列的持久化
背景：默认队列不支持持久化，rabbitmq重启之后，所有队列丢失 

配置持久化的队列的方式如下
1.声明队列时候，加上持久化的参数

持久化的生产者.py 

import pika
# 无密码
# connection = pika.BlockingConnection(pika.ConnectionParameters('123.206.16.61'))
# 有密码
credentials = pika.PlainCredentials("heiheihei","123")
connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.16.37',credentials=credentials))
channel = connection.channel()
# 声明一个队列(创建一个队列)
# 默认此队列不支持持久化，如果服务挂掉，数据丢失
# durable=True 开启持久化，必须新开启一个队列，原本的队列已经不支持持久化了
'''
实现rabbitmq持久化条件
 delivery_mode=2  #实现消息的持久化，重启后，队列和消息都不丢失 
使用durable=True声明queue是持久化

'''
channel.queue_declare(queue='西游记',durable=True)
channel.basic_publish(exchange='',
                      routing_key='西游记', # 消息队列名称
                      body='大师兄，师父被妖怪抓走了',
                      # 支持数据持久化
                      properties=pika.BasicProperties(
                          delivery_mode=2,#代表消息是持久的  2
                      )
                      )
connection.close()


	消费者.py 
	import pika
	#建立和服务端的连接部分
	credentials = pika.PlainCredentials("heiheihei","123")
	connection = pika.BlockingConnection(pika.ConnectionParameters('192.168.16.37',credentials=credentials))
	channel = connection.channel()
	# 确保队列持久化
	channel.queue_declare(queue='西游记',durable=True)
	
	'''
	必须确保给与服务端消息回复，代表我已经消费了数据，否则数据一直持久化，不会消失
	'''
	def callback(ch, method, properties, body):
		print("消费者接受到了任务: %r" % body.decode("utf-8"))
		# 模拟代码报错
		# int('asdfasdf')    # 此处报错，没有给予回复，保证客户端挂掉，数据不丢失
	   
		# 告诉服务端，我已经取走了数据，否则数据一直存在
		ch.basic_ack(delivery_tag=method.delivery_tag)
	# 关闭no_ack，代表给与回复确认
	channel.basic_consume(callback,queue='西游记',no_ack=False)
	channel.start_consuming()


总结一下
rabbitmq-server
消息队列，支持持久化的队列，和非持久化的队列
以及消息的确认机制，ack和no_ack的区别

saltstack学习
1.环境准备，准备2台服务器，一个是master一个是minion

master  192.168.16.37
minion  192.168.16.140




2.在两台机器上，分别配置hosts文件，  配置本地dns服务器(dnsmasq 小型dns服务器)
因为ip的通信，不易识别，且通信较慢，配置hosts强制的本地域名解析，通信方便
vim /etc/hosts
192.168.16.140 s19minion
192.168.16.37 s19master


关闭防火墙策略，以及关闭服务 ,selinux 内置的linux防火墙 ,iptables这是软件防火墙，还有硬件防火墙, nginx(web服务，反向代理，负载均衡),软件负载均衡，  f5硬件负载均衡
iptables -F  #清空规则
systemctl stop firewalld  #关闭网络服务器


3.在两台机器上，分别配置好yum源，下载saltstack软件
在master机器上 yum install salt-master    -y 
在minion机器上 yum install salt-minion   -y  



4.分别修改两个软件的配置文件，需要互相指定对方

编辑master的配置文件，这个配置文件是yaml语法的，不得有任何的错误，以及无用空格，必须按着语法来
vim /etc/salt/master  检查如下配置参数

[root@master ~]# grep -v ^# /etc/salt/master|grep -v ^$

interface: 0.0.0.0  #绑定到本地的0.0.0.0地址
publish_port: 4505　　#管理端口，命令发送
user: root　　　　　　#运行salt进程的用户
worker_threads: 5　　#salt运行线程数，线程越多处理速度越快，不要超过cpu个数
ret_port: 4506　　#执行结果返回端口
pidfile: /var/run/salt-master.pid #pid文件位置
log_file: /var/log/salt/master　　#日志文件地址


下属minion的配置如下

[root@s19minion ~]# grep -v "^#" /etc/salt/minion |grep -v "^$"
master: s19master
master_port: 4506
user: root
id: s19minion
log_file: /var/log/salt/minion



5.分别启动master和minion，注意了，salt-minion发送秘钥给master，是在重启的一瞬间发送的
systemctl start salt-master 

systemctl start salt-minion 

6.进行master秘钥认证，管理minion
	对秘钥身份进行，查看匹配
	在服务端查看minion的秘钥信息
	salt-key -f s19minion 
	

	在客户端minion查看自己的秘钥信息 
	 salt-call --local key.finger
	
	[root@s19master ~]# salt-key -L
	Accepted Keys:
	Denied Keys:
	Unaccepted Keys:
	s19minion
	Rejected Keys:

#接受minion的秘钥，远程管理 
	salt-key -a  s19minion   #接受下属的秘钥信息


7.学习salt的命令
salt-key -L #列出所有主机的秘钥信息
salt-key -a  秘钥id  #接受一个秘钥id
#常用参数
-L  #查看KEY状态
-A  #允许所有
-D  #删除所有
-a  #认证指定的key
-d  #删除指定的key
-r  #注销掉指定key（该状态为未被认证）

#   "*" 目标匹配字符串，*代表所有的被管控机器，  
salt   "*"  cmd.run   "你要远程执行的命令"
salt  "你要匹配的minion的id"   test.ping  #检测从机器是否存活

salt --out=json  "*" test.ping  #输出结果以json格式输出

salt '*' sys.list_functions test    #输出test模块下的所有方法 
salt "*" test.fib  50  #让所有的从机器，计算一下 斐波那契数列的值

salt "*" cmd.run  "yum remove  nginx -y"  # salt的cmd.run是一个万能模块，可以让从机器，执行任何的命令

salt提供了安装软件的模块命令，pkg模块就是调用的yum工具 
salt '*' pkg.install "nginx"   #所有的从机器就会执行 yum install nginx 

salt的远程管理模块
salt '*' service.start "nginx"   #告诉从机器，执行 systemctl start nginx  
salt '*' service.stop  "nginx"    #远程关闭nginx服务 

执行salt命令的输出格式
#输出json的数据，可以反序列化 
salt --out=json  "*"  cmd.run "hostname"
salt --out=yaml  "*"  cmd.run "hostname"

#输出yaml的格式
salt --out=yaml  "*"  cmd.run "hostname"




写出你见过的配置文件格式，功能参数写在文件中

my.cnf
nginx.conf 
uwsgi.ini 
xx.cfg  配置文件
xx.xml   java常用的配置文件格式
xx.yml   用的yaml语法的配置文件 
xx.json  


yaml的语法学习


python的字典套字典
{
	"s19": {
		"男同学":["嘿嘿嘿","刀疤哥","超弟"],
		"女同学":["文刚","刚妹","李高旺"],
	}
}


转化成yaml语法格式
http://www.bejson.com/validators/yaml/  在线的yaml转化练习
"s19":
  "男同学":
    - "嘿嘿嘿"
    - "刀疤哥"
  "女同学":
    - "文刚"
    - "刚妹儿"




沙河的一只鵰 -- 沙雕 


salt的静态数据采集之  grains，可以在minion启动的时候，自动的把自己所有硬件资产信息，发送给master

salt --out=json 's19minion' grains.items  #列出主机的所有信息，以json格式返回
salt  's19minion' grains.item os  
salt  's19minion' grains.item num_cpus
salt  's19minion' grains.item ipv4    #以key的形式取出单独的值

python操作salt的api命令
pip3 install salt 





linux安装pycharm进行图形化开发