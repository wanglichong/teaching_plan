# day73

## 内容回顾

RBAC   基于角色的权限控制

表结构

简单权限控制

权限表

​	url      —— 权限    正则表达式   没有^$ 

​	title    —— 标题

角色表

​	name

用户表

​	name  用户名

​	pwd  密码

用户和角色的关系表

角色和权限的关系表

一级菜单的表结构：

权限表

​	url      —— 权限    正则表达式   没有^$ 

​	title    —— 标题

​	is_menu   —— 是或是菜单

​	icon   ——  图标

二级菜单的表结构

菜单表

​	title  

​	icon    

权限表

​	url      —— 权限    正则表达式   没有^$ 

​	title    —— 标题

​	menu   —— 外键  关联菜单表   menu_id

二级菜单的数据结构：

menu_dict = {

​	menu_id: {

​		title ： 一级菜单的标题

​		icon : 一级菜单的图标

​		children ： [

​			{ url      title  }   ——  二级菜单的信息

​		]

​	}

}

## 今日内容

1.一级菜单的排序

修改表结构  加一个权重的字段

```
from collections import OrderedDict
sorted(menu_dict, key=lambda x: menu_dict[x]['weight'], reverse=True)
for key in keys:
	order_dict[key] = menu_dict[key]
```

2.二级菜单默认选中并且展开

选中    class active 

展开   body  hide属性去除  

默认所有的都是闭合

正则匹配 二级菜单的url匹配 当前url

​	匹配成功

​	   给二级菜单加上 class = active 

​	   给一级菜单下的body 的class = hide  移除掉 

3.非菜单权限的归属

客户管理

​	客户列表

​		添加客户

​		编辑客户

​		删除客户

财务管理

​	缴费列表

权限表	

id   	  url    	 			 	 title    	menu_id           parent_id

1 		/customer/list/		 客户列表		 1		   null

2 		/customer/add/		添加客户		null   	      1

3 		/customer/edit/		编辑客户		null   	      1



访问二级菜单或者子权限的时候   保存 要选中的二级菜单的ID  （权限的中间 ）

生成二级菜单的视频 判断依据 就是   保存 要选中的二级菜单的ID   == 当前循环的二级菜单的ID（inclusion_tag）

​	相等

​	  	 给二级菜单加上 class = active 

​	   	给一级菜单下的body 的class = hide  移除掉 

4.路径导航

```
import json

ret = json.dumps(permission_dict)
print(ret)

ret = json.loads(ret)
print(ret)
注意问题：
字典的key为数字，json序列化后数字变数字字符串
```

5.权限控制到按钮级别





