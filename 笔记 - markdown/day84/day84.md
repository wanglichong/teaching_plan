linux的容器技术
实现了容器技术的软件---docker


dev 开发
ops  运维 

devops


互联网公司的技术栈
老企业 （贵）
java + jquery + oracle(甲骨文数据库) + redhat(红帽操作系统)  + svn(git)(代码版本控制)  + mencached(缓存数据库) + apache(web服务器)

互联网企业（穷，省钱），（都是开源免费的软件）
python + nodejs + vue + centos7  + git + redis   + nginx + mysql  + rabbitmq + docker 



vmware虚拟化技术

个人学习版本：vmware workstaion (性能较低，只能运行10几个虚拟机)
企业版本虚拟化：vmware vsphere (运行处esxi虚拟化服务器)
				256G内存的物理服务器，
					虚拟化处了100+的linux

解决环境部署问题的办法：
1.虚拟化模板克隆

docker的三大生命周期

1.容器
2.镜像
3.仓库 

基于docker镜像(操作系统镜像)  运行处 容器实例（操作系统实例+应用）
							 运行处 容器实例（操作系统实例+应用）
							 运行处 容器实例（操作系统实例+应用）
							 运行处 容器实例（操作系统实例+应用）
							 运行处 容器实例（操作系统实例+应用）
							 运行处 容器实例（操作系统实例+应用）
							 运行处 容器实例（操作系统实例+应用）
							 运行处 容器实例（操作系统实例+应用）





dvd系统镜像.iso    基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 
					基于它 运行出 操作系统实例 

docker镜像（如同类的关系），基于镜像运行处容器（类的实例化）
class Student():
	def __init__(self):
		print("安装python3")
		print("安装python模块依赖")
		print("安装虚拟环境....")
		print("休息一会")
		
s1=Stduent()
s2=Stduent()
s3=Stduent()
s4=Stduent()
s5=Stduent()
s6=Stduent()



docker镜像   可以在dockerhub下载，也可以通过dockerfile构建镜像
docker容器    基于docker镜像，运行出的实例
docker仓库   存储docker镜像的地方，就是一个仓库，公网的仓库是 dockerhub，  还有私人的仓库

centos7安装docker
1.yum安装，阿里云的yum源，腾讯云yum源，清华源的yum源，最正确的官方的yum仓库（版本最新，问题是下载会很慢 ）
yum install docker -y 

2.启动docker
systemctl start docker 
systemctl status docker  # 验证docker是否启动

docker --version 

配置docker加速器，加速镜像下载查找
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://f1361db2.m.daocloud.io
#这里还得修改docker的配置.
vim /etc/docker/daemon.json，编辑如下配置，删除一个逗号
{"registry-mirrors": ["http://f1361db2.m.daocloud.io"]}



3.docker的命令学习，对镜像，容器的管理命令

1.获取镜像
2.运行镜像，生成容器
3.对容器进行管理



镜像：
增删查

docker search 镜像名字  #其实是去 docker hub  搜索镜像名

docker search hello-world  #搜索docker镜像
docker pull hello-world  #下载docker镜像 
docker image ls  #查看当前机器的docker镜像
docker images      #同上，查看本机的所有docker镜像
docker rmi  镜像名/镜像id   #删除docker镜像 


容器：
增删改查
docker run  镜像名字/镜像id     #运行docker镜像，生成容器记录
docker run  不存在的镜像名   #会先docker pull下载这个镜像，然后在自动运行 

docker container ls    #列出当前机器的所有容器(正在运行的容器，正在 运行的docker进程 )
docker  ps   #查看docker进程，docker容器记录的
docker ps -a  #查看docker所有的进程，以及挂掉的进程

运行一次镜像，就会生成一个容器记录
docker容器必须有正在运行的进程，否则容器就会挂掉

docker  rm  容器id/容器名   #删除容器记录(只能删除挂掉的容器记录)
docker rm `docker ps -aq`   #一次性删除所有docker容器记录，只能删除挂掉的记录 
docker run -d centos /bin/sh -c "while true;do echo hello centos; sleep 1;done"   #在后台运行docker容器
docker logs  容器id  #查看容器内的日志信息 
docker logs -f  容器id  #实时查看容器的日志 
docker exec -it  容器id   /bin/bash   #用exec指令，进入到容器系统内
	-i  交互式的shell命令方式
	-t  开启一个终端，去运行
docker exec  -it  容器id   /bin/bash  # 进入一个已经在运行的docker容器中
docker run -it ubuntu  /bin/bash      #交互式的运行ubuntu容器，且进入容器空间内


运行一个centos的docker容器
1.下载docker镜像，且是centos的镜像 
docker pull centos 

2.运行centos镜像，生成容器
docker run  centos   #这个容器，没有任何后台的进程，所以直接挂掉

3.运行一个后台有进程的容器
docker run -d centos /bin/sh -c "while true;do echo hello centos; sleep 1;done"
	-d daemonize 后台运行的意思 
	centos  指定一个镜像去运行
	/bin/sh   指定centos的解释器
	-c  指定一段shell语法
	"while true;do echo hello s19; sleep 1;done"  #每秒打印一次hello centos  




提交自定义的docker镜像
1.准备一个centos的docker容器，默认是没有vim的
docker run  -it  centos   /bin/bash  #创建启动一个docker容器，且是centos系统的

2.退出docker容器空间，然后提交这个容器，生成一个新的镜像文件
docker commit  你想要提交的容器id  你想创建的镜像名

3.查看你创建的镜像
docker images   

4.导出你提交的镜像，成为一个压缩文件
docker  save  yuchao163/s19-centos-vim  >  /opt/myimage.tar.gz


5.把这个压缩文件，传递给其他人，其他人导入这个镜像文件，然后运行，默认就携带了vim编辑器
docker load <  /opt/myimage.tar.gz



运行一个web内容的docker容器
docker run -d -P training/webapp python app.py
	-d  后台运行
	-P  大写的字母p参数，意思是随机映射一个端口 
	
	
	用户 192.168.16.37:8000   >  我的服务器 192.168.16.37:8000 如果是直接运行在物理机的8000端口，就直接响应 
							<  


​							
	用户 192.168.16.37:8000   >  我的服务器 192.168.16.37:8000 访问的不再是物理机上的进程，而是容器内的进程	 >   容器内的一个进程和端口 	


​	


docker的web应用：
#后台运行一个python web代码，且进行端口映射-P ，运行处一个docker容器进程

查看容器进程，结果是，宿主机的32768端口，映射到了容器的5000端口

[root@nginx1 opt]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                     NAMES
b41f1bb31493        training/webapp     "python app.py"          2 minutes ago       Up 2 minutes        0.0.0.0:32768->5000/tcp   tender_euclid


1.创建，且运行一个web容器
docker run -d -P training/webapp python app.py
2.查看容器的端口映射关系
[root@nginx1 opt]# docker port b41
5000/tcp -> 0.0.0.0:32768

3.访问宿主机的32768,即可访问到容器的5000应用

4.启停这个容器应用
docker start 容器id
docker stop 容器id 

5.指定端口映射关系 
-p  宿主机端口:容器内的端口 
docker run -d -p 8888:5000  training/webapp python app.py













docker镜像的获取方式
1.去网络仓库下载 (不可控制，镜像内的内容 )
docker pull  centos 
2.把其他人发来的docker镜像，进行导入(不可控，镜像内容也是其他人定制的)
3.dockerfile通过脚本，自定义docker镜像(把部署过程写入到一个脚本当中,学习dockerfile的指令，去部署即可)


dockerfile的学习
dockerfile作用是自定义一个docker镜像，学习dockerfile的语法指令 





#每一个应用程序运行，必须得有一个系统作为载体
#定义以哪一个基础镜像去运行docker容器
FROM scratch #制作base image 基础镜像，尽量使用官方的image作为base image
FROM centos #使用base image
FROM ubuntu:14.04 #带有tag的base image


LABEL version=“1.0” #容器元信息，帮助信息，Metadata，类似于代码注释
LABEL maintainer=“yc_uuu@163.com"   #告诉别人，这个dockerfile是谁写的



#RUN 万能的指令，去执行你输入的命令 

#对于复杂的RUN命令，避免无用的分层，多条命令用反斜线换行，合成一条命令！
RUN yum update && yum install -y vim \
    Python-dev #反斜线换行
RUN /bin/bash -c "source $HOME/.bashrc;echo $HOME”

#可以用RUN指令，告诉docker，去自动的装哪些程序依赖 





WORKDIR /root #相当于linux的cd命令，改变目录，尽量使用绝对路径！！！不要用RUN cd
WORKDIR /test #如果没有就自动创建
WORKDIR demo #再进入demo文件夹
RUN pwd     #打印结果应该是/test/demo








ADD and COPY 
ADD hello /  #把本地文件添加到镜像中，吧本地的hello可执行文件拷贝到镜像的/目录
ADD test.tar.gz   /  #添加到根目录并解压



ADD指令用于把物理机上的文件，添加到容器空间内，并且还有解压缩的作用

ADD  /opt/一脸懵逼.jpg    /opt/  #把物理机的linux /opt/下的一张jpg图片，传输到 容器空间内的/opt目录下 








WORKDIR /root
ADD hello test/  #进入/root/ 添加hello可执行命令到test目录下，也就是/root/test/hello 一个绝对路径
COPY hello test/  #等同于上述ADD效果

ADD与COPY
   - 优先使用COPY命令

    -ADD除了COPY功能还有解压功能

COPY指令的作用，是吧物理机的文件，拷贝到容器空间内，仅仅一个作用 




​	
添加远程文件/目录使用curl或wget

ENV #环境变量，尽可能使用ENV增加可维护性
ENV MYSQL_VERSION 5.6 #设置一个mysql常量



“${MYSQL_VERSION}” 
ENV指令 用于定义变量 

dockerfile的实际应用，创建一个flask镜像，基于这个flask镜像，运行出web 容器id

1.准备flask的代码
myflask.py 内容如下，基于python2去运行代码，省事

安装flask的模块命令
easy_install flask 

2.准备dockerfile的编写
	#无注释版
	FROM centos 
	COPY CentOS-Base.repo   /etc/yum.repos.d/
	COPY epel.repo /etc/yum.repos.d/   				
	COPY myflask.py /opt/						
	RUN yum clean all 							
	RUN yum install python-setuptools -y	 
	RUN easy_install flask						
	WORKDIR /opt								
	EXPOSE 8080								
	CMD ["python","myflask.py"]  
	
	#有注释版
	FROM centos  #以centos为基础操作系统
	COPY CentOS-Base.repo   /etc/yum.repos.d/        #修改容器内的yum源，把本地的repo文件，传输到容器内的yum仓库目录下 
	COPY epel.repo /etc/yum.repos.d/   				#同上，用于安装yum源  
	COPY myflask.py /opt/						#把本地的flask代码文件，拷贝到容器空间内
	RUN yum clean all 								#让容器执行这个命令，清空原本的yum缓存
	RUN yum install python-setuptools -y		#centos默认没有easy_install命令，无法安装模块，需要下载软件包 
	RUN easy_install flask						#容器自动的执行 安装flask模块的命令 
	WORKDIR /opt								#切换容器内的工作目录  
	EXPOSE 8080									#开放容器内的8080端口，提供给宿主机去映射
	CMD ["python","myflask.py"]  				#容器去执行这一段命令，




3.准备yum仓库文件，阿里云的两个文件
最终目录文件信息如下：
[root@nginx1 s19docker]# ls
CentOS-Base.repo  Dockerfile  epel.repo  myflask.py



4.打包构建docker镜像
docker build .   

5.查看构建好的docker镜像
docker imags 

6.修改镜像的名字
docker tag 5e0 yuchao163/s19-centos-flask

7.临时补充，运行docker容器时，添加上名字
docker run --name 容器的名字    运行的镜像名


8.基于咱们自定义的flask镜像，生成容器实例
docker run -d -p 7000:8080 镜像名/镜像id



docker的公有仓库：
1.docker提供了一个类似于github的仓库dockerhub,
网址https://hub.docker.com/需要注册使用
2.注册docker id后，在linux中登录dockerhub
docker login

注意要保证image的tag是账户名，如果镜像名字不对，需要改一下tag
docker tag chaoyu/centos-vim yuchao163/centos-vim
语法是：  docker tag   仓库名   yuchao163/仓库名


3.推送docker image到dockerhub
docker push yuchao163/centps-cmd-exec:latest
4.在dockerhub中检查镜像
https://hub.docker.com/
5.删除本地镜像，测试下载pull 镜像文件
docker pull yuchao163/centos-entrypoint-exec





docker的私有仓库:
1.下载docker官方提供的私有仓库镜像（提供了私有仓库的功能）
docker pull  registry  #下载私有仓库镜像
	
	-v的作用是，数据卷挂载，文件夹映射关系 
http://192.168.16.37:5000/v2/_catalog  存放数据的API(就是一段提供数据的URL) 地址 

2.修改docker的配置文件，支持非https的方式上传镜像
vim /etc/docker/daemon.json ，修改为如下配置

{"registry-mirrors": ["http://f1361db2.m.daocloud.io"],
"insecure-registries":["192.168.16.37:5000"]
}

3.还得修改docker的启动配置文件
修改如下文件/lib/systemd/system/docker.service

systemctl start docker  其实调用的就是这个文件/lib/systemd/system/docker.service

vim  /lib/systemd/system/docker.service # 编辑这个文件，添加如下参数
找到一个[Service]的配置快，改成如下配置

[Service]
Type=notify
NotifyAccess=main
EnvironmentFile=-/run/containers/registries.conf
EnvironmentFile=-/etc/docker/daemon.json      #改动这个东西 


4.重新加载docker服务
systemctl daemon-reload
5.重启docker服务
systemctl restart docker

6.由于重启了docker，所有的容器都挂掉了，还得重新再运行一下docker私有仓库
docker run --privileged=true -d -p 5000:5000 -v /opt/data/registry:/var/lib/registry registry
	-d  后台运行
	-p 端口映射
	-v 数据卷挂载
	 --privileged=true    #设置特权运行的容器


7.推送本地的镜像文件，到私有仓库中
docker push 192.168.16.37:5000/s19-hello-world


8.下载私有仓库的镜像
docker pull 192.168.16.37:5000/s19-hello-world


docker pull 镜像 
docker run   镜像文件  #运行出一个新的容器实例 
docker run -it  镜像id   #创建一个新的容器，并且进入容器内 

docker start/stop  容器id   #对已经存在的容器实例，进行启停

docker exec 容器id   #进入一个正在运行的docker容器中 



云服务器(centos7 ，宿主机系统)  +  docker软件 (实现了docker的镜像，容器，基于centos镜像，运行出容器实例（这个容器实例就是一个微型的centos）)


镜像image（就是一堆文件）




​	
​	
​	