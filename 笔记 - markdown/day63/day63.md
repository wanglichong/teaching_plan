# day63

## 内容回顾

### 1.orm

#### 必知必会13条

返回对象列表

all  

filter  

exclude

order_by

reverse

values 

values_list 

distinct 

返回对象

get

first

last

返回布尔值

exists

返回数字

count

#### 单表双下划线

__gt  

__gte 

__lt

__lte

__in = []

__range = [1,5]

__contains = ''      like

__startswith

__endswith

__isnull= True 

__year

#### 聚合和分组

aggregate()     终止子句

annotate()

```
models.Publisher.objects.annotate(max=Max('book__price'))
models.Book.objects.values('publisher').annotate(max=Max('price'))
```

#### F 和 Q

F   动态获取当前字段的值

sale__gt = F('kucun')         update(sale=F('sale')*2)

Q   条件

filter（Q(pk__gt=5)）

|  或

&  与

~  非

#### 事务  

```
from django.db import transaction

try:
   with transaction.atomic():
      # 一系列操作
   
except Exception as e:
   print(e)
      
```

### 2.cookie

#### 1.定义

cookie就是保存在浏览器上一组组键值对，服务器让浏览器进行设置的。

#### 2.为什么要有cookie？

HTTP协议是无状态，每次请求都是没有关系，没办法保存状态。使用cookie保存状态。

#### 3.特性

1. 保存浏览器本地
2. 服务器让浏览器进行设置的，浏览器也有权不设置
3. 下次访问服务器时自动携带相应的cookie

#### 4.django中的操作

1. 设置        响应头   set-cookie 

   ret = HttpResponse('ok')

   ret.set_cookie(key,value ,max_age=5)    # 普通的cookie

   

   ret.set_signed_cookie(key,value ,max_age=5,salt='xxxx')

2. 获取

   request.COOKIES[KEY]

   request.COOKIES.get(KEY)

   

   request.get_signed_cookie(key,salt='xxxx',default='')

3. 删除    

   ret = HttpResponse('ok')

   ret.delete_cookie(key)    # 普通的cookie

## 今日内容

### 1.session

   保存在服务器上一组组键值对，必须依赖于cookie。

####    为什么要使用session？

   1. cookie保存在浏览器本地，不安全
   2. cookie的大小受到限制

####    django中操作session

   1. 设置

      request.session[key] = value

   2. 获取

      request.session[key] 

      request.session.get(key)

3. 删除


   del request.session['k1']

      request.session.clear_expired() # 清除已过期的数据
      request.session.delete()     ＃　删除所有的session数据   不删除cookie
      request.session.flush()      ＃　删除所有的session数据  删除cookie

   from django.conf import global_settings

### 2.中间件

定义：

​	中间件是在全局范围内处理django的请求和响应的钩子。本质是就是一个类。

五种方法：（执行时间、执行顺序、参数、返回值）

- process_request(self,request)
- process_view(self, request, view_func, view_args, view_kwargs)
- process_template_response(self,request,response)
- process_exception(self, request, exception)
- process_response(self, request, response)

####    process_request(self,request)

执行时间：

​	视图函数执行之前

执行顺序：

​	按照注册的顺序   顺序执行 

参数：

​	request   -   和视图中的request对象是用一个

返回值：

​	None:  正常流程

​	HttpResponse对象：  后面的中间件的process_request方法、视图都不执行，直接执行当前中间件的process_response方法，再返回给浏览器

#### process_response(self, request, response)

执行时间：

​	视图函数执行之后

执行顺序：

​	按照注册的顺序   倒序执行 

参数：

​	request   -   和视图中的request对象是用一个

​	response  -  视图函数返回的响应对象

返回值：

​	HttpResponse对象：  必须返回

#### process_view(self, request, view_func, view_args, view_kwargs)

执行时间：

​	在process_request方法后，视图函数执行之前

执行顺序：

​	按照注册的顺序   顺序执行 

参数：

​	request   -   和视图中的request对象是用一个

​	view_func - 视图函数

​	view_args  - 给视图使用的位置参数

​	view_kwargs - 给视图使用的关键字参数

返回值：

​	None:  正常流程

​	HttpResponse对象：  后面的中间件的process_view方法、视图都不执行，直接执行最后一个中间件的process_response方法，再返回给浏览器

#### process_exception(self, request, exception)

执行时间(触发的条件)：

​	视图函数执行之后，视图有异常时才执行

执行顺序：

​	按照注册的顺序   倒序执行 

参数：

​	request   -   和视图中的request对象是用一个

​	exception -  错误的对象

返回值：

​	None:  正常流程 ，交给下一个中间件处理异常，都返回的是None，交给django处理异常（大黄页）

​	HttpResponse对象：  后面的中间件的process_exception方法不执行，直接执行最后一个中间件的process_response方法，再返回给浏览器

#### process_template_response(self,request,response)

执行时间(触发条件)：

​	视图函数执行之后，要求视图函数返回的对象是TemplateResponse对象

执行顺序：

​	按照注册的顺序   倒序执行 

参数：

​	request   -   和视图中的request对象是用一个

​	response  -  视图函数返回的响应对象

返回值：

​	HttpResponse对象：  必须返回



作业：

1. 中间件版的登录验证
2. 限制访问频率
   1. 5秒钟只能访问3次



