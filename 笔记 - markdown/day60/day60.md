# day60

## 内容回顾

### 视图 

#### 1. FBV   CBV 

定义;

```python
from django.views import View

class AddPublisher(View):
    
    def get(self,request):
        return response
    
    def post(self,request):
        return response
    
```

使用:

from app01 import views

url(r'add_publulisher',views.AddPublisher.as_view() )

#### 2. as_view的流程：

1. 程序执行时，会执行as_view（）  ——》   返回view函数

​	url(r'add_publulisher',   view  )

2. 请求到来的时候，执行view函数：

   1. 实例化AddPublisher（*args，**kwargs）  ——》 self

   2. self .request = request 

   3. 执行self.dispatch()     ——》   AddPublisher中有就执行，没有就找View中的dispatch() 

      1. 判断请求方法       ——》    http_method_names  [ ]

         1. 允许

            通过反射获取到当前请求方式对应的方法（get/post） ——》 handler

         2. 不允许

            ``` 
            self.http_method_not_allowed ——》 handler
            ```

      2.    执行handler方法  ——》返回 response

#### 3. 加装饰器的方法

CBV

```
from django.utils.decorators import method_decorator 方法装饰器
```

1. 加在方法上

```
@method_decorator(timer)
def get(self, request):
```

2. 加在类上

```python
@method_decorator(timer,name='post')
@method_decorator(timer,name='get')
class AddPublisher(View):
```

3. 加在dispatch上

```python
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs):
   ret = super().dispatch(request, *args, **kwargs)
   return ret

@method_decorator(timer,name='dispatch')
class AddPublisher(View):
```

#### 4. request

1. request.method   ——》 请求方式
2. request.GET    ——》  URL上携带的参数   {} 
3. request.POST  ——》 POST请求提交的数据   {}
4. request.body   ——》 请求体中的数据   字节
5. request.FILES  ——》 上传的文件
   1. form表单编码方式  enctype="multipart/form-data"
   2. POST  {% csrf_token %}
   3. file_obj.chunks()
6. request.META    ——》 请求头     全大写   HTTP_     -  ——》 _
7. request.path_info   ——》  路径   不包含 IP和端口 、参数
8. request.COOKIES    ——》 cookie
9. request.session     ——》 session
10. request.get_full_path()    ——》 完整的路径信息    不包含 IP和端口   包含参数 
11. request.is_ajax()     ——》  是否是ajax请求
12. request.get_host()  ——》   获取主机的IP和端口

#### 5. response

1. HttpResponse('字符串')     ——》 返回字符串    Content-Type  :   'text/html'

2. render(request,'模板的文件名',{k1:v1})    ——》  返回一个完整的HTML页面

3. redirect('要跳转的地址')    ——》 重定向   Location：'要跳转的地址

   ret  = HttpResponse('ok')   ret['Location']  = '/index/'    return ret  

4. ```
   from django.http.response import JsonResponse
   
   JsonResponse(字典)     # Content-Type: 'application/json'
   JsonResponse(非字典类型，safe = False) 
   ```

## 今日内容

### 路由系统

https://www.cnblogs.com/maple-shaw/articles/9282718.html

#### 1. 正则表达式

r''

最开始的/不用加

从上到下进行匹配  匹配成功下面就不在继续    ——》  ^ $  

[abc]   [0-9]  {}    ?   +  * .    \d    \w 

#### 2. 分组和命令分组

分组  （）

```
url(r'^blog/([0-9]{4})/([0-9]{2})/$', views.blog),
```

​	分组的结果会当做位置参数    ——》 传递给视图函数

命名分组  （?P<name>）

```
url(r'^blog/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blog),
```

​	分组的结果会当做关键字参数    ——》 传递给视图函数

#### 3. include

路由分发

```Python
from django.conf.urls import url,include
from app01 import views

urlpatterns = [

   url(r'^app01/', include('app01.urls')),
   url(r'^app02/', include('app02.urls')),
]
```

#### 4. URL命名和反向解析

##### 静态地址

命名

```
url(r'^blog/$', views.blogs, name='xxxx'),
```

使用：

​	模板

​	{% url  'xxxx'   %}   ——》  '/blog/'    一个完整的URL  

​	py文件 

​	from django.urls import  reverse

​	reverse('xxxx')    ——》   '/blog/'

##### 分组

命名

```
url(r'^blog/([0-9]{4})/([0-9]{2})/$', views.blog,name='blog' ),
```

使用：

​	模板

​	{% url  'blog'  ‘2019’  ‘12’  %}   ——》  '/blog/2019/12/'    一个完整的URL  

​	py文件 

​	from django.urls import  reverse

​	reverse('blog',args=('2018','02'))    ——》   '/blog/2018/02'  

##### 命名分组

命名

```
url(r'^blog/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.blog,name='blog' ),
```

使用：

​	模板

​	{% url  'blog'  ‘2019’  ‘12’  %}   ——》  '/blog/2019/12/'    一个完整的URL  

​	{% url 'blog' month='09' year='2016' %}     '/blog/2016/09/'  

​	py文件 

​	from django.urls import  reverse

​	reverse('blog',args=('2018','02'))    ——》   '/blog/2018/02'  

​	reverse('blog', kwargs={'year': '2020', 'month': '03'})    ——》 /blog/2020/03/

#### 5. namespace

```python
urlpatterns = [
   url(r'^admin/', admin.site.urls),
   url(r'^app01/', include('app01.urls',namespace='app01')),
   url(r'^app02/', include('app02.urls',namespace='app02')),

]
```

{%  url   'namespace:name'  参数 %}

reverse( 'namespace:name' ,arrgs=(),kwargs={} )

