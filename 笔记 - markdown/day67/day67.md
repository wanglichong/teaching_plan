# day67

## 内容回顾

```
from django import forms
from crm import models

class Regform(forms.Form):
   username = forms.CharField(
      max_length=12,
      min_length=6,
      initial='username',
      label='用户名',
      validators=[],
      widget=forms.TextInput(attrs={}),
      error_messages={
         'required':'必填',
         'max_length':'最大长度为12位',
      }
   )
   password = forms.CharField(
      max_length=12,
      min_length=6,
      initial='username',
      label='用户名',
      validators=[],
      widget=forms.PasswordInput(attrs={}),
      error_messages={
         'required': '必填',
         'max_length': '最大长度为12位',
      }
   )
   
class Regform(forms.ModelForm):
   password = forms.CharField(
      max_length=12,
      min_length=6,
      initial='username',
      label='用户名',
      validators=[],
      widget=forms.PasswordInput(attrs={}),
      error_messages={
         'required': '必填',
         'max_length': '最大长度为12位',
      }
   )
   
   re_password = forms.CharField(
      max_length=12,
      min_length=6,
      initial='username',
      label='用户名',
      validators=[],
      widget=forms.PasswordInput(attrs={}),
      error_messages={
         'required': '必填',
         'max_length': '最大长度为12位',
      }
   )
   
   class Meta:
      model = models.UserProfile
      fields = ['username','password','re_password']
      exclude = ['is_active']
      
      widgets ={
         'password':forms.PasswordInput(attrs={})
      }
      
      error_messages ={
         'password':{
            
         }
      }
```

## 今日内容

### 1. 展示客户列表

```python
字段的展示：

	1. 普通字段
    	{{ 对象.字段名 }}
	2. choice参数
    	对象.字段名   ——》 数据库的值
        对象.get_字段名_display()      ——》 中文
        {{ 对象.get_字段名_display }}
    3. 自定义方法
    	def show_classes(self):
			return ' | '.join([str(i) for i in self.class_list.all()])
        
        {{ 对象.show_classes }}
```

```
from django.utils.safestring import mark_safe
mark_safe(<span style="background-color: {};color: white;padding: 4px">{}</span>)
```

### 2. 分页

见代码