# day62

## 内容回顾

### 1.orm

ORM:对象关系映射   面向对象和关系型数据库的映射关系  通过操作对象的方法和属性操作数据库

对应关系：

​	类   ——》  表

​	对象  ——》 数据行（记录）

​	属性  ——》 字段

### 2.必知必会13条

返回对象列表的：

​	all      获取表中所有的对象

​	filter     获取满足条件的所有的对象

​	exclude     获取不满足条件的所有的对象

​	order_by    排序    默认升序     加- 降序  

​	reverse      对已经排序的对象列表进行倒序 

​	values        获取对象的字段名和值    {}   不带参数——》 获取所有字段的名和值         指定字段 

​	values_list        获取对象的值     ()     不带参数——》 获取所有字段值         指定字段 

​	distinct      去重   

返回对象：

​	get      没有或者多个就报错

​	first     取第一个

​	last      取最后一个

返回布尔值：

​	exists    是都存在

返回数字

​	count    计数

### 3.单表的双下划线

id__gt  

id__lt

id__gte

id__lte

id__range=[1,5]

id__in=[ 1,5  ]

name__startswith='a'         

name__istartswith='a'

name__endswith='a'

name__contains='a'     # like

name__isnull = True   

date__year   

### 4.外键

表示一对多的关系

基于对象

正向查询     book ——》  publisher

book_obj.publisher     ——》    所关联的出版社对象

book_obj.publisher.name     ——》    所关联的出版社对象名称

book_obj.publisher.pk    ——》    所关联的出版社对象主键

book_obj.publisher_id     ——》    从book中直接拿到所关联对象的id



反向查询    publisher ——》 book

不指定related_name 

pub_obj.book_set      ——》   关系管理对象

pub_obj.book_set.all()      ——》 出版社出版所有的书籍对象 

指定related_name=’books‘

pub_obj.books      ——》   关系管理对象

pub_obj.books.all()      ——》 出版社出版所有的书籍对象 



基于字段查询

models.Book.objects.filter(publisher__name='人民出版社')



不指定related_name 

models.Publisher.objects.filter(book__name='跟金老板学开车')

指定related_name =’books‘

models.Publisher.objects.filter(books__name='跟金老板学开车')

指定related_query_name =’book‘

models.Publisher.objects.filter(book__name='跟金老板学开车')

### 5.多对多

基于对象的查询

author_obj.books      ——》   关系管理对象

author_obj.books.all()      ——》   关系管理对象



不指定related_name 

book_obj.author_set      ——》   关系管理对象

book_obj.author_set.all()      ——》 作者写过所有的书籍对象 

指定related_name=’authors‘

book_obj.authors——》   关系管理对象

book_obj.authors.all()      ——》 作者写过所有的书籍对象 



管理对象的方法

all       获取所有的对象

set     设置关系      多对多 [ id,id  ]   [对象，对象]      一对多     [对象] 

add     添加关系     多对多 id,id     对象，对象     一对多     对象 

remove   删除关系        一对多：必须设置外键可为空，才有remove  clear方法

clear       清空所有的关系

create     创建一个对象并且添加关系

​		book_obj.authors.create(name='xxxx')    

## 今日内容

### 1.聚合和分组

分组

select dept, avg('salary')  from employee group by dept;

### 2.F 和 Q

```
Q(pk__lt=3)

|  或
&  与
~  非
```

### 3.事务

```
from app01 import models
from django.db import transaction

try:
   with transaction.atomic():
      #   一系列操作
      models.Author.objects.create(name='asasdasdas')
      models.Author.objects.create(name='asasdasdas')
      models.Author.objects.create(name='asasdasdas')

except Exception as e:
   print(e)
```

### 4.cookie

https://www.cnblogs.com/maple-shaw/articles/9502602.html

定义：

​	cookie就是保存在浏览器本地的一组组键值对

特性：

 	1. 服务器让浏览器进行设置的
 	2. 保存在浏览器本地的，浏览器也不可以不保存
 	3. 下次访问时自动携带相应的cookie

cookie能做什么：

1. 登录
2. 保存用户的浏览习惯
3. 投票

django的操作：

1. 设置

   ```
   ret = redirect('/home/')
   ret.set_cookie('is_login', '1')   # 普通的cookie
   
   ret.set_signed_cookie('is_login', '1', 'day62')  # 加密的cookie
   
   ```

2. 获取

   request.COOKIES    {}    #  普通的cookie

   request.get_signed_cookie('is_login',salt='day62',default='')   # 加密的cookie

   

3. 删除

   ret = redirect('/login/')
   ret.delete_cookie('is_login')


5.session













​	