s19day82学习笔记;

整个部署端口设计

vue(静态文件夹dist)  变相的就是nginx返回vue的页面，端口是80  ,也是nginx.conf提供的 server{}虚拟主机提供的
↓

反向代理端口 8500  ，是通过nginx 实现的，nginx.conf中编写一些参数，接口速率限制，爬虫验证，封停ip地址

↓
后台django的地址 9000


昨日内容回顾
1.部署了crm
nginx+ uwsgi + django  + 虚拟环境 +  supervisor + mariadb 

2.部署一个前后端分离的项目
vue(js框架)(nginx返回vue的页面) +  django rest freamwork(提供api数据接口的框架)（后端）+ 虚拟环境 + sqlite + redis（key-value型的数据库）(课程信息，登录的账号密码)

后端部署过程：
	1.准备后端代码  django 
		wget https://files.cnblogs.com/files/pyyu/luffy_boy.zip
	2.解压缩代码
		unzip luffy_boy.zip
	3.修改django的settings.py配置文件，允许所有主机登录
		ALLOWED_HOSTS = ["*"]
	4.由于这个路飞用的是sqlite，所以不需要配置mysql数据库了
	5.创建路飞的虚拟环境，运行代码
		mkvirtualenv s19luffy_boy
	6.解决路飞运行，所需的模块依赖
		办法1：手动一个个下载安装模块
		pip3 install -i https://pypi.douban.com/simple django==2.0.11
		pip3 install -i https://pypi.douban.com/simple django-rest-framework
		pip3 install -i https://pypi.douban.com/simple  requests
		pip3 install -i https://pypi.douban.com/simple django-redis 
		pip3 install -i https://pypi.douban.com/simple  pycrypto
		pip3 install -i https://pypi.douban.com/simple  uwsgi
	

		办法2：找到为你所用的requitements.txt文件，这个文件里面，定义了模块名以及版本号
			pip3 freeze >> requitements.txt  #导出模块的命令
			#提示
			#我已经给你准备好了这个文件的信息
				[root@web02 opt]# cat requirements.txt
certifi==2018.11.29
chardet==3.0.4
crypto==1.4.1
Django==2.1.4
django-redis==4.10.0
django-rest-framework==0.1.0
djangorestframework==3.9.0
idna==2.8
Naked==0.1.31
pycrypto==2.6.1
pytz==2018.7
PyYAML==3.13
redis==3.0.1
requests==2.21.0
shellescape==3.4.1
urllib3==1.24.1
uWSGI==2.0.17.1

			#安装这个文件
			pip3 install -r requirements.txt 


​		
		7.安装uwsgi，运行路飞的后端，支持多进程的django
		#命令运行方式如下
		uwsgi --http :9000  --module luffy_boy.wsgi
		#配置文件方式如下
		
		vim uwsgi.ini 
		#写入如下内容

[uwsgi]
# the base directory (full path)
#填写你项目的绝对路径，第一层
chdir           = /opt/s19luffy/luffy_boy
#找到django的那个wsgi.py文件
#根据上面一条参数，的相对路径来写
module          = luffy_boy.wsgi 
#虚拟环境的绝对路径
home            = /root/Envs/s19luffy_boy
master          = true
#根据你的cpu核数来定义这个参数，启动多少个进程
processes       = 4
# the socket (use the full path to be safe
#指定你的django启动在什么地址上，并且是什么协议
#如果你用了nginx，进行反向代理，请用socket参数
#如果你用了nginx，进行反向代理，请用socket参数
#如果你用了nginx，进行反向代理，请用socket参数
socket          = 0.0.0.0:9000
#如果你没用nginx反向代理，想要直接访问django用这个参数
#如果你没用nginx反向代理，想要直接访问django用这个参数
#http = 0.0.0.0:8000
vacuum          = true
py-autoreload    =   1

#后台运行uwsgi，不占用窗口,并且把后台日志输出到 uwsgi.log文件中
daemonize  =  uwsgi.log






		8.启动路飞后端
		uwsgi --ini  uwsgi.ini 


​		
前端部署过程：	
安装nodejs的环境如下

1.下载nodejs的解释器用于编译vue代码
wget https://nodejs.org/download/release/v8.6.0/node-v8.6.0-linux-x64.tar.gz

2.解压缩node的源代码包
[root@nginx1 bin]# pwd
/opt/s19luffy/node-v8.6.0-linux-x64/bin
[root@nginx1 bin]# ls
node  npm  npx

node如同python3.6是一个解释器
npm 如同pip是一个包管理工具


3.配置node的环境变量PATH
PATH="/opt/python362/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin:/opt/tngx230/sbin:/opt/s19luffy/node-v8.6.0-linux-x64/bin"




准备前端代码配置方式如下：  vue 
1.
wget https://files.cnblogs.com/files/pyyu/07-luffy_project_01.zip
	
2.解压缩代码包
unzip 07-luffy_project_01.zip 

未更改过的vue代码，长这样
[root@nginx1 07-luffy_project_01]# ls
build  config  index.html  package.json(vue包信息的)  package-lock.json  README.md  src  static

3.补充，修改vue向后台发送数据的接口地址，必须修改..
3.补充，修改vue向后台发送数据的接口地址，必须修改..
3.补充，修改vue向后台发送数据的接口地址，必须修改..
3.补充，修改vue向后台发送数据的接口地址，必须修改..
3.补充，修改vue向后台发送数据的接口地址，必须修改..
修改如下文件信息/opt/s19luffy/07-luffy_project_01/src/restful/api.js 
批量修改接口地址
sed -i  "s/127.0.0.1:8000/19

8500/g"  api.js 

sed 处理文本信息的命令
-i  讲替换结果插入到文件中
"s/127.0.0.1/192.168.16.37/g"       s是替换模式  /你想替换的内容/替换之后的内容/   g  global 全局替换 
api.js  你要操作的文件 


4.安装vue的模块
npm install 

5.模块安装完毕后，进行编译打包，生成dist，静态文件夹
npm run build  

6.编译打包vue完成之后，会生成一个dist静态文件夹，里面存放了路飞学成首页所有的内容，交给nginx即可
/opt/s19luffy/07-luffy_project_01/dist




nginx配置方式如下：
1.支持vue的页面虚拟主机
 location / {
			root /opt/s19luffy/07-luffy_project_01/dist;
			index index.html;
			try_files $uri $uri/ /index.html;  #保证路飞首页，刷新不出现404
        }


2.反向代理的虚拟主机
server {
	listen 8500;
	server_name 192.168.16.37;
	location / {
	uwsgi_pass 192.168.16.37:9000;
	include uwsgi_params;
}
}



安装启动redis数据库
1.通过yum简洁的安装
yum install redis -y 
2.通过yum安装的软件
systemctl start redis 
3.可以查看redis数据库信息
redis-cli 登录数据库
4.查看购物车信息
127.0.0.1:6379> keys *
1) ":1:a89b01e6861a9946de5220556a46b9d1fe5dc21d"

代码的账号密码
alex 
alex3714 


#如果vue代码 npm instlall  npm run build 失败.

#目的就是为了生成一个dist静态文件夹 

解决办法：
1.去其他人可以正确打包的机器上，重新解压缩vue的源码，api.js提交地址 改成你自己的服务器地址，再打包一次，最终生成dist文件夹,发送给自己机器即可

2.修改npm的源，要修改淘宝的npm源

​	


mariadb就是centos7下的mysql ，开源，免费，用法和mysql一模一样

1.阿里云的yum仓库，和mariadb官方的yum仓库，其实就是两个不同的url，提供了2个不同的yum仓库
阿里云的yum仓库中，软件版本可能较低，不会实时的更新
mysqld   服务端
mysql 这个命令就是客户端命令
navicat  mysql的图形化工具
pymysql  也是一个客户端

   

如果选择的是阿里云的yum仓库，安装命令如下
yum install mariadb-server    mariadb  -y  



如果你要选择最新的mariadb软件，请配置官方的yum源 
安装命令如下
yum install MariaDB-server MariaDB-client  


2.安装好mysql之后，就可以启动使用了，注意要先初始化数据库
执行这个初始化命令
mysql_secure_installation

3.学习mysql的授权命令
允许mysql服务端，可以远程连接的命令
grant all privileges on *.* to root@'%' identified by 'centos'; #允许root用户在任意的主机地址都可以登录mysql服务端
flush privileges; #立即刷新权限表  

4.客户端远程登录服务端的命令

mysql -uroot -p   -h 主机的ip


5.mysql的备份与恢复
备份命令
mysqldump -u root -p --all-databases > /opt/alldb.sql 
--all-databases  导出所有的数据库数据表

#指定数据库导出
mysqldump -u root -p -B ob_crm > /opt/crm.sql  

#指定单独的数据库中的一张表导出




恢复数据，导入的命令：
方式1：
mysql -uroot -p  < /opt/alldb.sql  
方式2：
登录mysql之后，source读取db文件
MariaDB [test]> source /opt/alldb.sql;


mysql主从同步的配置
1.环境准备，准备2台linux，分别安装好mariadb数据库

2.先从master主库开始配置
	1.停止数据库服务
	systemctl stop mariadb
2.修改mysql配置文件，支持binlog日志
server-id=1
log-bin=s19-bin-log 

3.重启mysql数据库，让binlog日志文件生效
systemctl restart mariadb 

4.创建一个用户，用于主从同步
create user 's19'@'%' identified by 'centos';

5.给这个账号授予复制的权限
grant replication slave on *.* to 's19'@'%';

6.导出当前的数据库，发送给从库，保证数据一致

mysqldump -uroot -p --all-databases > /opt/mydb.sql  




从库配置如下：
1.修改mysql配置文件，加上身份id ,vim  /etc/my.cnf 

[mysqld]
server-id=10
read-only=true

2.重启从库的mysql
systemctl restart mariadb 

3.从库导入主库的数据，保证起点一致性

4.配置主从同步的关键参数

mysql > change master to master_host='192.168.16.37',
master_user='s19',
master_password='centos',
master_log_file='s19-bin-log.000001',
master_log_pos=583;


5.开启主从同步
start slave;

6.查看主从同步的状态

show slave status\G

7.去主库写入数据，查看从库的实时数据同步










mysql忘记密码怎么办：
1.修改配置文件，添加跳过授权表的参数
vim /etc/my.cnf 
添加参数
[mysqld]
skip-grant-tables



2.用命令修改mysql的root密码
use mysql;
#更新root的密码
update user set authentication_string=password("centos") where host="localhost" and user="root";
#刷新权限表
flush privileges; 



3.修改mysql的中文支持
查看数据库的编码
MariaDB [(none)]> \s

show create database douyin;  #查看如何创建的数据库
show create table  s19;  #查看如何创建的表
desc s19;  #查看表结构

4.修改mysql的服务端编码，让它支持中文
修改mysql的配置文件
vim /etc/my.cnf 
修改为如下配置

[mysqld]
character-set-server=utf8
collation-server=utf8_general_ci
log-error=/var/log/mysqld.log

[client]
default-character-set=utf8
[mysql]
default-character-set=utf8

5.修改数据库，表的字符集方式
#修改数据库的编码
alter database douyin default character set="utf8";
#修改数据表的编码
alter table xiu convert to character set "utf8";
#刷新权限
flush privileges; 









 











​	





 