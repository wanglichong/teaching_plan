# day66

### 1.安排

1. 业务
2. 权限的组件

### 2.CRM

​	customer  relationship management 

### 3.谁去使用这套系统？

   	销售 班主任 项目经理 财务

### 4.需求分析

 1. 注册

 2. 登录

 3. 销售：

     1. 客户信息管理

        展示   添加  编辑  客户的信息 

    2. 跟进信息管理

       展示   添加  编辑  跟进记录 

    3. 报名信息的管理

       展示   添加  编辑  报名信息

    4. 缴费记录的管理

       展示   添加  编辑  缴费信息

	4. 班主任：

    	1. 班级信息管理

        展示   添加  编辑  班级信息

    	2. 课程记录管理

        展示   添加  编辑  课程记录

    3. 学习（上课）记录管理

       展示   添加  编辑  学习（上课）记录

### 5.表结构的设计

1. 用户表
2. 客户表
3. 跟进记录
4. 报名记录表

5. 缴费记录表
6. 校区表

7. 班级表

8. 课程记录表

   1   s19    day66     上课内容   作业

   2   s19    day67     上课内容   作业

   3   s20    day30     上课内容   作业

9. 学习记录表

   课程_id    学生_  id     出勤    作业成绩

   1		1            正常         100

   1                 2            迟到          90 

   2                1

   2                2

### 6.实现功能

登录注册

下载 django-multiselectfield

语言环境配置

```
LANGUAGE_CODE = 'zh-Hans'
```

### 7.modelform

```Python
from django import forms
from django.core.exceptions import ValidationError

class RegForm(forms.ModelForm):
   password = forms.CharField(min_length=6,widget=forms.PasswordInput(attrs={'placeholder': '您的密码'}))
   re_password = forms.CharField(min_length=6,widget=forms.PasswordInput(attrs={'placeholder': '确认您的密码'}))

   class Meta:
      model = models.UserProfile
      fields = '__all__'  # ['字段名']
      exclude = ['is_active']

      widgets = {
         'username': forms.TextInput(attrs={'placeholder': '您的用户名'}),
         'password': forms.PasswordInput(attrs={'placeholder': 'xxxxx'}),
         'name': forms.TextInput(attrs={'placeholder': '您的真实姓名'}),
         'mobile': forms.TextInput(attrs={'placeholder': '您的手机号'}),
      }

      error_messages = {
         'username': {'invalid': '请输入正确的邮箱地址'}

      }

   def clean(self):
      # 获取到两次密码
      password = self.cleaned_data.get('password')
      re_password = self.cleaned_data.get('re_password')
      if password == re_password:
         # 加密后返回
         md5 = hashlib.md5()
         md5.update(password.encode('utf-8'))
         password = md5.hexdigest()
         self.cleaned_data['password'] = password
         # 返回所有数据
         return self.cleaned_data
      # 抛出异常
      self.add_error('re_password','两次密码不一致!!')
      raise ValidationError('两次密码不一致')
```



## git  

做版本控制

### 命令

git  init    初始化仓库

git status   查看文件状态

git add 文件名   添加文件

git add .     添加当前目录所有的文件

git commit -m '提示信息'

配置

git config --global user.email "you@example.com"

git config --global user.name "Your Name"

### 流程

1. 创建学号对应的文件夹（E:\s19\18122619007）

2. 进入文件夹内，打开git窗口   

   1. git init   

   2. 创建项目  E:\s19\18122619007\ob_crm

   3. git add . 

   4. 配置

      git config --global user.email "you@example.com"

      git config --global user.name "Your Name"

   5. 一个完成之后 提交版本

      git commit -m '实现的功能

3. 注册码云 https://gitee.com/

   点击链接加入组织

   进入组织

   ![1555406223092](assets/1555406223092.png)



​	新建远程仓库

​	![1555406248236](assets/1555406248236.png)

![1555405542350](assets/1555405542350.png)

​	执行命令

 1. 添加远程仓库的地址

     git remote add origin https://gitee.com/old_boy_python_stack_19/xiao.git

2. 把本地的代码推送到远程仓库

   git push  origin master



### 作业

1. 创建crm项目
2. 实现登录和注册的功能
3. 使用git管理项目
4. 把代码推送到码云