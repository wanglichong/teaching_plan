# day72

## 内容回顾

权限控制

1.表结构设计

权限表

 - id
 - url   权限   URL的正则表达式    没有带^$
 - title  标题   

角色表

- id
- name   名称 
- permissions    多对多  关联权限

用户表

- id
- name   用户名
- pwd     密码
- roles    多对多  关联角色

角色和权限的关系表

- id
- role_id     外键 关联角色
- permission_id  外键 关联权限

用户和角色关系表

- id
- user_id  外键  关联到用户
- role_id  外键  关联到角色

2.流程

 	1. 登录
     - 中间件   process_request
       - 白名单
     - 登录成功
       - 查询当前用户所有的权限
       - 把权限信息和登录状态保存到session中
	2. 中间件
    - 获取当前访问的地址
    - 白名单
    - 登录状态的校验
      - 没有登录返回登录页面
    - 免认证的地址的校验
    - 获取当前用户的权限信息
    - 权限的校验
      - 循环权限列表
      - 正则匹配  re.match （  ^ {url} $    ）
        - 能匹配成功  return  None
        - 所有都没匹配成功    return  HttpRsponse(‘没有权限’)

今日内容：

1.git 

​	git init 

​	配置

​	git config  -- global   eamil  ''

​	git config  -- global   user  ''

​	git  add  文件 

​	git  add .

​	git status   查看状态 

​	git commit -m '描述信息'



​	git remote add origin  '地址'

​	git push  origin  master

​	git  pull  origin  master

2.分支

git branch   查看

git branch   dev   新建dev分支

git branch  -d  dev   删除dev分支

git checkout  dev   切换到dev分支

git merge   dev     从dev合并到当前分支（有冲突 手动解决）



单人开发 

新建一个dev分支   开发新功能

开发完成 合并到master分支

有bug  新建debug分组    

到debug分支上修改bug 

修改完成 合并到master分支  并且删除debug分支



多人协作开发

远程仓库中每人有自己的分支

在本地开发  开发完成后提交到自己的分支

提交pull requests 合并到dev分支上





1.动态生成一级菜单

2.动态生成二级菜单

财务管理   ——  一级菜单

​	缴费列表   —— 二级菜单

客户管理

​	客户列表





