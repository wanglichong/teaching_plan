### 流程

agent —— 脚本放在每台服务器上（场景：机器多）

1. 在本地使用subprocess模块实行命令获取信息
2. 通过requests模块发送到api

ssh —— 脚本放在一台中控机上（场景：机器少、不安装其他的软件）

1. 获取未采集资产信息的主机列表
2. 循环主机列表 拿到主机名 通过ssh（paramiko模块）远程连接上服务器执行命令拿到结果
3. 通过requests模块发送到api

slat ——  脚本放在一台中控机上（场景：机器少、安装自动化管理工具）

1. 获取未采集资产信息的主机列表
2. 循环主机列表，拿到主机名，通过slat（salt模块）远程连接上服务器执行命令拿到结果
3. 通过requests模块发送到api

master   minion

### 目录结构

bin      ——  可执行文件

src       ——  逻辑代码

config   —— 配置

lib      ——    公共库

db     ——  数据库

log     ——  日志

README——  说明

### 配置相关

配置分类：

​	自定义的配置

​		MODE = 'ssh'
​		USER = 'xxx'


​       默认的配置

​		EMAIL = 'SS@qq.com'

```
import importlib
module = importlib.import_module('config.settings')   # 通过路径字符获取模块

import os
os.environ['USER_SETTING'] = 'config.settings'   # 给环境变量中设置键值对
```

高级配置

![1557996102367](../../%E7%AC%94%E8%AE%B0%20-%20markdown/day87/assets/1557996102367.png)



![1557996121692](../../%E7%AC%94%E8%AE%B0%20-%20markdown/day87/assets/1557996121692.png)

### 类的约束

1.抽象类抽象方法

```
import abc


class BaseHandler(metaclass=abc.ABCMeta):
   @abc.abstractmethod
   def handler(self):
      pass
      
 class AgentHandler(BaseHandler):
 	def handler(self):
      pass
```

2. 抛出异常

```
class BaseHandler():

   def handler(self):
      raise NotImplementedError('handler() must be Implemented')
```

### 开放封闭原则

开放： 配置

封闭：源码

### 根据配置选择模式（agent、ssh、salt）

![1557996524448](assets/1557996524448.png)

### 根据配置获取相应的插件的信息

```Python
def get_server_info(handler, hostname=None):
   # 根据配置获取相应的硬件的信息
   info = {}
   for name, path in settings.PLUGINS_DICT.items(): # 配置
      cls = get_class(path)
      obj = cls() # Disk Memory NIC 类
      info[name] = obj.process(handler, hostname) # 执行process方法获取结果赋值到info中
   return info
```

### 采集资产的插件根据模式自动选择执行命令的方式

![1557997043813](assets/1557997043813.png)