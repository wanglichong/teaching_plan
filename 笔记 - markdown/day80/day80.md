# day80

## 内容回顾

今日内容

Nginx学习

nginx  web server 
1.静态网站 ，静态虚拟主机的作用
就是不变化的网页，静态的html，css js等页面，以及jpg gif mp4等静态资源
有一些草根站长，放一些小说，图片等等...


2.动态网站
crm  
指的是，可以与数据库打交道，数据交互的网页，网页内容根据数据库的数据变化
登录功能，有注册功能的...
并且有编程语言支持的

3.nginx的并发性非常强悍
轻松支持10万+并发连接数

tenginx 

4.常见web服务器有哪些
windows下 IIS服务器
linux下的web服务器  nginx  apache  lighthttp

5.
web服务器  
	nginx 这样的软件
	web服务器它自己不支持编程，仅仅是页面返回,nginx + lua 

web框架的概念
	django  flask  tornado这样的 web逻辑框架 
	支持程序员自己写代码，进行逻辑处理 


6.nginx是web服务器、反向代理服务器、邮件代理服务器，负载均衡等等，支持高并发的一款web服务器

7.nginx安装方式
	1.yum
	2.rpb手动安装
	3.源代码编译安装，选择这个，支持自定义的第三方功能扩展，比如自定义安装路径，支持https，支持gzip资源压缩


8.注意点，删除之前yum安装的nginx  
yum remove nginx -y #卸载yum安装的nginx

选择源码编译安装
注意，解决源码编译安装nginx所需的软件依赖，重要的事说三遍
注意，解决源码编译安装nginx所需的软件依赖，重要的事说三遍
注意，解决源码编译安装nginx所需的软件依赖，重要的事说三遍

执行它 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
执行它 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
yum install gcc patch libffi-devel python-devel  zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel openssl openssl-devel -y

1.下载淘宝nginx的源代码包
wget http://tengine.taobao.org/download/tengine-2.3.0.tar.gz

2.解压缩源码包
tar -zxvf tengine-2.3.0.tar.gz 

3.进入源码目录开始编译安装

	cd  tengine-2.3.0
	编译安装三部曲
	./configure --prefix=/opt/tngx230/
	make && make install 

4.配置淘宝nginx的环境变量，写入/etc/profile
如下内容
PATH="/opt/python362/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin:/opt/tngx230/sbin"
读取/etc/profile 

source /etc/profile 

5.启动nginx 
直接输入nginx 指令，默认代表启动，不得再执行第二次
nginx  
nginx -s reload  #平滑重启nginx，不重启nginx，仅仅是重新读取nginx配置文件
nginx -s stop  #停止nginx进程
nginx -t  #检测nginx.conf的语法


6.学习nginx的目录配置文件信息
[root@localhost tngx230]# pwd
/opt/tngx230
[root@localhost tngx230]# ls
client_body_temp  conf  fastcgi_temp  html  logs  proxy_temp  sbin  scgi_temp  uwsgi_temp
#解释目录
conf  存放nginx配置文件的
html  存放前端文件目录  ,首页文件就在这里
logs  存放nginx运行日志，错误日志的
sbin  存放nginx执行脚本的


7.部署一个自己的web站点
修改index.html文件内容，即可看到新的页面内容

/opt/tngx230/html就是 nginx的网页根目录，放入任意的文件内容都可以被解析到
例如下载一张图片
cd /opt/tngx230/html
wget http://img.doutula.com/production/uploads/image/2019/05/07/20190507183856_lfUSFG.jpg
mv 20190507183856_lfUSFG.jpg xiaomaomi.jpg
此时可以访问  192.168.16.37/xiaomaomi.jpg 

8.nginx配置文件学习

	#全局变量写在最外层
	worker_processes  4;
	
	events {
		worker_connections  1024;
	}
	
	#定义nginx核心功能的参数
	http {
		include       mime.types;
		default_type  application/octet-stream;
		#定义nginx访问日志格式的
		log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
						  '$status $body_bytes_sent "$http_referer" '
						  '"$http_user_agent" "$http_x_forwarded_for"';
		#访问日志的功能	  
		access_log  logs/access.log  main;
		#对图片等静态资源压缩，提升传输效率
		gzip  on;
		#nginx的主页面功能都是server参数提供的
		#server被称作是虚拟主机
		server {
			#nginx监听的端口
			listen       80;
			#填写服务器的域名，或者ip，或者localhost
			server_name  localhost;
			#路径匹配，当你的请求来自于 192.168.16.37/ 这样的url的时候，就进入以下的location路径匹配
			location / {
				#root参数，定义网页根目录的,nginx目录下的html,可以自由更改
				root   html;
				#指定首页文件的名字的
				index  index.html;
			}
			#错误页面 
			#当请求返回404错误码的时候，就给用户看到一个页面
			#这个404.html放在网页根目录下
			error_page    404              /404.html;
			# redirect server error pages to the static page /50x.html
			#
			error_page   500 502 503 504  /50x.html;
			location = /50x.html {
				root   html;
			}
		}
	}

9.nginx的错误页面优化
就是打开nginx.conf里面的 如下参数
error_page    404              /404.html;

10.nginx的访问日志功能
打开如下功能参数 nginx.conf 


log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
				  '$status $body_bytes_sent "$http_referer" '
				  '"$http_user_agent" "$http_x_forwarded_for"';
access_log  logs/access.log  main;

11.nginx限制访问功能

就是在nginx.conf中 找到location 参数，写入如下信息

location / {
				deny 你想禁止的ip地址;
				deny  你也可以禁止一整个ip地址段;
				deny  192.168.16.0/24;   /24子网掩码地址  255.255.255.0
				#root参数，定义网页根目录的,nginx目录下的html,可以自由更改
				root   html;
				#指定首页文件的名字的
				index  index.html;
			}


12.nginx的状态信息功能，检测当前有多少个链接数

	在nginx.conf下打开一个参数即可
	#当你的请求 来自于 192.168.16.37/status ,就进入如下的代码块
	location /status {
	#开启nginx状态功能
				 stub_status on;
	}       
	
	使用linux的压测命令 ，给nginx发送大量的请求
	ab命令
	安装方式
	yum -y install httpd-tools 
	
	-n requests #执行的请求数，即一共发起多少请求。
	
	-c concurrency #请求并发数。
	
	-k #启用HTTP KeepAlive功能，即在一个HTTP会话中执行多个请求。
	url必须格式如下
	ab -kc 1000 -n 100000 http://192.168.16.37/   

13.多虚拟主机，（在一台服务器上，运行读个网站页面）
	基于多域名的虚拟主机实现，其实就是读个server标签

	环境准备，1个linux服务器，192.168.16.37
	2个域名,我们是写入hosts文件，强制解析的假域名
	www.s19dnf.com
	www.s19riju.com 
	
	安装好nginx软件，
	配置方式如下
		1.服务器准备好了，nginx也安装好了
		2.在windows中写入hosts假的域名
			找到如下文件，编辑写入域名对应关系
			C:\Windows\System32\drivers\etc\hosts 
			
			192.168.16.37  www.s19dnf.com 
			192.168.16.37  www.s19riju.com 
		3.修给nginx.conf的配置，实现多虚拟主机，多个server{}标签是同级的关系
			#第一个虚拟主机，dnf网址
			server {
				listen       80;
				server_name  www.s19dnf.com;
				location / {
				root   /opt/dnf/; 
				index  index.html index.htm;
				}
			}
			#第二个虚拟主机，日剧的页面
			server {
				listen 80;
				server_name www.s19riju.com;
				location / {
				root  /opt/riju/;
				index  index.html;
				}
			}
	
		4.准备2个网站的文件夹，以及页面文件
		/opt/dnf/index.html 
		/opt/riju/index.html 
		
		5.重启nginx
		nginx -s reload 
		
		6.访问2个虚拟主机的页面，在windows浏览器中访问
		www.s19dnf.com 
		www.s19riju.com 

14.nginx的反向代理功能
1.见过生活中的代理

客户端（请求资源）  ->   代理（转发资源） ->  服务端（提供资源）
					

房屋的中介，就是个代理
我们老百姓就是客户端，中介是代理，房东就是服务端提供资源的

买票，找到黄牛，黄牛提供12306的车票

代购 
我们买东西， 找到代购，代购就可以提供我们想要的资源

客户端和 nginx 和 django 的关系

配置nginx实现反向代理的步骤：
环境准备2台机器，（如果你的机器性能太低，和你的同桌合作）

192.168.16.37   充当资源服务器，提供一个页面 
	1.提供了一个dnf的页面

192.168.16.140 	充当代理服务器的角色 (也是安装nginx，转发请求，反向代理的功能也是nginx提供的)
	1.安装好nginx
	2.修改如下配置 nginx.conf 
	
	    #当你的请求来自于192.168.16.140/ 这样的url时，就进入如下location路径匹配
	    location / {
	    #当请求进来这个路径匹配，这个nginx代理角色，直接转发给资源服务器
	    proxy_pass 192.168.16.37;
	    }
	3.测试访问反向代理的页面
		访问 192.168.16.140即可看到192.168.16.37的页面了




练习：
展示效果给工头看
1.完成nginx搭建
2.nginx web服务器展示，显示一个图片文件
3.nginx 404错误页面优化
4.nginx 访问日志功能
5.nginx 多虚拟主机功能(基于域名的 一个dnf网址，一个日剧网址)
6.再安装一个linux虚拟机，我们需要做反向代理的实验

#预习
7.机器配置足够的情况下，再安装一台，准备3台linux虚拟机，用于做nginx负载均衡的实验


​		
​		
更改主机名的命令

hostnamectl set-hostname 你想要的主机名   #重新登录回话后生效













 











​	





 