# day80

## 内容回顾

s19day81笔记

昨日内容回顾
1.编译安装tengine服务
	编译三部曲
	1.解决软件编译安装所需的依赖  yum instal ****
	2.下载软件的源代码包，并且解压缩出源码包内容
	3.进入源代码包目录，执行./configure --prefix=安装绝对路径,释放编译文件，makefile
	4.调用linux的编译器  make && make install ,开始安装软件
2.nginx的核心工作目录
conf  存放nginx.conf的目录，功能配置都在这
logs  存放err.log  access.log 
html   网页根目录（默认的配置），存放 index.html  以及404.html    nginx.conf location中  root参数定义的目录

3.nginx提供web服务器页面的功能参数，就是 server{}虚拟主机标签
#server虚拟主机，nginx会自上而下的加载，因为可能有多个虚拟主机
#虚拟主机定义了网站的端口，域名，网页内容存放路径
  server {
        listen       80;
        server_name  www.s19dnf.com;
        location / {
            root   /opt/dnf/;
            index  index.html index.htm;
        }
    }
#nginx支持多虚拟主机，也就是通过多个server标签实现的
#第二个虚拟主机，日剧的页面
server {
	listen 80;
	server_name www.s19riju.com;
	location / {
	root  /opt/riju/;
	index  index.html;
}
}


#nginx会根据，用户访问的url域名，进行server_name域名匹配，然后选择返回不同的 虚拟主机内容

4.还有剩余的功能配置
404页面
在虚拟主机中server{}标签中定义error_log参数
error_page  404              /404.html;

访问日志功能，放在http{}标签中，代表全局作用域，对所有虚拟主机生效，写在单独的虚拟主机中，就是局部变量

log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                 '$status $body_bytes_sent "$http_referer" '
                 '"$http_user_agent" "$http_x_forwarded_for"';

access_log  logs/access.log  main;


5.反向代理功能参数
在虚拟主机中，找到location标签，然后写入proxy_pass 你先代理的服务器的ip地址;





nginx实现负载均衡的配置，动静分离的效果

1.环境准备，准备三台机器
192.168.16.37  资源服务器
192.168.16.140  资源服务器 
192.168.16.241  充当代理服务器作用，以及负载均衡的作用 

 



2.分别配置三台机器

192.168.16.37  资源服务器  提供dnf的页面 

192.168.16.140  资源服务器 ,提供小猫咪的页面 
(讲道理，应该是和192.168.16.37一样的代码配置，一样的页面)

192.168.16.241  负载均衡的配置
	nginx.conf修改为如下的配置
	1.添加负载均衡池，写入web服务器的地址
	 upstream  mydjango  {
		#负载均衡的方式，默认是轮训，1s一次
		#还有其他的负载均衡的规则
		server  192.168.16.37 ;  
		server 192.168.16.140 ;
}

	负载均衡的规则
	调度算法   　　 概述
	轮询    　　　　按时间顺序逐一分配到不同的后端服务器(默认)
	weight  　　   加权轮询,weight值越大,分配到的访问几率越高，最常用的方式，
	ip_hash   　　 每个请求按访问IP的hash结果分配,这样来自同一IP的固定访问一个后端服务器
	url_hash   　  按照访问URL的hash结果来分配请求,是每个URL定向到同一个后端服务器
	least_conn    最少链接数,那个机器链接数少就分发
	
	1.轮询(不做配置，默认轮询)
	
	2.weight权重(优先级)
	
	3.ip_hash配置，根据客户端ip哈希分配，不能和weight一起用

2.负载均衡的配置方式第二步骤
#负载均衡器，接收到请求后，转发给负载均衡的地址池
location / {
        proxy_pass http://mydjango;
        }

3.动静分离的配置
	1.配置动态请求的服务器
	192.168.16.140  充当静态服务器，返回小猫咪的页面，以及一些图片资源
	需要安装nginx即可
	nginx.conf配置文件如下
	#当所有请求来自于 192.168.16.140/时，就进入如下路径匹配，返回小猫咪页面
	location / {
            root   html;
            index  index.html index.htm;
        }
	#当请求是192.168.16.140/**.jpg 就进入如下location匹配，返回/opt/images/目录下的内容 
         location ~* .*\.(png|jpg|gif)$ {
                root /opt/images/;
        }


	2.配置静态请求的服务器
	192.168.16.37 运行django页面 
	安装nginx，以及django动态页面
	
		1.先后台运行django页面
		python3 nginx_django/manage.py runserver 0.0.0.0:8000 & 
	
		2.修改nginx的配置，可以转发，动静的请求
						upstream mydjango {
							server 192.168.16.37:8000;
						}
	
					upstream mystatic {
						server 192.168.16.140;
					}
					
					server {
						listen       80;
						server_name  192.168.16.37;
						#当外部请求，是192.168.16.37:80时候，就进入如下location，返回django页面，反向
				代理
						location / {
								proxy_pass http://mydjango;
						}
						#当外部请求时，192.168.16.37:80/**.jpg，就转发给那台静态资源服务器，去处理
						 location ~ .*\.(png|jpg|gif)$ {
								proxy_pass http://mystatic;
					}
				}


​	
​	
​	
	3.在负载均衡器上，配置请求转发
	192.168.16.241  配置负载均衡，动静分离
		1.定义一个upstrem地址池，进行请求分发定义
			 upstream  myserver  {
						server  192.168.16.37;
						server 192.168.16.140;
			}
		
		2.通过location进行请求分发的操作
		  location / {
				proxy_pass http://myserver;
			}

ob_crm项目部署：

django自带了socket服务端吗？wsgiref这个是python自带的socket模块，django默认用的是wsgiref的单机socket模块
Python manage.py runserver 这是调试命令，测试django是否有问题的
最终上线 是 uwsgi + django 的方式，
	
为什么要用nginx结合uwsgi
1.nginx支持静态文件处理性能更好,django uwsgi,默认不支持静态文件解析 
2.nginx的负载均衡特性，让网站并发性更高
3.并且反向代理特性，用户访问 80，即可访问到8000的应用
4.uwsgi支持多进程的方式，启动django，性能更高
5.nginx转发请求给uwsgi ，应该用 uwsgI_pass  ，实现了uwsgi协议的请求转发


如果出现错误，  
python app  application  not found  
就是你的uwsgi 没找到 wsgi.py 这个文件对象 application = get_wsgi_application()



配置步骤:
1.nginx + uwsgi + 虚拟环境 + mysql  + supervisor
环境准备，准备一台linux
192.168.16.37 ，

第一步，先准备后端代码 ob_crm 
第二步：安装python3解释器，以及虚拟环境工具virtualenvwrapper 
第三部，学习uwsgi命令，通过uwsgi启动ob_crm 
	1.激活虚拟环境在虚拟环境下，安装所需的模块
		安装如下内容即可
		(ob_crm) [root@nginx1 ob_crm]# cat requirements.txt 
		Django==1.11.20
		django-multiselectfield==0.1.8
		PyMySQL==0.9.3
		pytz==2019.1
	2.安装这个文件
	pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt


	3.安装uwsgi命令
	pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple  uwsgi


	4.学习uwsgi启动ob_crm的命令
	uwsgi --http  :8088 --module mysite.wsgi --py-autoreload=1 
		--http  指定用http协议 
		:8088  指定ob_crm启动的端口
		--module   指定django的wsgi.py的文件地址
			指定你的django项目第二层的目录名，下面的wsgi.py
		--py-autoreload=1     开启uwsgi的热加载功能
	
	所以咱们用的命令应该是如下：
		1.必须进入项目目录 
			cd /opt/teaching_plan/ob_crm
		2.使用命令启动ob_crm （uwsgi不解析django的静态文件）
			uwsgi --http  :8088 --module ob_crm.wsgi
		3.让你的项目支持热加载
			uwsgi --http  :8088 --module ob_crm.wsgi  --py-autoreload=1


​		
	由于uwsgi的启动参数过多，我们互选择配置文件的方式，启动项目
	uwsgi.ini  这个文件的名字，可以手动创建的
	
	1.创建uwsgi.ini文件，写入如下参数
	touch uwsgi.ini 
	写入如下内容，根据你自己的项目目录路径来更改
		[uwsgi]
		# the base directory (full path)
		#填写你项目的绝对路径，第一层
		chdir           = /opt/teaching_plan/ob_crm/
		#找到django的那个wsgi.py文件
		#根据上面一条参数，的相对路径来写
		module          = ob_crm.wsgi
		#虚拟环境的绝对路径
		home            = /root/Envs/ob_crm
		master          = true
		#根据你的cpu核数来定义这个参数，启动多少个进程
		processes       = 4
		# the socket (use the full path to be safe
		#指定你的django启动在什么地址上，并且是什么协议
		#如果你用了nginx，进行反向代理，请用socket参数
		#如果你用了nginx，进行反向代理，请用socket参数
		#如果你用了nginx，进行反向代理，请用socket参数
		socket          = 0.0.0.0:8000
		#如果你没用nginx反向代理，想要直接访问django用这个参数
		#如果你没用nginx反向代理，想要直接访问django用这个参数
		#http = 0.0.0.0:8000
		vacuum          = true
		py-autoreload    =   1
		~                            
	2.通过配置文件启动项目
	uwsgi --ini  uwsgi.ini 
	
	3.收集django的所有静态文件，统一管理，丢给nginx去解析
		1.修改django的settings.py，写入如下参数
			STATIC_ROOT='/opt/s19static'
			STATIC_URL = '/static/'
			STATICFILES_DIRS = [
					os.path.join(BASE_DIR, 'static')
			]
		2.执行命令，收集静态文件
			python3 manage.py collectstatic


​		
​	
nginx配置来了！！！
1.nginx的反向代理功能
2.nginx解析静态文件的功能

 location / {
        uwsgi_pass 127.0.0.1:8000;
        include uwsgi_params;
        }

location /static {
        alias /opt/s19static;
}




进程管理工具supervisor的使用
#supervisor其实就是在帮咱们去执行命令 

退出虚拟环境，在物理环境下安装
退出虚拟环境，在物理环境下安装
退出虚拟环境，在物理环境下安装
1.安装supervisor,通过pip直接安装
pip3 install supervisor 

2.生成supervisor的配置文件
echo_supervisord_conf >  /etc/supervisor.conf

3.修改配置，写入你管理ob_crm的命令参数
vim /etc/supervisor.conf  #直接进入最底行，写任务


[program:s19_ob_crm]
command=/root/Envs/ob_crm/bin/uwsgi --ini /opt/teaching_plan/ob_crm/uwsgi.ini
stopasgroup=true    
killasgroup=true    


4.通过命令，启动supervisor，同时启动ob_crm 
[root@nginx1 conf]# supervisor
supervisorctl  这是管理命令
supervisord  	这个是服务端命令 

#启动服务端的命令
supervisord -c /etc/supervisor.conf 

#通过客户端命令，管理ob_crm
[root@nginx1 conf]# supervisorctl -c /etc/supervisor.conf 
s19_ob_crm                       RUNNING   pid 10483, uptime 0:00:36
supervisor> 

#停止任务
supervisor> stop s19_ob_crm 
s19_ob_crm: stopped

#查看任务状态
supervisor> status 
s19_ob_crm                       STOPPED   May 08 12:23 PM


#停止所有的任务
supervisor> stop all 
s19_ob_crm                       STOPPED   May 08 12:23 PM

#启动所有任务
supervisor> start all 
s19_ob_crm                       RUNNING   May 08 12:23 PM











 











​	





 