# day56

## 内容回顾

1. django处理请求的流程：

   1. 浏览器地址栏出入网址，回车发送get请求。
   2. wsgi模块接受消息 。
   3. 获取到url地址，通过对应关系找函数，执行函数。
   4. 函数返回响应，wsgi模块封装响应返回给浏览器。

2. 发请求的途径

   1. 地址栏输入地址  ——》 get请求
   2. a标签      ——》 get请求
   3. form表单  ——》  get/post 

3. 函数

   1. request 

      request.method   请求方式  GET  POST

      request.GET       URL上携带的参数

      request.POST     POST请求的数据

   2. 返回值
      HttpResponse('字符串')    —— 》 字符串

      render(requset,'模板的文件名'，{})     ——》 一个页面

      redirect（’要跳转的路径‘）    ——》 重定向     Location：’要跳转的路径’

4. 外键

表示多对一的关系

publisher     book     

查询

```python
for book_obj in book_list:
    book_obj.pk  # 主键
    book_obj.name  # 书名
    book_obj.pub   # 书籍关联的出版社对象 
    book_obj.pub_id   # 书籍关联的出版社对象ID

```

增加

```python
 models.Book.objects.create(name='xxxx',pub=pub_obj)
 models.Book.objects.create(name='xxxx',pub_id=pub_obj.pk)
```

删除

```python
models.Book.objects.get(pk=1).delete()
models.Book.objects.filter(pk=1).delete()
```

编辑

```python
obj = models.Book.objects.get(pk=1)
obj.name = 'xxxxx'
obj.pub  = pub_obj 
obj.pub_id  = pub_obj.pk 
obj.save()
```

5. 模板

   for

   ​	{% for i in book_list %}

   ​		{{  forloop.counter  }}

   ​		{{  i  }}

   ​		{{  i.name }}

   ​	{% endfor %}

   if

   ​	{%  if   条件    %}

   ​			x1  

   ​	{%  elif   条件1 %}

   ​			x2

   ​	{% else %}

   ​			x3 

   ​	{% endif  %}

## 今日内容

多对多关系

![1554085342073](.\assets\1554085342073.png)



#### 展示

设计url和函数的对应关系

```
url(r'^author_list/', views.author_list),
```

写函数

```Python
# 展示作者
def author_list(request):
   # 查询所有的作者
   authors = models.Author.objects.all()

   return render(request, 'author_list.html', {'authors': authors})
```

html

```html
<table border="1">
    <thead>
    <tr>
        <th>序号</th>
        <th>ID</th>
        <th>作者姓名</th>
        <th>代表作</th>
    </tr>
    </thead>
    <tbody>
    {% for author in authors %}
        <tr>
            <td>{{ forloop.counter }}</td>
            <td>{{ author.pk }}</td>
            <td>{{ author.name }}</td>
            <td>
                    {% for book in author.books.all %}
                        《 {{ book.name }} 》
                    {% endfor %}
            </td>
        </tr>
    {% endfor %}
    </tbody>
</table>
```



```Python
authors = models.Author.objects.all()　　＃　对象列表
for author in authors:
   print(author)    #　　作者对象
   print(author.pk)
   print(author.name)
   print(author.books) 　　　 # 关系管理对象
   print(author.books.all())  # 所有的书籍
```

新增：

提交数据是多个值：

```
request.POST.getlist('books_id')  —— 》   []
```

```Python
#  插入数据库
author_obj = models.Author.objects.create(name=name)  # 创建作者对象
author_obj.books.set(books_id)				# 设置当前作者和书籍的多对多的关系  每次都是重新设置
```

创建多对多关系的三种方式：

1. django创建第三张表

   ```Python
   class Author(models.Model):
      name = models.CharField(max_length=32)
      books = models.ManyToManyField('Book')  # 表示多对多的关系  生成第三张表
   ```

2. 自己创建第三张表

   ```Python
   class Author(models.Model):
      name = models.CharField(max_length=32)
   
   
   class Author_Book(models.Model):
      author = models.ForeignKey('Author')
      book = models.ForeignKey('Book')
      time = models.CharField(max_length=32)
   ```

3. django + 自建表

   ```Python 
   class Author(models.Model):
      name = models.CharField(max_length=32)
   
      books = models.ManyToManyField('Book',through='Author_Book')  # 表示多对多的关系 
   
   
   class Author_Book(models.Model):
      author = models.ForeignKey('Author')
      book = models.ForeignKey('Book')
      time = models.CharField(max_length=32)
   ```