### 公司愿景

开发自动化运维平台

- 自动装机

- 配置管理 

- 监控

- 堡垒机

现状：使用excel表管理资产

CMDB （资产管理） 

实现：

subprocess.getoutput('ls')

shell命令

### agent

 - agent  （放在每台服务器上，定时任务）
 - api (django程序)

### SSH类

pip install paramiko

```python
import paramiko

# 创建SSH对象
ssh = paramiko.SSHClient()

# 允许连接不在know_hosts文件中的主机
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# 连接服务器
ssh.connect(hostname='192.168.16.140', port=22, username='root', password='centos')
# 执行命令
stdin, stdout, stderr = ssh.exec_command('ifconfig')
# 获取命令结果
result = stdout.read()
print(result)
# 关闭连接
ssh.close()
```

### salt

master

​	yum install salt-master -y 

​	配置：

​	/etc/salt/master  

​	interface: 0.0.0.0

​	启动： service salt-master start 

​	授权：

​		salt-key -L  查看

​		salt-key -a   id  接受某一个 

​		salt-key -A   接受所有

​		salt-key -d  id   删除某一个

minion

​	yum install salt-minion -y 

​	配置：/etc/salt/minion

​	master: 192.168.16.37

​	启动： service salt-minion start 

​	

命令行：

​	salt '*'  cmd.run 'ifconfig'

python接口：

```
import salt.client
local = salt.client.LocalClient()
result = local.cmd('c2.salt.com', 'cmd.run', ['ifconfig'])
result = local.cmd('*', 'cmd.run', ['ifconfig'])
```



### CMDB目的：   ——   自动化平台的基础

​	管理所有的服务器资产

​	自动获取资产信息

​	给其他的平台提供数据

### 实现的思路

1. agent：
   1. 脚本放在每一台服务器上 —— agent
   2. 定时启动  
   3. 执行linux的命令，对命令的结果进行解析
   4. 通过requests模块发送资产信息到api
   5. api接受数据保存到数据库

2. ssh类（中控机）
   1. 脚本放在中控机上
   2. 定时启动  
   3. ssh远程连接上每一台服务器，执行linux的命令，对命令的结果进行解析
   4. 通过requests模块发送资产信息到api
   5. api接受数据保存到数据库
3. salt（中控机）
   1. 脚本放在中控机上
   2. 定时启动  
   3. salt（rpc）远程连接上每一台服务器，执行linux的命令，对命令的结果进行解析
   4. 通过requests模块发送资产信息到api
   5. api接受数据保存到数据库



开发的目标：

​	兼容三种模式，并且支持可扩展





-i https://pypi.tuna.tsinghua.edu.cn/simple