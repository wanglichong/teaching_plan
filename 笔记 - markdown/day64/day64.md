# day64

## 内容回顾

### 1.cookie和session

cookie

1. 定义：保存在浏览器本地一组组键值对

2. 为什么要有cookie？

   http协议是无状态。每次请求都是独立，相互之间是没有关系的，没办法保存状态。

3. 特性：

   1. 服务器让浏览器进行设置
   2. 保存在浏览器本地
   3. 下次访问的时候自动携带相应的cookie

4. django中操作cookie：

   1. 设置：      本质是   响应头  set-cookie :  key=value;

      ret  = HttpResponse('ok')

      ret .set_cookie(key,value,max_age=5)     path domain   # 普通的cookie

      ret.set_signed_cookie(key,value,salt='day62') 

   2. 获取：   本质   请求头  cookie： 

      request.COOKIES    ——》    {}

      request.COOKIES[key]    request.COOKIES.get(key)

      request.get_signed_cookie(key,salt='day62',default='xxxx')

   3. 删除：

      del  request.COOKIES[key] 

session

1. 定义：保存在服务器的一组组键值对，必须依赖于cookie。

2. 为什么要使用session？

   1. cookie保存在浏览器本地，不安全
   2. cookie的大小受到限制

3. django中的操作：

   1. 设置：

      request.session[key]  = value

      request.session.setdefault(key,value)

   2. 获取

      request.session[key]

      request.session.get(key)

   3. 删除

      del request.session[key]

       request.session.delete()      # 删除所有的session的数据   不删除cookie 

       request.session.flush()         # 删除所有的session的数据   删除cookie 

   4. 其他

       request.session.set_expiry(value)     # 设置超时时间

       request.session.clear_expired()        # 清除已失效的session数据

   5. 配置

      from django.conf import   global_settings

      

### 2. 中间件

中间件：本质上就是Python中的一个类，在全局范围内处理django的请求和响应的钩子。

   5个方法，4个特点

   process_request(self,request)

   执行的时间：请求到来后，路由匹配之前

   执行的顺序：按照注册顺序 顺序执行

   返回值：

   ​	None： 正常流程

   ​	HttpResponse对象：当前中间件之后的中间的process_request方法、视图函数都不执行，直接执行当前中间件的process_response方法。

   process_response(self,request,response)

   执行的时间：视图函数之后

   执行的顺序：按照注册顺序 倒序执行

   返回值：

   ​	HttpResponse对象：必须返回 

   process_view(self,request,view_func,view_args,view_kwargs):

   执行的时间：路由匹配之后，视图函数之前

   执行的顺序：按照注册顺序 顺序执行

   返回值：

   ​	None： 正常流程

   ​	HttpResponse对象：当前中间件之后的中间的process_view方法、视图函数都不执行，直接执行最后一个中间件的process_response方法。

   process_exception(self,request,exception)

   执行的时间(触发条件)：视图函数有异常

   执行的顺序：按照注册顺序 倒序执行

   返回值：

   ​	None： 交给下一个中间件处理异常

   ​	HttpResponse对象：当前中间件之后的中间的process_exception方法不执行，直接执行最后一个中间件的process_response方法。

   process_template_response(self,request,response)

   执行的时间(触发条件)：视图函数返回的对象是templateResponse

   执行的顺序：按照注册顺序 倒序执行

   返回值：

   ​	HttpResponse对象：必须返回

3.限制访问频率

5秒只能访问3次

1. 记录访问的次数
   1. 访问的时间
   2. 标识用户
2. 根据访问次数做判断
   1. 大于3次  返回 response对象
   2. 小于3次   正常流程

## 今日内容

### 1. csrf相关的装饰器

```python
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.decorators.csrf import ensure_csrf_cookie
csrf_exempt      # 当前的视图不需要CSRF校验
csrf_protect     # 当前的视图需要CSRF校验

CBV中  csrf_exempt只能加在dispatch上

```

### 2. csrf中间件

1. 请求到来时执行process_request方法：

   从cookie中获取csrftoken的值   —— 》 赋值给   request.META['CSRF_COOKIE'] 

2. 执行process_view方法：

   1. 查看视图是否加了csrf_exempt装饰器：

      1. 有    不需要csrf 校验  返回none 
      2. 没有  需要csrf 校验   

   2. 请求方式的判断：

      1. 如果是 'GET', 'HEAD', 'OPTIONS', 'TRACE'    不需要csrf 校验  返回none 
      2. 其他方式需要csrf 校验  

   3. 进行CSRF校验：

      1. 从request.META获取到csrftoken的值

      2. 尝试从表单中获取csrfmiddlewaretoken的值：

         1. 能获取到

            csrfmiddlewaretoken的值和cookie中的csrftoken值做对比

             	1. 对比成功   通过csrf校验
             	2. 对比不成功   不通过csrf校验  拒绝请求

         2. 获取不到  尝试从请求头中获取x-csrftoken的值

            x-csrftoken的值和cookie中的csrftoken值做对比

            1. 对比成功   通过csrf校验
            2. 对比不成功   不通过csrf校验  拒绝请求

            

### 3. ajax

#### 1.发请求的途径：

  1. 地址栏上输入地址   GET
  2. a标签     GET
  3. form表单  GET/POST

#### 2.ajax

 	是一个js的技术，发送请求的一种途径。

特点：

  1. 异步
  2. 局部刷新
  3. 传输的数据量小（xml  json）

#### 3.简单实例

```
$.ajax({
    url:'/calc/',
    type:'post',
    data:{
        i1:$('[name="i1"]').val(),
        i2:$('[name="i2"]').val(),
    },
    success:function (res) {
        $('[name="i3"]').val(res)
    }
})
```

#### 4.参数

```
$.ajax({
    url: '/ajax_test/',   ＃　请求的地址
    type: 'post',		＃　请求的方式
    data: {				＃　请求的数据
        name: 'alex',
        age: 73,
        hobby: JSON.stringify(['装逼', '作死', '卖烧饼'])
    },
    success: function (res) {　　　　＃　成功后执行的函数

    },
    error: function (res) {			＃　失败后执行的函数
        console.log(res)
    },
})
```

#### ５.上传文件

```
$('#b4').click(function () {

    var form_obj = new FormData();
    form_obj.append('f1',$('#f1')[0].files[0]);
    form_obj.append('name','alex';

    $.ajax({
        url: '/upload/',
        type: 'post',
        data:form_obj,
        processData:false,    // 不用处理编码方式
        contentType:false,    // 不用处理contentType的请求头
        success: function (res) {
            console.log(res)
        },

    })
```

#### 6. ajax通过django的csrf校验

1. 确保csrftoken的cookie

   1. 一个加 {% csrf_tokne%}

   2. 给视图函数加装饰器

   ```
   from django.views.decorators.csrf import ensure_csrf_cookie
   ```

2. 提供参数
   1. 提交的数据中加csrfmiddlewaretoken值

```
data: {
    csrfmiddlewaretoken: $('[name="csrfmiddlewaretoken"]').val(),
    i1: $('[name="i1"]').val(),
    i2: $('[name="i2"]').val(),
},
```

​	2. 加x-csrftoken的请求头

 ```
   headers: {
       'x-csrftoken': $('[name="csrfmiddlewaretoken"]').val(),
   },
 ```

​	3. 文件导入

